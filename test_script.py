#!/usr/bin/env python3


import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

import string
import random

print("zk-data-test-script")

for s in libs.send.get_server_list():
    if s[0] == "test_server": ip = s[1]
print("ip: " + ip)

secure_key = "1673723054.5696783"

def get_string():
    name = ""
    lenge = random.randint(2, 18)
    for x in range(0, lenge):
        name = name + random.choice(string.ascii_letters)
    
    return name

c_list = libs.send.search_client(ip, secure_key, {"name": get_string()})
if c_list == []:
    new_client = libs.send.create_client(ip, secure_key)
    new_client["name"] = get_string()
else:
    new_client = libs.send.get_client(ip, secure_key, c_list[0])
libs.send.set_client(ip, secure_key, new_client)

# Article
article_number = get_string()
a_list = libs.send.search_article(ip, secure_key, {"suche": article_number})
if a_list == []:
    new_article = libs.send.create_article(ip, secure_key, -1)
    new_article["name_de"] = get_string() + ": " + article_number
    new_article["artikel"] = article_number
    new_article["preisvk"] = round(random.randint(1, 9999)/random.randint(1, 9999), 4)
    new_article["preisek"] = new_article["preisvk"] / 1.8
else:
    new_article = libs.send.get_article(ip, secure_key, a_list[0])    
libs.send.set_article(ip, secure_key, new_article)

# Angebot
new_delivery = libs.send.create_delivery_angebot(ip, secure_key)
libs.send.set_delivery_info(ip, secure_key, new_delivery["identification"], get_string())
libs.send.set_delivery_date_todo(ip, secure_key, new_delivery["identification"] , libs.BlueFunc.TodayAddDays(random.randint(1, 365)))
libs.send.set_delivery_client(ip, secure_key, new_delivery["identification"] , new_client["identification"])
price = round(random.randint(1, 9999)/random.randint(1, 9999), 4)
libs.send.set_delivery_line(ip, secure_key, new_delivery["identification"], 0, random.randint(0, 100), new_article["barcode"], get_string(), price, 21.0)
libs.send.set_delivery_finish(ip, secure_key, new_delivery["identification"] , random.randint(0, 1))

# Lieferschein
new_delivery = libs.send.create_delivery(ip, secure_key)
libs.send.set_delivery_info(ip, secure_key, new_delivery["identification"], get_string())
libs.send.set_delivery_date_todo(ip, secure_key, new_delivery["identification"] , libs.BlueFunc.TodayAddDays(random.randint(1, 365)))
libs.send.set_delivery_client(ip, secure_key, new_delivery["identification"] , new_client["identification"])
libs.send.set_delivery_finish(ip, secure_key, new_delivery["identification"] , random.randint(0, 1))

price = round(random.randint(1, 9999)/random.randint(1, 9999), 4)
libs.send.set_delivery_line(ip, secure_key, new_delivery["identification"], 0, random.randint(0, 100), new_article["barcode"], get_string(), price, 21.0)
price = round(random.randint(1, 9999)/random.randint(1, 9999), 4)
libs.send.set_delivery_line(ip, secure_key, new_delivery["identification"], 1, random.randint(0, 100), new_article["barcode"], get_string(), price, 21.0)
price = round(random.randint(1, 9999)/random.randint(1, 9999), 4)
libs.send.set_delivery_line(ip, secure_key, new_delivery["identification"], 2, random.randint(0, 100), new_article["barcode"], get_string(), price, 6.0)


### TODO Invoice and Invoice E testing
