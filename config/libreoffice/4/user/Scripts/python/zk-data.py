#!/usr/bin/env python3

import uno
import sys
import requests
import json
import os

import easygui
sys.path.append("/etc/zk-data-libs")
import libs.send

server_list = libs.send.get_server_list()
server_choice = easygui.choicebox("Server wählen", "Bitte Server name auswählen", server_list)
server_ip = server_choice.split("\'")[-2]
secure_key = easygui.passwordbox("Benutzer Passwort Eingeben")

def version_610_2():
    print("")
 
def find_identification_by_artikel_and_lieferant():
    oDoc = XSCRIPTCONTEXT.getDocument()
    
    oSheet = oDoc.getSheets().getByIndex(0)
    
    create_article_if_not_found = easygui.ynbox('Artikel erstellen die nicht existieren ?', 'Title', ('Ja', 'Nein'))
    if create_article_if_not_found:
        set_custom_identification = easygui.ynbox('Eigene Identification festlegen ?', 'Title', ('Ja', 'Nein'))
        if set_custom_identification:
            start_id = easygui.enterbox('Start identification für neue Artikel ?')
            try: start_id = int(start_id)
            except: start_id = 0
    
    max_columns = 100
    max_rows = 1000
    
    headers = []
    identification_column_index = 0
    artikel_column_index = 0
    lieferant_column_index = 0
    for header_name_index in range(0, max_columns):
        header_name = oSheet.getCellByPosition(header_name_index, 0).String
        if header_name == "identification":
            identification_column_index = header_name_index
        if header_name == "artikel":
            artikel_column_index = header_name_index
        if header_name == "lieferant":
            lieferant_column_index = header_name_index
        headers.append(header_name)
        
    if "identification" in headers and "artikel" in headers:
        for line_index in range(1, max_rows):
            line_artikel = oSheet.getCellByPosition(artikel_column_index, line_index).String
            line_artikel = line_artikel.replace("-", "").replace(" ", "").replace(".", "").replace(",", "")
            line_lieferant = oSheet.getCellByPosition(lieferant_column_index, line_index).String
            if not line_artikel == "" and not line_lieferant == "":
                DATA = {"mode": "search_article", "artikel": line_artikel, "lieferant": line_lieferant}
                article_list = libs.send.send_and_receive(server_ip, secure_key, DATA)
                if article_list == []:
                    if create_article_if_not_found:
                        if set_custom_identification:
                            DATA = {"mode": "create_article", "identification": start_id}
                        else:
                            DATA = {"mode": "create_article"}
                            
                        new_article = libs.send.send_and_receive(server_ip, secure_key, DATA)
                        oSheet.getCellByPosition(identification_column_index, line_index).String = new_article["identification"]
                        
                        DATA = {"mode": "set_article", "identification": new_article["identification"], "lieferant": line_lieferant, "artikel": line_artikel}
                        new_article = libs.send.send_and_receive(server_ip, secure_key, DATA)
                else:
                    if len(article_list) == 1:
                        oSheet.getCellByPosition(identification_column_index, line_index).String = article_list[0]
                        
 
    return None
    
def get_article_data():
    oDoc = XSCRIPTCONTEXT.getDocument()
    
    oSheet = oDoc.getSheets().getByIndex(0)
       
    max_columns = 100
    max_rows = 1000
    
    headers = []
    identification_index = 0
    for header_name_index in range(0, max_columns):
        header_name = oSheet.getCellByPosition(header_name_index, 0).String
        headers.append(header_name)
        if header_name == "identification":
            identification_index = header_name_index
        
    if "identification" in headers:
        for line_index in range(1, max_rows):
            article_identification = oSheet.getCellByPosition(identification_index, line_index).String
            if not article_identification == "":
                DATA = {"mode": "get_article", "identification": article_identification}
                article = libs.send.send_and_receive(server_ip, secure_key, DATA)
                    
                if not article == {}:
                    for header_name_index in range(0, max_columns):
                        if not header_name_index == identification_index:
                            header_name = headers[header_name_index]
                            if header_name in article:
                                value = str(article[header_name])
                                if header_name in ["preisek", "preisvkh", "preisvk", "anzahl"]:
                                    value = value.replace(".", ",")
                                oSheet.getCellByPosition(header_name_index,line_index).String = value
                  
    return None
    
def get_client_data():
    oDoc = XSCRIPTCONTEXT.getDocument()
    
    oSheet = oDoc.getSheets().getByIndex(0)
       
    max_columns = 100
    max_rows = 1000
    
    headers = []
    identification_index = 0
    for header_name_index in range(0, max_columns):
        header_name = oSheet.getCellByPosition(header_name_index, 0).String
        headers.append(header_name)
        if header_name == "identification":
            identification_index = header_name_index
        
    if "identification" in headers:
        for line_index in range(1, max_rows):
            client_identification = oSheet.getCellByPosition(identification_index, line_index).String
            if not client_identification == "":
                DATA = {"mode": "get_client", "identification": client_identification}
                article = libs.send.send_and_receive(server_ip, secure_key, DATA)
                    
                if not article == {}:
                    for header_name_index in range(0, max_columns):
                        if not header_name_index == identification_index:
                            header_name = headers[header_name_index]
                            if header_name in article:
                                value = str(article[header_name])
                                #if header_name in ["preisek", "preisvkh", "preisvk", "anzahl"]:
                                #    value = value.replace(".", ",")
                                oSheet.getCellByPosition(header_name_index,line_index).String = value
                  
    return None
    
def set_article_data():
    oDoc = XSCRIPTCONTEXT.getDocument()
    
    oSheet = oDoc.getSheets().getByIndex(0)
       
    max_columns = 100
    max_rows = 1000
    
    headers = []
    identification_index = 0
    for header_name_index in range(0, max_columns):
        header_name = oSheet.getCellByPosition(header_name_index, 0).String
        headers.append(header_name)
        if header_name == "identification":
            identification_index = header_name_index
        if header_name == "status":
            status_index = header_name_index
        
    if "identification" in headers:
        for line_index in range(1, max_rows):
            article_identification = oSheet.getCellByPosition(identification_index, line_index).String
            if not article_identification == "":
                DATA = {"mode": "get_article", "identification": article_identification}
                article = libs.send.send_and_receive(server_ip, secure_key, DATA)
                
                if not article == {}:
                    new_data = {"mode": "set_article", "identification": article_identification}
                    for header_name_index in range(0, max_columns):
                        if not header_name_index == identification_index:
                            header_name = headers[header_name_index]
                            if header_name in article:
                                value = oSheet.getCellByPosition(header_name_index,line_index).String
                                if not value == "":
                                    new_data[header_name] = value
                            
                    result = libs.send.send_and_receive(server_ip, secure_key, new_data)
                    if "status" in headers:
                        oSheet.getCellByPosition(status_index,line_index).String = str(result)
                  
    return None
    
def set_client_data():
    oDoc = XSCRIPTCONTEXT.getDocument()
    
    oSheet = oDoc.getSheets().getByIndex(0)
       
    max_columns = 100
    max_rows = 1000
    
    headers = []
    identification_index = 0
    for header_name_index in range(0, max_columns):
        header_name = oSheet.getCellByPosition(header_name_index, 0).String
        headers.append(header_name)
        if header_name == "identification":
            identification_index = header_name_index
        if header_name == "status":
            status_index = header_name_index
        
    if "identification" in headers:
        for line_index in range(1, max_rows):
            client_identification = oSheet.getCellByPosition(identification_index, line_index).String
            if not client_identification == "":
                DATA = {"mode": "get_client", "identification": client_identification}
                article = libs.send.send_and_receive(server_ip, secure_key, DATA)
                
                if not article == {}:
                    new_data = {"mode": "set_client", "identification": client_identification}
                    for header_name_index in range(0, max_columns):
                        if not header_name_index == identification_index:
                            header_name = headers[header_name_index]
                            if header_name in article:
                                value = oSheet.getCellByPosition(header_name_index,line_index).String
                                if not value == "":
                                    new_data[header_name] = value
                            
                    result = libs.send.send_and_receive(server_ip, secure_key, new_data)
                    if "status" in headers:
                        oSheet.getCellByPosition(status_index,line_index).String = str(result)
                  
    return None
