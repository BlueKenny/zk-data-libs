#!/usr/bin/env python3


def ArtikelFormat(artikel, lieferant):    
    DictType = {"STIHL": funct434,
                "GM": funct4_5
    }

    artikel = str(artikel).upper()
    lieferant = str(lieferant).upper()

    if lieferant in DictType: return DictType[lieferant](artikel) #function=DictType(artikel)
    else: return funct2x(artikel)


def funct4_5(artikel):# 1234.12345
    print("funct4_5(" + str(artikel) + ")")
    
    artikel = artikel[:4] + "-" + artikel[4:]
    return artikel

def funct434(artikel):# 1234.123.1234
    print("funct434(" + str(artikel) + ")")
    
    artikel = artikel[:4] + "." + artikel[4:7] + "." + artikel[7:]
    return artikel


def funct2x(artikel): # 12.12.12...
    print("funct2x(" + str(artikel) + ")")

    return artikel
