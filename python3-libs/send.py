#!/usr/bin/env python3

# -*- coding: utf-8 -*-
import platform
import socket
import os
import time

import requests

import sys
sys.path.append("/etc/zk-data-libs/")

import base64

import libs.BlueFunc
import random

import json
    
def send_and_receive(ip_adr, secure_key, DATA):
    print("send_and_receive(" + str(ip_adr) + ", " + str(secure_key) + ", " + str(DATA) + ")")
    try:
        DATA["secure_key"] = str(secure_key)
        if not ":51515" in str(ip_adr): ip_adr = str(ip_adr) + ":51515"
        print("http://"+ ip_adr)
        r = requests.post("http://"+ ip_adr, json = DATA)
        print(r.text)
        data_answer = r.json()
    except:
        print("ERROR in send_and_receive")
        data_answer = ""
    
    return data_answer
       
def send_and_receive_old(ip_adr, secure_key, DATA):
    print("send_and_receive(" + str(ip_adr) + ", " + str(secure_key) + ", " + str(DATA) + ")")
    
    try:            
        ip = ip_adr.split(":")[0]
        port = int(ip_adr.split(":")[1])
            
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((ip, port))

        DATA["secure_key"] = str(secure_key)
        data_send = json.dumps(DATA)  # data serialized
        data_send = data_send.encode()
                
        sock.sendto(data_send, (ip, port))
        data_answer = sock.recv(25000)
              
        data_answer = data_answer.decode()
        data_answer = json.loads(data_answer)
        sock.close()
    except Exception as e:
        print("Error in send_and_receive: " + str(DATA))
        print("Error -> " + str(e))
        
        if "NO_ERROR_SENDING" in str(DATA):
            print("NO_ERROR_SENDING in DATA")
        else:        
            log_data = []
            log_data.append("ZK-DATA send_and_receive ERROR")
            log_data.append(os.popen("uname -a").read())
            log_data.append("---------------------")
            log_data.append("send:")
            log_data.append("send_and_receive(" + str(ip_adr) + ", " + str(secure_key) + ", " + str(DATA) + ")")
            log_data.append("---------------------")
            log_data.append("error:")                
            log_data.append(str(e))                
                    
            libs.BlueFunc.send_telegram_text("\n".join(log_data))
            
        data_answer = ""
                        
    return data_answer
    
def get_cache(variable):
    print("get_cache(" + str(variable) + ")")
    variable = str(variable)
    
    file = "/tmp/zk-data-send-cache/" + variable
    
    data = ""
    
    if os.path.exists(file):
        data = open(file, "r").read()
        if not data == "":       
            data = json.loads(data)        
    else:
        if not os.path.exists("/tmp/zk-data-send-cache/"):
            os.system("mkdir /tmp/zk-data-send-cache/")
            
        open(file, "w").write(data)
        
    return data

def set_cache(variable, data):
    print("set_cache(" + str(variable) + ", " + str(data) + ")")
    variable = str(variable)
    data = json.dumps(data)  # data serialized
    
    file = "/tmp/zk-data-send-cache/" + variable

    if not os.path.exists("/tmp/zk-data-send-cache/"):
        os.system("mkdir /tmp/zk-data-send-cache/")

    open(file, "w").write(data)

##############          STOCK

def get_file_length(ip, secure_key, identification):
    print("get_file_length(" + str(identification) + ")")
    try:
        Dict = {"mode":"get_file_length"}
        Dict["identification"] = str(identification)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_file_length() = " + str(data))
        return data
    except:
        print("get_file_length() = ERROR")
        return 0

def get_file_part(ip, secure_key, identification, index):
    print("get_file_part(" + str(identification) + ", " + str(index) + ")")
    filename = "/tmp/get_file_" + identification + "_" + identification
    
    DATA = {}
    DATA["mode"] = "get_file_part"
    DATA["index"] = index
    DATA["identification"] = identification        
    part = libs.send.send_and_receive(ip, secure_key, DATA)     
    if not part == "FILE_END":
        if part == "":
            filename = ""        
        else:
            if index == 0: # new file
                data = part.encode('utf8')
                data = base64.b64decode(data)
                open(filename, "wb").write(data)
            else:                             
                old_data = open(filename, "rb").read()            
                new_data = part.encode('utf8')
                new_data = base64.b64decode(new_data)
                open(filename, "wb").write(old_data + new_data)    

    if not os.path.exists(filename): filename = ""
    return filename

def get_file(ip, secure_key, identification):
    print("get_file(" + str(identification) + ")")
    timestamp = str(libs.BlueFunc.Timestamp())
    filename = "/tmp/get_file_" + timestamp + "_" + identification
    index = 0
    while True:
        DATA = {}
        DATA["mode"] = "get_file_part"
        DATA["index"] = index
        DATA["identification"] = identification        
        part = libs.send.send_and_receive(ip, secure_key, DATA)          
        if part == "FILE_END": break
        if index == 0: # new file
            data = part.encode('utf8')
            data = base64.b64decode(data)
            open(filename, "wb").write(data)
        else:                             
            old_data = open(filename, "rb").read()            
            new_data = part.encode('utf8')
            new_data = base64.b64decode(new_data)
            open(filename, "wb").write(old_data + new_data)    
        index = index + 1        
    if not os.path.exists(filename): filename = ""
    return filename
        
def send_file(ip, secure_key, file):# return identification on server
    print("send_file(" + str(file) + ")")
    filename = file.split("/")[-1]
    file_bytes = open(file, "rb").read()
    file_as_string = base64.b64encode(file_bytes)          
    index = 0
    parts_list = libs.BlueFunc.file_split(file_as_string, 1024)
    answer_file_list = []
    identification = filename
    for part in parts_list:
        DATA = {}
        DATA["mode"] = "send_file_part"
        DATA["index"] = index
        DATA["identification"] = identification          
        DATA["data"] = part.decode('utf8')    
        answer = libs.send.send_and_receive(ip, secure_key, DATA)              
        if index == 0: identification = answer            
        index = index + 1
    return identification

def get_delivery_rest(ip, secure_key, identification):
    print("get_delivery_rest(" + str(identification) + ")")
    
    delivery = get_delivery(ip, secure_key, identification)
    rest = 0.0
    
    for index in range(0, len(delivery["linien"].split("|"))):
        index = int(index)
        
        anzahl = float(delivery["anzahl"].split("|")[index])
        bcode = delivery["bcode"].split("|")[index]
        preis_vkh = float(delivery["preis_htva"].split("|")[index])
        preis_ek = 0.0
        
        if not "bcode" == "":
            print("bcode: " + str(bcode))
            article = get_article(ip, secure_key, bcode)
            if article == {}:
                preis_ek = 0.0
            else:
                preis_ek = float(article["preisek"])
        
        added = anzahl * (preis_vkh - preis_ek)
        rest = round(rest + added, 2)
        print("rest: " + str(rest))
        
    return float(rest)

def create_rechnung_e(ip, secure_key, lieferant, datum, doc_id, total, pdf_link):
    try:
        Dict = {"mode":"create_rechnung_e"}        
        Dict["lieferant"] = str(lieferant)
        Dict["dokument_datum"] = str(datum)
        Dict["doc_id"] = str(doc_id)
        Dict["total"] = float(total)
        Dict["pdf_link"] = str(pdf_link)
        data = send_and_receive(ip, secure_key, Dict)
        print("create_rechnung_e(" + str(lieferant) + ", " + str(datum) + ", " + str(doc_id) + ", " + str(total) + ", " + str(pdf_link) + ") = " + str(data))
        return data
    except:
        print("create_rechnung_e(" + str(lieferant) + ", " + str(datum) + ", " + str(doc_id) + ", " + str(total) + ", " + str(pdf_link) + ") = ERROR")
        return {}
        
def get_next_changed_article(ip, secure_key, timestamp):# return [ str(bcode) , float(timestamp) ]
    try:
        Dict = {"mode":"get_next_changed_article"}
        Dict["timestamp"] = float(timestamp)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_next_changed_article(" + str(timestamp) + ") = " + str(data))
        return data
    except:
        print("get_next_changed_article(" + str(timestamp) + ") = ERROR")
        return ["", 0.0]
        
def get_next_changed_client(ip, secure_key, timestamp):#return List of Dict
    try:
        Dict = {"mode":"get_next_changed_client"}
        Dict["timestamp"] = float(timestamp)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_next_changed_client(" + str(timestamp) + ") = " + str(data))
        return data
    except:
        print("get_next_changed_client(" + str(timestamp) + ") = ERROR")
        return ["", 0.0]

def get_article_move(ip, secure_key, identification, bcode):# return [ str(bcode) , float(timestamp) ]
    try:
        Dict = {"mode":"get_article_move"}
        Dict["identification"] = identification
        Dict["bcode"] = bcode
        data = send_and_receive(ip, secure_key, Dict)
        print("get_article_move(" + str(identification) + ", " + str(bcode) + ") = " + str(data))
        return data
    except:
        print("get_article_move(" + str(ID) + ", " + str(bcode) + ") = ERROR")
        return {}

def create_transaktion_free(ip, secure_key, datum, uhrzeit, betrag, konto_in, konto_out, mitteilung):#return Dict
    try:
        Dict = {"mode":"create_transaktion_free"}
        Dict["datum"] = str(datum)
        Dict["uhrzeit"] = str(uhrzeit)
        Dict["betrag"] = str(betrag)
        Dict["konto_in"] = str(konto_in)
        Dict["konto_out"] = str(konto_out)
        Dict["mitteilung"] = ""
        for char in str(mitteilung):
            if len(Dict["mitteilung"]) < 1000:
                Dict["mitteilung"] = Dict["mitteilung"] + char
        data = send_and_receive(ip, secure_key, Dict)
        print("create_transaktion_free(" + str(datum) + ", " + str(uhrzeit) + ", " + str(betrag) + ", " + str(konto_in) + ", " + str(konto_out) + ", " + str(mitteilung) + ") = " + str(data))
        return data
    except:
        print("create_transaktion_free(" + str(datum) + ", " + str(uhrzeit) + ", " + str(betrag) + ", " + str(konto_in) + ", " + str(konto_out) + ", " + str(mitteilung) + ") = ERROR")
        return -1
        
def create_client(ip, secure_key):#return Dict
    try:
        Dict = {"mode":"create_client"}
        data = send_and_receive(ip, secure_key, Dict)        
        print("create_client() = " + str(data))
        return data
    except:
        print("create_client() = ERROR")
        return {}

def get_sn_data(ip, secure_key, sn):#return list
    try:
        Dict = {"mode":"get_sn_data"}
        Dict["sn"] = str(sn)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_sn_data(" + str(sn) + ") = " + str(data))
        return data
    except:
        print("get_sn_data(" + str(sn) + ") = ERROR")
        return []

def search_sn_by_dates(ip, secure_key, date_from, date_to):#return filepath on server
    try:
        Dict = {"mode":"search_sn_by_dates"}
        Dict["date_from"] = str(date_from)
        Dict["date_to"] = str(date_to)
        data = send_and_receive(ip, secure_key, Dict)
        print("search_sn_by_dates(" + str(date_from) + ", " + str(date_to) + ") = " + str(data))
        return data
    except:
        print("search_sn_by_dates(" + str(date_from) + ", " + str(date_to) + ") = ERROR")
        return ""

def create_client_with_identification(ip, secure_key, ID):#return Dict
    try:
        Dict = {"mode":"create_client_with_identification"}
        Dict["identification"] = str(ID)
        data = send_and_receive(ip, secure_key, Dict)
        print("create_client_with_identification(" + str(ID) + ") = " + str(data))
        return data
    except:
        print("create_client_with_identification(" + str(ID) + ") = ERROR")
        return {}

def get_client(ip, secure_key, ID):#return Dict
    try:
        Dict = {"mode":"get_client"}
        Dict["identification"] = str(int(ID))        
        data = send_and_receive(ip, secure_key, Dict)              
        print("get_client(" + str(ID) + ") = " + str(data))
        return data
    except:
        print("get_client(" + str(ID) + ") = ERROR")
        return {}
        
def get_deliverys_file(ip, secure_key, year):#get file with all deliverys from year
    try:
        Dict = {"mode":"get_deliverys_file"}
        Dict["year"] = str(year)
        data = send_and_receive(ip, secure_key, Dict)              
        print("get_deliverys_file() = " + str(data))
        return data
    except:
        print("get_deliverys_file() = ERROR")
        return {}
        
def get_invoices_file(ip, secure_key, year):#get file with all invoices from year
    try:
        Dict = {"mode":"get_invoices_file"}
        Dict["year"] = str(year)
        data = send_and_receive(ip, secure_key, Dict)              
        print("get_invoices_file() = " + str(data))
        return data
    except:
        print("get_invoices_file() = ERROR")
        return {}
        
        
def get_articles(ip, secure_key):#get all articles
    try:
        articles_path = get_articles_file(ip, secure_key)
        data = requests.get("http://" + ip.split(":")[0] + articles_path).json()             
        print("get_articles() => " + str(len(data)) + " articles")
        return data
    except:
        print("get_articles() = ERROR")
        return {}
        
def get_articles_file(ip, secure_key):#get file with all articles
    try:
        Dict = {"mode":"get_articles_file"}     
        data = send_and_receive(ip, secure_key, Dict)              
        print("get_articles_file() = " + str(data))
        return data
    except:
        print("get_articles_file() = ERROR")
        return {}
        
def get_clients_file(ip, secure_key):#get file with all clients
    try:
        Dict = {"mode":"get_clients_file"}     
        data = send_and_receive(ip, secure_key, Dict)              
        print("get_clients_file() = " + str(data))
        return data
    except:
        print("get_clients_file() = ERROR")
        return {}

def GetKunden(ip, secure_key, ID):#return Dict
    return get_client(ip, secure_key, ID)
        
def set_client(ip, secure_key, Dict):#return Dict
    try:
        Dict["mode"] ="set_client"       
        data = send_and_receive(ip, secure_key, Dict)              
        print("set_client(" + str(Dict) + ") = " + str(data))
        return data
    except:
        print("set_client(" + str(Dict) + ") = ERROR")
        return {}

def SetKunde(ip, secure_key, Dict):#return Dict
    return set_client(ip, secure_key, Dict)

def close(ip, secure_key):#close server
    try:
        Dict = {"mode":"close"}
        data = send_and_receive(ip, secure_key, Dict)
        print("close() = " + str(data))
        return data
    except:
        print("close() = ERROR ? or closed")
        return {}

def NeuerLieferschein(ip, secure_key):#return Dict
    try:
        Dict = {"mode":"NeuerLieferschein"}
        data = send_and_receive(ip, secure_key, Dict)
        print("NeuerLieferschein() = " + str(data))
        return data
    except:
        print("NeuerLieferschein() = ERROR")
        return {}

def get_barcode_helper(ip, secure_key, barcode): # return List + Name of Articles (<ID 100)
    try:
        Dict = {"mode":"get_barcode_helper"}
        Dict["barcode"] = str(barcode)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_barcode_helper(" + str(barcode) + ") = " + str(data))
        return data
    except:
        print("get_barcode_helper() = ERROR")
        return ""

def get_article_categories(ip, secure_key):
    try:
        Dict = {"mode":"get_article_categories"}
        data = send_and_receive(ip, secure_key, Dict)
        print("get_article_categories() = " + str(data))
        return data
    except:
        print("get_article_categories() = ERROR")
        return []

def get_stats_categorieprofit(ip, secure_key, date):
    try:
        Dict = {"mode":"get_stats_categorieprofit"}
        Dict["date"] = str(date)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_stats_categorieprofit(" + str(date) + ") = " + str(data))
        return data
    except:
        print("get_stats_categorieprofit(" + str(date) + ") = ERROR")
        return {}

def get_marchants(ip, secure_key ): # get list of marchants
    try:
        Dict = {"mode":"get_marchants"}
        data = send_and_receive(ip, secure_key, Dict)
        print("get_marchants() = " + str(data))
        return data
    except:
        print("get_marchants() = ERROR")
        return []
        
def get_client_total(ip, secure_key, identification ): # get total of Client deliverys
    try:
        Dict = {"mode":"get_client_total"}
        Dict["identification"] = str(identification)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_client_total(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("get_client_total(" + str(identification) + ") = ERROR")
        return 0

def get_worker_total(ip, secure_key, days): # get total of Worker
    try:
        Dict = {"mode":"get_worker_total"}
        Dict["days"] = str(days)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_worker_total(" + str(days) + ") = " + str(data))
        return str(data)
    except:
        print("get_worker_total(" + str(days) + ") = ERROR")
        return 0

def GetGewinn(ip, secure_key, DATUM):#return Float
    try:
        Dict = {"mode":"GetGewinn"}
        Dict["datum"] = str(DATUM)
        data = send_and_receive(ip, secure_key, Dict)
        print("GetGewinn(" + str(DATUM) + ") = " + str(data))
        return float(data)
    except:
        print("GetGewinn(" + str(DATUM) + ") = ERROR")
        return 0.0

def print_article_stock(ip, secure_key, date):
    try:
        Dict = {"mode":"print_article_stock"}
        Dict["datum"] = str(date)
        data = send_and_receive(ip, secure_key, Dict)
        print("print_article_stock() = " + str(data))
        return data
    except:
        print("print_article_stock() = ERROR")
        return "ERROR"

def GetLieferschein(ip, secure_key, ID):#return Dict
    return get_delivery(ip, secure_key, ID)

def get_delivery_with_log(ip, secure_key, identification):#return Dict
    try:
        Dict = {"mode":"get_delivery"}
        Dict["identification"] = str(identification).replace(".0", "")
        Dict["log"] = True
        data = send_and_receive(ip, secure_key, Dict)
        print("get_delivery_with_log(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("get_delivery_with_log(" + str(identification) + ") = ERROR")
        return {}
        
def get_delivery(ip, secure_key, identification):#return Dict
    try:
        Dict = {"mode":"get_delivery"}
        Dict["identification"] = str(identification).replace(".0", "")
        data = send_and_receive(ip, secure_key, Dict)
        print("get_delivery(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("get_delivery(" + str(identification) + ") = ERROR")
        return {}

def get_delivery_pdf(ip, secure_key, identification):#return Answer Path
    try:
        Dict = {"mode":"get_delivery_pdf"}
        Dict["identification"] = str(identification).replace(".0", "")
        data = send_and_receive(ip, secure_key, Dict)
        print("get_delivery_pdf(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("get_delivery_pdf(" + str(identification) + ") = ERROR")
        return ""

def pre_printing_documents(ip, secure_key, document_type):#return bool of sucess
    try:
        Dict = {"mode":"pre_printing_documents"}
        Dict["document_type"] = str(document_type)
        data = send_and_receive(ip, secure_key, Dict)
        print("pre_printing_documents() = " + str(data))
        return data
    except:
        print("pre_printing_documents() = ERROR")
        return ""

def check_all_vat(ip, secure_key):#return bool of sucess
    try:
        Dict = {"mode":"check_all_vat"}
        data = send_and_receive(ip, secure_key, Dict)
        print("check_all_vat() = " + str(data))
        return data
    except:
        print("check_all_vat() = ERROR")
        return ""
        
def get_invoice_pdf(ip, secure_key, identification):#return Answer Path
    try:
        Dict = {"mode":"get_invoice_pdf"}
        Dict["identification"] = str(identification).replace(".0", "")
        data = send_and_receive(ip, secure_key, Dict)
        print("get_invoice_pdf(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("get_invoice_pdf(" + str(identification) + ") = ERROR")
        return ""

def delete_invoice_pdf(ip, secure_key, identification):
    try:
        Dict = {"mode":"delete_invoice_pdf"}
        Dict["identification"] = str(identification).replace(".0", "")
        data = send_and_receive(ip, secure_key, Dict)
        print("delete_invoice_pdf(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("delete_invoice_pdf(" + str(identification) + ") = ERROR")
        return ""

def get_invoice_e_pdf(ip, secure_key, identification):#return Answer Path
    try:
        Dict = {"mode":"get_invoice_e_pdf"}
        Dict["identification"] = str(identification).replace(".0", "")
        data = send_and_receive(ip, secure_key, Dict)
        print("get_invoice_e_pdf(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("get_invoice_e_pdf(" + str(identification) + ") = ERROR")
        return ""

def get_delivery_not_invoiced(ip, secure_key, index):#return Dict
    try:
        Dict = {"mode":"get_delivery_not_invoiced"}
        Dict["index"] = str(index)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_delivery_not_invoiced(" + str(index) + ") = " + str(data))
        return data
    except:
        print("get_delivery_not_invoiced(" + str(index) + ") = ERROR")
        return {}
        
def set_invoice_date_todo(ip, secure_key, identification, date):#return Dict
    try:
        Dict = {"mode":"set_invoice_date_todo"}
        Dict["identification"] = str(identification)
        Dict["date"] = str(date)
        data = send_and_receive(ip, secure_key, Dict)
        print("set_invoice_date_todo(" + str(identification) + ", " + str(date) + ") = " + str(data))
        return data
    except:
        print("set_invoice_date_todo(" + str(identification) + ", " + str(date) + ") = ERROR")
        return {}
        
def set_rechnung_date_todo(ip, secure_key, identification, date):#return Dict
    return set_invoice_date_todo(ip, secure_key, identification, date)
        
def get_mwst(ip, secure_key):#return liste
    try:
        Dict = {"mode":"get_mwst"}
        data = send_and_receive(ip, secure_key, Dict)
        print("get_mwst() = " + str(data))
        return data
    except:
        print("get_mwst() = ERROR")
        return []
        
def get_mwst_data(ip, secure_key, datum):#return link
    try:
        Dict = {"mode":"get_mwst_data"}
        Dict["datum"] = str(datum)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_mwst_data(" + str(datum) + ") = " + str(data))
        return data
    except:
        print("get_mwst(" + str(datum) + ") = ERROR")
        return ""
        
def get_delivery_user(ip, secure_key, ID):#return Dict
    try:
        Dict = {"mode":"get_delivery_user"}
        Dict["identification"] = str(ID)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_delivery_user(" + str(ID) + ") = " + str(data))
        return data
    except:
        print("get_delivery_user(" + str(ID) + ") = ERROR")
        return {}

def set_delivery_info(ip, secure_key, ID, info):#return bool(sucess)
    try:
        Dict = {"mode":"set_delivery_info"}
        Dict["identification"] = str(ID)
        Dict["info"] = info
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_info(" + str(ID) + ", " + str(info) + ") = " + str(data))
        return data
    except:
        print("set_delivery_info(" + str(ID) + ", " + str(info) + ") = ERROR")
        return {}

def set_delivery_user(ip, secure_key, identification , user, state ): # return bool(sucess)
    try:
        Dict = {"mode":"set_delivery_user"}
        Dict["identification"] = str(identification)
        Dict["add_user"] = user
        Dict["state"] = state
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_user(" + str(identification) + ", " + str(user) + ", " + str(state) + ") = " + str(data))
        return data
    except:
        print("set_delivery_user(" + str(identification) + ", " + str(user) + ", " + str(state) + ") = ERROR")
        return {}

def set_delivery_date_todo(ip, secure_key, ID , date): # return bool(sucess)
    try:
        Dict = {"mode":"set_delivery_date_todo"}
        ID = str(ID)
        date = str(date)
        Dict["identification"] = str(ID)
        Dict["date"] = str(date)
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_date_todo(" + str(ID) + ", " + str(date) + ") = " + str(data))
        return data
    except:
        print("set_delivery_date_todo(" + str(ID) + ", " + str(date) + ") = ERROR")
        return {}

def add_delivery_article(ip, secure_key, identification, bcode):# return bool(sucess)
    try:
        Dict = {"mode":"add_delivery_article"}
        Dict["identification"] = str(identification)
        Dict["bcode"] = str(bcode)
        data = send_and_receive(ip, secure_key, Dict)
        print("add_delivery_article(" + str(identification) + ", " + str(bcode) + ") = " + str(data))
        return data
    except:
        print("add_delivery_article(" + str(identification) + ", " + str(bcode) + ") = ERROR")
        return False
        
def set_delivery_line(ip, secure_key, identification, index, quantity, barcode, name, price, tva): # return bool(sucess)
    barcode = str(barcode).rstrip()
    
    if not barcode == "":
        print("check if " + str(barcode) + " isdigit()")
        if str(barcode).isdigit():
            print("barcode is digit")
            barcode = int(barcode)
        else:
            print("barcode is not digit -> start search_article")
            search_liste = search_article(ip, secure_key, {"suche": str(barcode)})
            if len(search_liste) == 1:
                barcode = int(search_liste[0])
            else:
                barcode = ""
                
    try:
        article_dict = get_article(ip, secure_key, str(barcode))     
        if article_dict == {}:
            price = str(price).upper().replace("EK", "")
            price = str(price).upper().replace("VK", "")
            price = str(price).upper().replace("VKH", "")
        else:
            price = str(price).upper().replace("EK", str(article_dict["preisek"]))    
            price = str(price).upper().replace("VK", str(article_dict["preisvk"])) 
            price = str(price).upper().replace("VKH", str(article_dict["preisvkh"]))
            
            # check TVA/VAT
            if not float(tva) == 0.0: # allow without vat sells
                tva = float(article_dict["tva"])
                
        Dict = {"mode":"set_delivery_line"}        
        Dict["identification"] = int(identification)
        Dict["index"] = int(index)
        Dict["anzahl"] = float(eval( str(quantity).replace(",", ".")) )
        Dict["name"] = str(name)          
        Dict["barcode"] = str(barcode)   
        if str(price) == "": price = 0.0        
        Dict["preis"] = float(eval( str(price).replace(",", ".")) )
        try:Dict["tva"] = float(str(tva).replace("%", ""))
        except: Dict["tva"] = 0.0
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_line(" + str(identification) + ", " + str(index) + ", " + str(quantity) + ", " + str(barcode) + ", " + str(name) + ", " + str(price) + ", " + str(tva) + ") = " + str(data))
        return data
    except:
        print("set_delivery_line(" + str(identification) + ", " + str(index) + ", " + str(quantity) + ", " + str(barcode) + ", " + str(name) + ", " + str(price) + ", " + str(tva) +  ") = ERROR")
        return {}

def set_tag_position(ip, secure_key, tag, position, timestamp): # return bool(sucess)
    try:
        Dict = {"mode":"set_tag_position"}
        Dict["tag"] = str(tag).upper()
        Dict["position"] = str(position).upper()
        try: Dict["timestamp"] = str(float(timestamp))
        except: Dict["timestamp"] = str(float(libs.BlueFunc.timestamp()))
        data = send_and_receive(ip, secure_key, Dict)
        print("set_tag_position(" + str(tag) + ", " + str(position) + ", " + str(timestamp) + ") = " + str(data))
        return data
    except:
        print("set_tag_position(" + str(tag) + ", " + str(position) + ", " + str(timestamp) + ") = ERROR")
        return False
        
def set_delivery_client(ip, secure_key, ID , client_id): # return bool(sucess)
    try:
        Dict = {"mode":"set_delivery_client"}
        Dict["identification"] = str(ID)
        Dict["kunde_id"] = client_id
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_client(" + str(ID) + ", " + str(client_id) + ") = " + str(data))
        return data
    except:
        print("set_delivery_client(" + str(ID) + ", " + str(client_id) + ") = ERROR")
        return {}

def set_delivery_engine(ip, secure_key, ID , text ): # return bool(sucess)
    try:
        Dict = {"mode":"set_delivery_engine"}
        Dict["identification"] = str(ID)
        Dict["maschine"] = text
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_engine(" + str(ID) + ", " + str(text) + ") = " + str(data))
        return data
    except:
        print("set_delivery_engine(" + str(ID) + ", " + str(text) + ") = ERROR")
        return {}

def set_delivery_finish(ip, secure_key, ID , finish ): # return bool(sucess)
    try:
        Dict = {"mode":"set_delivery_finish"}
        Dict["identification"] = str(ID)
        Dict["fertig"] = finish
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_finish(" + str(ID) + ", " + str(finish) + ") = " + str(data))
        return data
    except:
        print("set_delivery_finish(" + str(ID) + ", " + str(finish) + ") = ERROR")
        return {}

def set_delivery_line_pos(ip, secure_key, ID , old_pos , new_pos ): # return bool(sucess)
    try:
        Dict = {"mode":"set_delivery_line_pos"}
        Dict["identification"] = str(ID)
        Dict["old_pos"] = int(old_pos)
        Dict["new_pos"] = int(new_pos)
        data = send_and_receive(ip, secure_key, Dict)
        print("set_delivery_line_pos(" + str(ID) + ", " + str(old_pos) + ", " + str(new_pos) + ") = " + str(data))
        return data
    except:
        print("set_delivery_line_pos(" + str(ID) + ", " + str(old_pos) + ", " + str(new_pos) + ") = ERROR")
        return {}

def create_delivery(ip, secure_key): # return dict
    try:
        Dict = {"mode":"create_delivery"}
        data = send_and_receive(ip, secure_key, Dict)
        print("create_delivery() = " + str(data))
        return data
    except:
        print("create_delivery() = ERROR")
        return {}

def create_delivery_with_identification(ip, secure_key, identification): # return dict
    try:
        Dict = {"mode":"create_delivery_with_identification"}
        Dict["identification"] = int(identification)
        data = send_and_receive(ip, secure_key, Dict)
        print("create_delivery_with_identification() = " + str(data))
        return data
    except:
        print("create_delivery_with_identification() = ERROR")
        return {}

def create_delivery_angebot(ip, secure_key): # return dict
    try:
        Dict = {"mode":"create_delivery_angebot"}
        data = send_and_receive(ip, secure_key, Dict)
        print("create_delivery_angebot() = " + str(data))
        return data
    except:
        print("create_delivery_angebot() = ERROR")
        return {}

def create_delivery_bestellung(ip, secure_key): # return dict
    try:
        Dict = {"mode":"create_delivery_bestellung"}
        data = send_and_receive(ip, secure_key, Dict)
        print("create_delivery_bestellung() = " + str(data))
        return data
    except:
        print("create_delivery_bestellung() = ERROR")
        return {}

def delete_delivery_line(ip, secure_key, identification , index ): # return bool(sucess)
    try:
        Dict = {"mode":"delete_delivery_line"}
        Dict["identification"] = str(identification)
        Dict["index"] = int(index)
        data = send_and_receive(ip, secure_key, Dict)
        print("delete_delivery_line(" + str(identification) + ", " + str(index) + ") = " + str(data))
        return data
    except:
        print("delete_delivery_line(" + str(identification) + ", " + str(index) + ") = ERROR")
        return {}

def get_logs(ip, secure_key, document_type, document_id):#return Link or False
    try:
        Dict = {"mode":"get_logs"}
        Dict["document_type"] = str(document_type)
        Dict["document_id"] = str(document_id)
        data = send_and_receive(ip, secure_key, Dict)        
        print("get_logs(" + str(document_type) + ", " + str(document_id) + ") = " + str(data))
        return data
    except:
        print("get_logs(" + str(document_type) + ", " + str(document_id) + ") = ERROR")
        return False
        
def Print(ip, secure_key, printer, ID, info, quantity):#return Bool
    try:
        Dict = {"mode":"Print"}
        ID = str(ID)
        printer = str(printer)
        Dict["identification"]=str(ID)
        Dict["printer"]=int(printer)
        Dict["info"]=bool(info)
        Dict["quantity"]=int(quantity)
        data = send_and_receive(ip, secure_key, Dict)
        print("Print(" + str(printer) + ", " + str(ID) + ", " + str(info) + ", " + str(quantity) + ") = " + str(data))
        return data
    except:
        print("Print(" + str(printer) + ", " + str(ID) + ", " + str(info) + ", " + str(quantity) + ") = ERROR")
        return False

def SetLieferschein(ip, secure_key, Dict):#return Bool
    try:
        Dict["mode"] = "SetLieferschein"
        data = send_and_receive(ip, secure_key, Dict)
        print("SetLieferschein(" + str(Dict) + ") = " + str(data))
        return data
    except:
        print("SetLieferschein(" + str(Dict) + ") = ERROR")
        return False

def print_rappel(ip, secure_key, client):#return link
    try:
        Dict = {}
        Dict["mode"] = "print_rappel" 
        Dict["client"] = str(client)       
        data = send_and_receive(ip, secure_key, Dict)       
        print("print_rappel(" + str(client) + ") = " + str(data))
        return data
    except:
        print("print_rappel(" + str(client) + ") = ERROR")
        return False

def get_article(ip, secure_key, ID):#return Dict
    try:
        print("get_article")
        Dict = {"mode":"get_article"}
        ID = str(ID).replace(".0", "")
        if len(ID) == 13 or len(ID) == 12:
            Dict["barcode"] = int(ID)
        else:
            Dict["identification"] = str(ID)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_article(" + str(ID) + ") = " + str(data))
        return data
    except:
        print("get_article(" + str(ID) + ") = ERROR")
        return {}
        
def GetArt(ip, secure_key, ID):#return Dict
    return get_article(ip, secure_key, ID)

def getMinimum(ip, secure_key, position):#return Dict
    try:
        Dict = {"mode":"getMinimum"}
        Dict["position"] = str(position)
        data = send_and_receive(ip, secure_key, Dict)
        print("getMinimum(" + str(position) + ") = " + str(data))
        return data
    except:
        print("getMinimum(" + str(position) + ") = ERROR")
        return {}

def removeMinimum(ip, secure_key, identification):#return Dict
    try:
        Dict = {"mode":"removeMinimum"}
        Dict["identification"] = str(identification)
        data = send_and_receive(ip, secure_key, Dict)
        print("removeMinimum(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("removeMinimum(" + str(identification) + ") = ERROR")
        return {}

def add_lieferschein_to_vorlage(ip, secure_key, lieferschein_id, rechnung_id):  
    try:
        Dict = {"mode":"add_lieferschein_to_vorlage"}
        Dict["lieferschein_id"] = str(lieferschein_id)
        Dict["rechnung_id"] = str(rechnung_id)
        data = send_and_receive(ip, secure_key, Dict)
        print("add_lieferschein_to_vorlage(" + str(lieferschein_id) + ", " + str(rechnung_id) + ") = " + str(data))
        return data
    except:
        print("add_lieferschein_to_vorlage(" + str(lieferschein_id) + ", " + str(rechnung_id) + ") = ERROR")
        return 0

def remove_lieferschein_from_vorlage(ip, secure_key, lieferschein_id, rechnung_id):  
    try:
        Dict = {"mode":"remove_lieferschein_from_vorlage"}
        Dict["lieferschein_id"] = str(lieferschein_id)
        Dict["rechnung_id"] = str(rechnung_id)
        data = send_and_receive(ip, secure_key, Dict)
        print("remove_lieferschein_from_vorlage(" + str(lieferschein_id) + ", " + str(rechnung_id) + ") = " + str(data))
        return data
    except:
        print("remove_lieferschein_from_vorlage(" + str(lieferschein_id) + ", " + str(rechnung_id) + ") = ERROR")
        return 0

def get_article_average(ip, secure_key, identification):  # return average per year (365days)
    try:
        Dict = {"mode":"get_article_average"}
        Dict["identification"] = str(identification)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_article_average(" + str(identification) + ") = " + str(data))
        return data
    except:
        print("get_article_average(" + str(identification) + ") = ERROR")
        return 0
    
def set_rechnung_rappel(ip, secure_key, rechnung_id):# return Bool of sucess
    try:
        Dict = {"mode":"set_rechnung_rappel"}
        Dict["identification"] = str(rechnung_id)
        data = send_and_receive(ip, secure_key, Dict)
        print("set_rechnung_rappel(" + str(rechnung_id) + ") = " + str(data))
        return data
    except:
        print("set_rechnung_rappel(" + str(rechnung_id) + ") = ERROR")
        return False
        
def get_invoice_e(ip, secure_key, rechnung_id):
    try:
        Dict = {"mode":"get_invoice_e"}
        Dict["identification"] = str(int(rechnung_id))
        data = send_and_receive(ip, secure_key, Dict)
        print("get_invoice_e(" + str(rechnung_id) + ") = " + str(data))
        return data
    except:
        print("get_invoice_e(" + str(rechnung_id) + ") = ERROR")
        return {}
        
def get_rechnung_e(ip, secure_key, rechnung_id):
    return get_invoice_e(ip, secure_key, rechnung_id)
     
def get_invoice(ip, secure_key, rechnung_id):
    try:
        Dict = {"mode":"get_invoice"}
        Dict["identification"] = str(rechnung_id)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_invoice(" + str(rechnung_id) + ") = " + str(data))
        return data
    except:
        print("get_invoice(" + str(rechnung_id) + ") = ERROR")
        return {}
        
def get_rechnung(ip, secure_key, rechnung_id):
    return get_invoice(ip, secure_key, rechnung_id)
      
def set_rechnung_e(ip, secure_key, rechnung_dict):
    try:
        Dict = rechnung_dict
        Dict["mode"] = "set_rechnung_e"
        data = send_and_receive(ip, secure_key, Dict)
        print("set_rechnung_e(" + str(rechnung_dict) + ") = " + str(data))
        return data
    except:
        print("set_rechnung_e(" + str(rechnung_dict) + ") = ERROR")
        return False

def create_rechnung_vorlage(ip, secure_key, lieferschein_id): 
    try:
        Dict = {"mode":"create_rechnung_vorlage"}
        Dict["lieferschein_id"] = str(lieferschein_id)
        data = send_and_receive(ip, secure_key, Dict)
        print("create_rechnung_vorlage(" + str(lieferschein_id) + ") = " + str(data))
        return data
    except:
        print("create_rechnung_vorlage(" + str(lieferschein_id) + ") = ERROR")
        return 0

def get_inventar(ip, secure_key, tag, monat, jahr, start):#return Dict
    print("get_inventar(" + str(tag) + ", " + str(monat) + ", " + str(jahr) + ", " + str(start) + ")")
    try:
        Dict = {"mode":"get_inventar"}
        Dict["tag"] = str(int(tag))
        Dict["monat"] = str(int(monat))
        Dict["jahr"] = str(int(jahr))
        Dict["start"] = int(start)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_inventar(" + str(tag) + ", " + str(monat) + ", " + str(jahr) + ", " + str(start) + ") = " + str(data))
        return data
    except:
        print("get_inventar(" + str(tag) + ", " + str(monat) + ", " + str(jahr) + ", " + str(start) + ") = ERROR")
        return {}

def getInventar(ip, secure_key, tag, monat, jahr, start):#return Dict
    return get_inventar(ip, secure_key, tag, monat, jahr, start)

def search_belt(ip, secure_key, min_thick, max_thick, min_length, max_length, index): # return next Article with BELT in Size and withing the range
    print("search_belt(" + str(min_thick) + ", " + str(max_thick) + ", " + str(min_length) + ", " + str(max_length) + "," + str(index) + ")")
    try:
        Dict = {"mode":"search_belt"}
        Dict["min_thick"] = str(int(min_thick))
        Dict["max_thick"] = str(int(max_thick))
        Dict["min_length"] = str(int(min_length))
        Dict["max_length"] = str(int(max_length))
        Dict["index"] = int(index)
        data = send_and_receive(ip, secure_key, Dict)
        print("search_belt(" + str(min_thick) + ", " + str(max_thick) + ", " + str(min_length) + ", " + str(max_length) + ") = " + str(data))
        return data
    except:
        print("search_belt(" + str(min_thick) + ", " + str(max_thick) + ", " + str(min_length) + ", " + str(max_length) + ") = ERROR")
        return {"identification": "-1"}

def add_inventar(ip, secure_key, bcode, anzahl, categorie, tag, monat, jahr):#return Bool
    try:
        Dict = {}
        Dict["mode"] = "add_inventar"
        Dict["bcode"] = str(bcode)
        Dict["anzahl"] = float(anzahl)
        Dict["categorie"] = int(categorie)
        Dict["tag"] = str(tag)
        Dict["monat"] = str(monat)
        Dict["jahr"] = str(jahr)
        data = send_and_receive(ip, secure_key, Dict)
        print("add_inventar(" + str(bcode) + ", " + str(anzahl) + ", " + str(tag) + ", " + str(monat) + ", " + str(jahr) + ") = " + str(data))
        if not data: os.system("aplay /etc/zk-data-client/qml/DATA/scan-no.wav &") # SERVER Say ERROR
        else: os.system("aplay /etc/zk-data-client/qml/DATA/scan-yes.wav &") # SERVER Say OK
        return data
    except:
        print("add_inventar(" + str(bcode) + ", " + str(anzahl) + ", " + str(tag) + ", " + str(monat) + ", " + str(jahr) + ") = False")
        return False
        
def addInventar(ip, secure_key, bcode, anzahl, tag, monat, jahr):#return Bool
    return add_inventar(ip, secure_key, bcode, anzahl, tag, monat, jahr)


def set_article(ip, secure_key, Dict):#return Bool
    try:
        Dict["mode"] = "set_article"
        data = send_and_receive(ip, secure_key, Dict)
        print("set_article(" + str(Dict) + ") = " + str(data))
        return data
    except:
        print("set_article(" + str(Dict) + ") = False")
        return False
        
def SetArt(ip, secure_key, Dict):#return Bool
    answer = set_article(ip, secure_key, Dict)
    return answer

def print_inventar(ip, secure_key, start_jahr, start_monat, start_tag, end_jahr, end_monat, end_tag, anwenden):#return Bool
    try:
        Dict = {}
        Dict["mode"] = "print_inventar"
        Dict["start_jahr"] = int(start_jahr)
        Dict["start_monat"] = int(start_monat)
        Dict["start_tag"] = int(start_tag)
        Dict["end_jahr"] = int(end_jahr)
        Dict["end_monat"] = int(end_monat)
        Dict["end_tag"] = int(end_tag)      
        Dict["anwenden"] = bool(anwenden)
        data = send_and_receive(ip, secure_key, Dict)
        print("print_inventar(" + str(Dict) + ") = " + str(data))
        return data
    except:
        print("print_inventar(" + str(Dict) + ") = False")
        return False
        
def get_ip_of_server(server_name):
    print("get_ip_of_server(" + str(server_name) + ")")
    server_list = get_server_list()
    for server in server_list:
        if server[0] == server_name:
            return server[1]
    return ""
    
def get_server_list():#return List
    print("get_server_list()")
    ip_extern = os.popen("curl --connect-timeout 4000 -4 icanhazip.com").readlines()
    if len(ip_extern) > 0:
        ip_extern = ip_extern[0].rstrip()
        print("ip_extern: " + str(ip_extern))
        try:
            answer = requests.get("https://zk-data.zaunz.org/get_server_list.php?ip_extern=" + str(ip_extern))
            print("answer.status_code: " + str(answer.status_code))
            if answer.status_code == 200:
                server_list = json.loads(answer.text)
                #server_list.append(["local", "127.0.0.1:PORT???"])
                libs.BlueFunc.setData("server_list", server_list)
            else:
                server_list = libs.BlueFunc.getData("server_list")
        except:
            server_list = libs.BlueFunc.getData("server_list")
    else:
        server_list = libs.BlueFunc.getData("server_list")
    
    #answer = requests.get("https://zk-data.zaunz.org/get_server_list.php")
    #server_list = json.loads(answer.text)
    #server_list = [["burkardt", "192.168.1.35"]]
    #print("server_list: " + str(server_list))
    return server_list
    
def get_server_name(ip, secure_key):#return Server Name
    try:
        Dict = {"mode":"get_server_name"}
        Dict["NO_ERROR_SENDING"] = True
        data = send_and_receive(ip, secure_key, Dict)
        print("get_server_name() = " + str(data))
    except:
        print("get_server_name() = ERROR")
        data = "error"
    return data

def create_backup(ip, secure_key):#create Backup File and return path
    try:
        Dict = {"mode":"create_backup"}
        data = send_and_receive(ip, secure_key, Dict)
        print("create_backup() = " + str(data))
    except:
        print("create_backup() = ERROR")
        data = ""
    return data
    
def get_server_version(ip, secure_key):#return Server Version
    try:
        Dict = {"mode":"get_server_version"}
        data = send_and_receive(ip, secure_key, Dict)
        print("get_server_version() = " + str(data))
    except:
        print("get_server_version() = ERROR")
        data = "error"
    return data

def get_all_predictions(ip, secure_key):
    try:
        Dict = {"mode":"get_all_predictions"}
        data = send_and_receive(ip, secure_key, Dict)
        print("get_all_predictions() = " + str(data))
    except:
        print("get_all_predictions() = ERROR")
        data = False
    return data
    
def make_prediction(ip, secure_key, prediction_name, word_data):
    try:
        Dict = {"mode":"make_prediction"}
        Dict["prediction_name"] = str(prediction_name)
        Dict["word_data"] = str(word_data)
        data = send_and_receive(ip, secure_key, Dict)
        print("make_prediction() = " + str(data))
    except:
        print("make_prediction() = ERROR")
        data = False
    return data

def add_to_prediction(ip, secure_key, prediction_name, answer, word_data):
    try:
        Dict = {"mode":"add_to_prediction"}
        Dict["prediction_name"] = str(prediction_name)
        Dict["answer"] = str(answer)
        Dict["word_data"] = str(word_data)
        data = send_and_receive(ip, secure_key, Dict)
        print("add_to_prediction() = " + str(data))
    except:
        print("add_to_prediction() = ERROR")
        data = False
    return data
            
def create_article(ip, secure_key, new_ID):#return Dict
    if True:#try:
        Dict = {"mode":"create_article"}
        Dict["identification"] = str(new_ID)
        data = send_and_receive(ip, secure_key, Dict)
        print("create_article() = " + str(data))
        return data
    if False:#except:
        print("create_article() = ERROR")
        return {}
            
def get_article_sell(ip, secure_key, identification, date):#return float
    try:
        Dict = {"mode":"get_article_sell"}
        Dict["identification"] = str(identification)
        Dict["date"] = str(date)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_article_sell() = " + str(data))
        return data
    except:
        print("get_article_sell() = ERROR")
        return 0.0
        
def GetID(ip, secure_key):#return Dict
    try:
        Dict = {"mode":"GetID"}
        data = send_and_receive(ip, secure_key, Dict)
        print("GetID() = " + str(data))
        return data
    except:
        print("GetID() = ERROR")
        return {}

def GetPrinter(ip, secure_key):#return Dict
    try:
        Dict = {"mode":"GetPrinter"}
        data = send_and_receive(ip, secure_key, Dict)
        print("GetPrinter() = " + str(data))
        return data
    except:
        print("GetPrinter() = ERROR")
        return {}

def add_article(ip, secure_key, ID, Anzahl):#return Bool of sucess
    try:
        Dict = {"mode":"add_article"}
        Dict["identification"] = str(ID)
        Dict["add"] = str(Anzahl)
        data = send_and_receive(ip, secure_key, Dict)
        print("add_article(" + str(ID) + ", " + str(Anzahl) + ") = " + str(data))
        return data
    except:
        print("add_article(" + str(ID) + ", " + str(Anzahl) + ") = ERROR")
        return False

def AddArt(ip, secure_key, ID, Anzahl):#return Bool of sucess
    return add_article(ip, secure_key, ID, Anzahl)
    
def get_best_clients(ip, secure_key, max_quantity, days):# return 10 best clients
    try:
        Dict = {"mode":"get_best_clients"}
        Dict["max_quantity"] = int(max_quantity)
        Dict["days"] = int(days)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_best_clients() = " + str(data))
        return data
    except:
        print("get_best_clients() = ERROR")
        return []

def search_article(ip, secure_key, Dict):# Give Dict with Search return List of IDs
    try:
        Suchen = False
        if "suche" in Dict:
            if len(Dict["suche"]) > 0: Suchen = True  
            EndString = ""
            for character in Dict["suche"]:
                if character.isalpha() or character.isdigit():
                    EndString = EndString + str(character)
            Dict["suche"] = EndString.upper()
        if "artikel" in Dict:
            if len(Dict["artikel"]) > 1: Suchen = True   
        if "lieferant" in Dict:
            Suchen = True   
        if "name" in Dict:
            if len(Dict["name"]) > 1: Suchen = True   
        if "ort" in Dict:
            Suchen = True             
        if Suchen:
            Dict["mode"]="search_article"
            data = send_and_receive(ip, secure_key, Dict)
            print("search_article(" + str(Dict) + ") = " + str(data))
            return data
        else:
            return []
    except:
        print("SearchArt(" + str(Dict) + ") = ERROR")
        return []
        
def SearchArt(ip, secure_key, Dict):# Give Dict with Search return List of IDs
    return search_article(ip, secure_key, Dict)

def get_cashbook(ip, secure_key, konto, datum):
    return get_cashbook_pdf(ip, secure_key, konto, datum)

def get_cashbook_pdf(ip, secure_key, konto, datum):
    try:
        Dict = {}
        Dict["mode"]="get_cashbook_pdf"
        Dict["konto"] = int(konto)
        Dict["datum"] = str(datum)
        data = send_and_receive(ip, secure_key, Dict)
        if data == True:
            data = "http://" + str(ip) + "/zk-data/cashbook/" + str(konto) + "-" + datum + ".pdf"
        return data
    except:
        print("get_cashbook_pdf(" + str(datum) + ") = ERROR")
        return False

def get_cashbook_date_before(ip, secure_key, konto, datum):
    try:
        Dict = {}
        Dict["mode"]="get_cashbook_date_before"
        Dict["konto"] = int(konto)
        Dict["datum"] = str(datum)
        data = send_and_receive(ip, secure_key, Dict)
        return data
    except:
        print("get_cashbook_date_before(" + str(datum) + ") = ERROR")
        return False

def get_pdfs_file(ip, secure_key):
    try:
        Dict = {}
        Dict["mode"]="get_pdfs_file"
        data = send_and_receive(ip, secure_key, Dict)
        return data
    except:
        print("get_pdfs_file(" + str(datum) + ") = ERROR")
        return ""

def get_cashbook_date_after(ip, secure_key, konto, datum):
    try:
        Dict = {}
        Dict["mode"]="get_cashbook_date_after"
        Dict["konto"] = int(konto)
        Dict["datum"] = str(datum)
        data = send_and_receive(ip, secure_key, Dict)
        return data
    except:
        print("get_cashbook_date_after(" + str(datum) + ") = ERROR")
        return False

def get_cashbook_date_last(ip, secure_key, konto):
    try:
        Dict = {}
        Dict["mode"]="get_cashbook_date_last"
        Dict["konto"] = int(konto)
        data = send_and_receive(ip, secure_key, Dict)
        return data
    except:
        print("get_cashbook_date_last() = ERROR")
        return False
        
        
def search_delivery(ip, secure_key, Dict):# Give Dict with Search return List of IDs
    try:
        Dict["mode"]="search_delivery"
        data = send_and_receive(ip, secure_key, Dict)
        try: print("search_delivery(" + str(Dict) + ") = " + str(data))
        except: print("search_delivery(" + str(Dict) + ") = <ERROR0>")
        return data
    except:
        print("search_delivery(" + str(Dict) + ") = ERROR")
        return []
        
def search_delivery_by_changed_date_and_total(ip, secure_key, datum, total):
    try:
        Dict = {}
        Dict["mode"]="search_delivery_by_changed_date_and_total"
        Dict["datum"] = str(datum)
        Dict["total"] = float(total)
        data = send_and_receive(ip, secure_key, Dict)
        try: print("search_delivery_by_changed_date_and_total(" + str(Dict) + ") = " + str(data))
        except: print("search_delivery_by_changed_date_and_total(" + str(Dict) + ") = <ERROR0>")
        return data
    except:
        print("search_delivery_by_changed_date_and_total(" + str(Dict) + ") = ERROR")
        return []

def search_client(ip, secure_key, Dict):# Give Dict with Search return List of IDs
    try:
        Dict["mode"]="search_client"
        data = send_and_receive(ip, secure_key, Dict)
        print("search_client(" + str(Dict) + ") = " + str(data))
        return data
    except:
        print("search_client(" + str(Dict) + ") = ERROR")
        return []

def SearchKunden(ip, secure_key, Dict):# Give Dict with Search return List of IDs
    return search_client(ip, secure_key, Dict)

def create_rechnung(ip, secure_key, rechnung_mode, vorlage_identification, pin):
    try:
        Dict = {}
        Dict["mode"] = "create_rechnung"
        Dict["vorlage_identification"] = int(str(vorlage_identification).replace("RV", ""))
        Dict["user_pin"] = str(pin).upper()
        Dict["rechnung_mode"] = str(rechnung_mode).upper()
        data = send_and_receive(ip, secure_key, Dict)
        print("create_rechnung(" + str(rechnung_mode) + ", " + str(vorlage_identification) + ", " + str(pin) + ") = " + str(data))
        return data
    except:
        print("create_rechnung() = ERROR")
        return []

def create_rechnung(ip, secure_key, rechnung_mode, vorlage_identification, pin):
    try:
        Dict = {}
        Dict["mode"] = "create_rechnung"
        Dict["vorlage_identification"] = int(str(vorlage_identification).replace("RV", ""))
        Dict["user_pin"] = str(pin).upper()
        Dict["rechnung_mode"] = str(rechnung_mode).upper()
        data = send_and_receive(ip, secure_key, Dict)
        print("create_rechnung(" + str(rechnung_mode) + ", " + str(vorlage_identification) + ", " + str(pin) + ") = " + str(data))
        return data
    except:
        print("create_rechnung() = ERROR")
        return []

def getUserData_from_key(ip, secure_key):# return Dict from User Key
    try:
        Dict = {}
        Dict["mode"]="getUserData_from_key"
        data = send_and_receive(ip, secure_key, Dict)
        print("getUserData_from_key(" + str(secure_key) + ") = " + str(data))
        return data
    except:
        print("getUserData_from_key() = ERROR")
        return []
        
def getUserData(ip, secure_key, name):# return Dict from User
    try:
        Dict = {}
        Dict["mode"]="getUserData"
        Dict["user"]=str(name)
        data = send_and_receive(ip, secure_key, Dict)
        print("getUserData(" + str(name) + ") = " + str(data))
        return data
    except:
        print("getUserData() = ERROR")
        return []

def clear_cache():
    os.system("rm -r /tmp/cache_zk-data_*")
    os.system("rm -r /tmp/zk-data-send-cache")

def set_to_cache(key, data):
    filename = "/tmp/cache_zk-data_" + str(key)[-1] + "/" + str(key)
    if not os.path.exists("/tmp/cache_zk-data_" + str(key)[-1]):
        os.mkdir("/tmp/cache_zk-data_" + str(key)[-1])

    data = json.dumps(data)
    open(filename, "w").write(data)

def get_from_cache(key):
    filename = "/tmp/cache_zk-data_" + str(key)[-1] + "/" + str(key)
    if os.path.exists(filename):
        data = open(filename, "r").read()
        data = json.loads(data)
        return data
    else:
        return ""

def search_invoice(ip, secure_key, identification, kunde, rest, datum, index):
    try:
        Dict = {}
        Dict["mode"]="search_invoice"
        Dict["identification"]=str(identification).rstrip()
        Dict["kunde"]=str(kunde).rstrip()
        Dict["rest"]=bool(rest)
        Dict["datum"]=str(datum)
        Dict["index"]=int(index)
        data = send_and_receive(ip, secure_key, Dict)
        print("search_invoice(" + str(identification) + ", " + str(kunde) + ", " + str(rest) + ", " + str(datum) + ", " + str(index) + ") = " + str(data))
        return data
    except:
        print("search_invoice() = ERROR")
        return -1
        
def search_rechnung(ip, secure_key, identification, kunde, rest, datum, index):
    return search_invoice(ip, secure_key, identification, kunde, rest, datum, index)

def search_invoice_e(ip, secure_key, identification, dokument_id, lieferant, rest, datum, index):
    try:
        Dict = {}
        Dict["mode"]="search_invoice_e"
        Dict["identification"]=str(identification).rstrip()
        Dict["dokument_id"]=str(dokument_id).rstrip()
        Dict["lieferant"]=str(lieferant).rstrip()
        Dict["rest"]=bool(rest)
        Dict["datum"]=str(datum)
        Dict["index"]=int(index)
        data = send_and_receive(ip, secure_key, Dict)
        print("search_invoice_e(" + str(identification) + ", " + str(dokument_id) + ", " + str(lieferant) + ", " + str(datum) + ", " + str(index) + ") = " + str(data))
        return data
    except:
        print("search_invoice_e() = ERROR")
        return -1

def search_rechnung_e(ip, secure_key, identification, dokument_id, lieferant, rest, datum, index):
    return search_invoice_e(ip, secure_key, identification, dokument_id, lieferant, rest, datum, index)

def create_transaktion(ip, secure_key, konto, betrag, rechnung, pin_code):
    try:
        Dict = {}
        Dict["mode"]="create_transaktion"
        Dict["konto"]=int(konto)
        Dict["betrag"]=float(str(betrag).replace(",", "."))
        Dict["rechnung"]=str(rechnung)
        Dict["pin_code"]=str(pin_code).upper()
        data = send_and_receive(ip, secure_key, Dict)
        print("create_transaktion(" + str(konto) + ", " + str(betrag) + ", " + str(rechnung) + ", " + str(pin_code) + ") = " + str(data))
        return data
    except:
        print("create_transaktion() = ERROR")
        return []

def get_stats_gewinn(ip, secure_key, jahr):
    try:
        Dict = {}
        Dict["mode"]="get_stats_gewinn"
        Dict["jahr"]=str(jahr)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_stats_gewinn(" + str(jahr) + ") = " + str(data))
        return data
    except:
        print("get_stats_gewinn() = ERROR")
        return []

def get_stats_per_hour(ip, secure_key, date):
    try:
        Dict = {}
        Dict["mode"]="get_stats_per_hour"
        Dict["date"]=str(date)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_stats_per_hour(" + str(date) + ") = " + str(data))
        return data
    except:
        print("get_stats_per_hour(" + str(date) + ") = ERROR")
        return []

def get_stats_umsatz(ip, secure_key, date_start, date_end):
    try:
        Dict = {}
        Dict["mode"]="get_stats_umsatz"
        Dict["date_start"]=str(date_start)
        Dict["date_end"]=str(date_end)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_stats_umsatz(" + str(date_start) + ", " + str(date_end) +  ") = " + str(data))
        return data
    except:
        print("get_stats_umsatz() = ERROR")
        return []

def get_stats_clients(ip, secure_key, date_start, date_end):
    try:
        Dict = {}
        Dict["mode"]="get_stats_clients"
        Dict["date_start"]=str(date_start)
        Dict["date_end"]=str(date_end)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_stats_clients(" + str(date_start) + ", " + str(date_end) +  ") = " + str(data))
        return data
    except:
        print("get_stats_clients() = ERROR")
        return []

def get_stats_delivery(ip, secure_key, date_start, date_end):
    try:
        Dict = {}
        Dict["mode"]="get_stats_delivery"
        Dict["date_start"]=str(date_start)
        Dict["date_end"]=str(date_end)
        data = send_and_receive(ip, secure_key, Dict)
        print("get_stats_delivery(" + str(date_start) + ", " + str(date_end) +  ") = " + str(data))
        return data
    except:
        print("get_stats_delivery() = ERROR")
        return []

def search_transaktion_by_date(ip, secure_key, datum, index):
    try:
        Dict = {}
        Dict["mode"]="search_transaktion_by_date"
        Dict["datum"]=str(datum)
        Dict["index"]=str(index)
        data = send_and_receive(ip, secure_key, Dict)
        print("search_transaktion_by_date(" + str(datum) + ", " + str(index) + ") = " + str(data))
        return data
    except:
        print("search_transaktion_by_date() = ERROR")
        return []
        
def search_transaktion_by_rechnung(ip, secure_key, rechnung):
    try:
        Dict = {}
        Dict["mode"]="search_transaktion_by_rechnung"
        Dict["rechnung"]=str(rechnung)
        data = send_and_receive(ip, secure_key, Dict)
        print("search_transaktion_by_rechnung(" + str(rechnung) + ") = " + str(data))
        return data
    except:
        print("search_transaktion_by_rechnung() = ERROR")
        return []

def get_transaktion(ip, secure_key, identification):
    #set_cache(variable, data)
    variable = "get_transaktion(" + str(identification) + ")"
    data = get_cache(variable)
    if str(data).rstrip() == "":
        try:
            Dict = {}
            Dict["mode"]="get_transaktion"
            Dict["identification"]=int(identification)
            data = send_and_receive(ip, secure_key, Dict)
            print("get_transaktion(" + str(identification) + ") = " + str(data))
            if not str(data) == "-1":
                if not data == []:
                    set_cache(variable, data)
        except:
            print("get_transaktion() = ERROR")
            data = []            
    return data

def get_konto(ip, secure_key, index):# return Dict from Konto
    print("get_konto(" + str(index) + ")")
    try:
        Dict = {}
        Dict["mode"]="get_konto"
        Dict["index"]=int(index)
        data = send_and_receive(ip, secure_key, Dict)        
        print("get_konto(" + str(index) + ") = " + str(data))
        return data
    except:
        print("get_konto() = ERROR")
        return []

def setUserData(ip, secure_key, name, color):# return Bool of sucess
    try:
        Dict = {}
        Dict["mode"]="setUserData"
        Dict["name"]=str(name)
        Dict["color"]=str(color)
        data = send_and_receive(ip, secure_key, Dict)
        print("setUserData(" + str(name) + ") = " + str(data))
        return data
    except:
        print("setUserData() = ERROR")
        return []

def getUserList(ip, secure_key):# return List of User
    try:
        Dict = {}
        Dict["mode"]="getUserList"
        data = send_and_receive(ip, secure_key, Dict)
        print("getUserList() = " + str(data))
        return data
    except:
        print("getUserList() = ERROR")
        return []

def addUserToList(ip, secure_key, Username):# Give Username return Nothing
    try:
        Dict = {}
        Dict["mode"]="addUserToList"
        Dict["user"]=str(Username)
        data = send_and_receive(ip, secure_key, Dict)
        print("addUserToList(" + str(Username) + ") = " + str(data))
        return data
    except:
        print("addUserToList(" + str(Username) + ") = ERROR")
        return []
