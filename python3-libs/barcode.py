#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import socket
import os

try:
    from .BlueFunc import *
    from .ArtikelFormat import *
except:
    from BlueFunc import *
    from ArtikelFormat import *

sys.path.append("/etc/zk-data-libs/")
import libs.send

def PrintClient(identification, name, adresse, land, plz, ort, tva):
    print("PrintClient(" + str(identification) + ", " + str(name) + ", " + str(adresse) + ", " + str(land) + ", " + str(plz) + ", " + str(ort) + ", " + str(tva) + ")")

    if os.path.exists("/tmp/default-29mm-90mm/"): 
        os.system("rm -r /tmp/default-29mm-90mm/")
        time.sleep(0.1)
        
    os.system("cd /tmp/ && unzip -q /etc/zk-data-client/qml/DATA/barcode/client-default-29mm-90mm.ods -d /tmp/default-29mm-90mm/")
    daten = open("/tmp/default-29mm-90mm/content.xml", "r").read()            

    daten = daten.replace("@kunde_identification@", str(identification))
    daten = daten.replace("@kunde_name@", str(name))
    daten = daten.replace("@kunde_adr@", str(adresse))
    daten = daten.replace("@kunde_land@", str(land))
    daten = daten.replace("@kunde_plz@", str(plz))
    daten = daten.replace("@kunde_ort@", str(ort))
    daten = daten.replace("@kunde_tva@", str(tva))
    
    open("/tmp/default-29mm-90mm/content.xml", "w").write(daten)

    os.system("cd /tmp/default-29mm-90mm && zip -r -q client-default-29mm-90mm.ods * && libreoffice --convert-to pdf:writer_pdf_Export /tmp/default-29mm-90mm/client-default-29mm-90mm.ods")
    
    anzahl = 1
    
    printer_list = os.popen("lpstat -a").read()
    if not anzahl == 0:
        for x in range(0, int(anzahl)):
            if "QL-800" in printer_list:
                os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL-800 /tmp/default-29mm-90mm/client-default-29mm-90mm.pdf")
            else:
                if "QL800" in printer_list:
                    os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL800 /tmp/default-29mm-90mm/client-default-29mm-90mm.pdf")
                else:  
                    if "QL-600" in printer_list:
                        os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL-600 /tmp/default-29mm-90mm/client-default-29mm-90mm.pdf")
                    else:
                        if "QL600" in printer_list:
                            os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL600 /tmp/default-29mm-90mm/client-default-29mm-90mm.pdf")
                   
                
            time.sleep(0.5)
            
def PrintBarcode2(ip, secure_key, identification, name, preis, anzahl):
    print("PrintBarcode2(" + str(identification) + ", " + str(name) + ", " + str(preis) + ", " + str(anzahl) + ")")

    if os.path.exists("/tmp/default-29mm-90mm/"): 
        os.system("rm -r /tmp/default-29mm-90mm/")
        time.sleep(0.1)

    home_dir = libs.BlueFunc.getHomeDir()  
    print("home_dir: " + home_dir)      
    if os.path.exists(home_dir + "/.config/zk-data-client/DATA/barcode/default-29mm-90mm.odt"):
        os.system("cd /tmp/ && unzip -q " + home_dir + "/.config/zk-data-client/DATA/barcode/default-29mm-90mm.odt -d /tmp/default-29mm-90mm/")    
    else:   
        os.system("cd /tmp/ && unzip -q /etc/zk-data-client/qml/DATA/barcode/default-29mm-90mm.odt -d /tmp/default-29mm-90mm/")
    daten = open("/tmp/default-29mm-90mm/content.xml", "r").read()

    artikel = libs.send.get_article(ip, secure_key, identification)
    
    if "@ort@" in daten:
        ort = artikel["ort"]
        daten = daten.replace("@ort@", ort)
        
    if 'name="barcode"' in daten:
        print("Barcode image found, create new one with new barcode")
        image_name = daten.split('name="barcode"')[1]
        image_name = image_name.split('href="Pictures/')[1]
        image_name = image_name.split('.svg" ')[0]
        print("image_name: " + str(image_name))
                
        barcode = str(artikel["barcode"])
        
        cmd = "rm /tmp/default-29mm-90mm/Pictures/" + image_name + ".svg && barcode ean13 -b " + barcode + " -o /tmp/default-29mm-90mm/Pictures/" + image_name + ".svg"
        #cmd = "rm /tmp/default-29mm-90mm/Pictures/" + image_name + ".svg && barcode ean13 -b " + barcode + " -s -o /tmp/default-29mm-90mm/Pictures/" + image_name + ".svg"
        print("cmd: " + str(cmd))
        os.system(cmd)
           
    new_id = ""
    for char in str(identification):
        new_id = new_id + char
        test_float = float(len(new_id.replace(".", "")) / 2)
        test_int = int(test_float)
        if test_float == float(test_int):
            new_id = new_id + "."
    
    if new_id[-1] == ".":
        identification = new_id[:-1]
    else:
        identification = new_id      
            
    daten = daten.replace("@identification@", str(identification))
    daten = daten.replace("@name@", str(name))
    daten = daten.replace("@preis@", str(preis))
    
    daten = daten.replace("@artikel@", str(artikel["artikel"]))
    daten = daten.replace("@artikel2@", str(artikel["artikel2"]))
    daten = daten.replace("@artikel3@", str(artikel["artikel3"]))
    daten = daten.replace("@artikel4@", str(artikel["artikel4"]))
    
    open("/tmp/default-29mm-90mm/content.xml", "w").write(daten)

    os.system("cd /tmp/default-29mm-90mm && zip -r -q default-29mm-90mm.odt * && libreoffice --convert-to pdf:writer_pdf_Export /tmp/default-29mm-90mm/default-29mm-90mm.odt")
    
    printer_list = os.popen("lpstat -a").read()
    if not anzahl == 0:
        for x in range(0, int(anzahl)):
            if "QL-800" in printer_list:
                os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL-800 /tmp/default-29mm-90mm/default-29mm-90mm.pdf &")
            else:
                if "QL800" in printer_list:
                    os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL800 /tmp/default-29mm-90mm/default-29mm-90mm.pdf &")
                else:  
                    if "QL-600" in printer_list:
                        os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL-600 /tmp/default-29mm-90mm/default-29mm-90mm.pdf &")
                    else:
                        if "QL600" in printer_list:
                            os.system("cd /tmp/default-29mm-90mm && libreoffice --pt QL600 /tmp/default-29mm-90mm/default-29mm-90mm.pdf &")
                   
            time.sleep(0.5)

def CheckBarcode(Barcode):
    print("Check Barcode")
    print("Barcode " + str(Barcode))

    if Barcode[-13] + Barcode[-12] + Barcode[-11] + Barcode[-10] == "0000":
        print("Barcode Startswith " + Barcode[-13] + Barcode[-12] + Barcode[-11] + Barcode[-10])
        print("0000 Barcode False")
        return False
    else:
        EndInt = str(Barcode)[-1]
        Sum = 0
        for x in range(1, 13):
            if int(x / 2) == x / 2:
                print("a = 3")
                a = 3
            else:
                print("a = 1")
                a = 1
            Sum = Sum + int(Barcode[-13 + x]) * a
            print("Sum = " + str(Sum) + " + " + str(Barcode[-13 + x]) + ") * " + str(x))
        print("Sum " + str(Sum))
        End = str(10 - int(str(Sum)[-1]))[-1]
        print("End " + str(End))
        print("EndInt " + str(EndInt))
        if str(End) == str(EndInt):
            print("Barcode is True")
            return True
        else:
            print("Barcode is False")
            return True  ##False # Test geht nur wenn barcode von mie ist


def IDToBarcode(ID):
    print("ID To Barcode")
    copy_ID = ""
    for char in str(ID):
        if char.isdigit(): copy_ID = copy_ID + str(char)
        else: copy_ID = copy_ID + "0"
    ID = copy_ID
        
    
    while not len(str(ID)) == 6:
        ID = "0" + str(ID)
    
    Barcode = "123456" + str(ID)
    if len(str(ID)) == 6:
        #print("Barcode " + str(Barcode))
        Sum = 0
        for x in range(1, 13):
            if int(x / 2) == x / 2:
                #print("a = 3")
                a = 3
            else:
                #print("a = 1")
                a = 1
            Sum = Sum + int(Barcode[-13 + x]) * a
        #print("Sum = " + str(Sum) + " + " + str(Barcode[-13 + x]) + " * " + str(x))
        #print("Sum " + str(Sum))
        End = str(10 - int(str(Sum)[-1]))[-1]
        Barcode = str(Barcode) + str(End)
        print("Barcode " + str(Barcode))
    return str(Barcode)

def PrintBarcode(IP, ID, Barcode, Name, Price):
    print("Barcode")
    
    TCP_IP = getData("PrinterIP")  # ZBR5581684 ##### ZBR7681522
    TCP_PORT = 9100

    ID = str(ID)
    if len(ID) > 5:
        ID = ID[0] + ID[1] + "." + ID[2] + ID[3] + "." + ID[4] + ID[5]
            
    Barcode = str(int(Barcode))
    Name = str(Name)
    Price = str(Price) + " Euro"

    size = "^CF0,15"
    
    BarcodePos = "^FO140,5^BY2"
    BarcodeType = "^BEI,30,N,N"
    BarcodeString = "^FD" + Barcode + "^FS"
    Barcode = BarcodePos + BarcodeType + BarcodeString

    IDPos = "^FO120,10^BY2"
    IDText = "^ACB^FD" + ID + "^FS"  # ^AOB^FD
    ID = IDPos + IDText

    MaxStringLen = 15
    if len(Name) < MaxStringLen:
        NamePos = "^FO150,70^BY1"
        NameText = "^ACN^FD" + Name + "^FS"
        Name = NamePos + NameText
    else:
        Name1 = Name[0:MaxStringLen]
        Name2 = Name[MaxStringLen:len(Name)]
        NamePos1 = "^FO150,70^BY1"
        NameText1 = "^ACN^FD" + Name1 + "^FS"
        NamePos2 = "^FO150,100^BY1"
        NameText2 = "^ACN^FD" + Name2 + "^FS"
        Name = NamePos1 + NameText1 + NamePos2 + NameText2

    PricePos = "^FO160,40^BY1"
    PriceText = "^ACN^FD" + Price + "^FS"
    Price = PricePos + PriceText

    zpl = "^XA" + size + Barcode + ID + Name + Price + "^XZ"
    if TCP_IP == "CUPS":
        open("/tmp/zk-data-barcode", "w").write(zpl)
        os.system("lp -o raw /tmp/zk-data-barcode")
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.send(bytes(zpl, "utf-16"))
        s.close()
    print("Printed")

def PrintArtikel(Artikel, Lieferant):
    Artikel = str(Artikel).upper()
    Lieferant = str(Lieferant).upper()
    
    TCP_IP = getData("PrinterIP")  # ZBR5581684 ##### ZBR7681522
    TCP_PORT = 9100
    
    Name = ArtikelFormat(Artikel, Lieferant)

    MaxStringLen = 14
    if len(Artikel) < MaxStringLen:
        NamePos = "^FO140,40^BY1"
        NameText = "^ACN^FD" + Name + "^FS"
        Artikel = NamePos + NameText
    else:
        Name1 = Artikel[0:MaxStringLen]
        Name2 = Artikel[MaxStringLen:len(Artikel)]
        NamePos1 = "^FO140,40^BY1"
        NameText1 = "^ACN^FD" + Name1 + "^FS"
        NamePos2 = "^FO150,60^BY1"
        NameText2 = "^ACN^FD" + Name2 + "^FS"
        Artikel = NamePos1 + NameText1 + NamePos2 + NameText2

    zpl = "^XA" + Artikel + "^XZ"

    if TCP_IP == "CUPS":
        open("/tmp/zk-data-barcode", "w").write(zpl)
        os.system("lp -o raw /tmp/zk-data-barcode")
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.send(bytes(zpl, "utf-16"))
        s.close()

def PrintLocation(Location):
    Location = str(Location)
    
    TCP_IP = getData("PrinterIP")  # ZBR5581684 ##### ZBR7681522
    TCP_PORT = 9100

    #BarcodePos = "^FO150,10^BY1"
    #BarcodeType = "^B3,Y,40,N,N"
    #BarcodeString = "^FD" + Location + "^FS"
    #Barcode = BarcodePos + BarcodeType + BarcodeString

    setStringSize(40)

    StringPos = "^FO120,50^BY1"
    StringText = "^ACN^FD" + Location + "^FS"

    String = StringPos + StringText

    zpl = "^XA" + String + "^XZ"

    if TCP_IP == "CUPS":
        open("/tmp/zk-data-barcode", "w").write(zpl)
        os.system("lp -o raw /tmp/zk-data-barcode")
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.send(bytes(zpl, "utf-16"))
        s.close()
    
    setStringSize(5)

def setStringSize(Size):
    Size = str(Size)
    
    TCP_IP = getData("PrinterIP")  # ZBR5581684 ##### ZBR7681522
    TCP_PORT = 9100

    StringSize = "^CFA," + Size + "^FS"
    StringText = "^FO50,340^FD100 Disney Rules^FS"
    String = StringSize #+ StringText

    zpl = "^XA" + String + "^XZ"

    if TCP_IP == "CUPS":
        open("/tmp/zk-data-barcode", "w").write(zpl)
        os.system("lp -o raw /tmp/zk-data-barcode")
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.send(bytes(zpl, "utf-16"))
        s.close()


def PrintNoteBarcode(NUMBER, NAME, LINE, DATE):
    print("Barcode")

    NUMBER = str(NUMBER)
    NAME = str(NAME)
    LINE = str(LINE)
    DATE = str(DATE)
    
    TCP_IP = getData("PrinterIP")  # ZBR5581684 ##### ZBR7681522
    TCP_PORT = 9100

    #BarcodePos = "^FO140,5^BY2"
    #BarcodeType = "^BEI,30,N,N"
    #BarcodeString = "^FD" + Barcode + "^FS"
    #Barcode = BarcodePos + BarcodeType + BarcodeString

    IDPos = "^FO120,40^BY1"
    IDText = "^ACB^FD" + NUMBER + "^FS"  # ^AOB^FD
    ID = IDPos + IDText

    NamePos = "^FO160,10^BY1"
    NameText = "^ACN^FD" + NAME + "^FS"
    Name = NamePos + NameText

    NotePos = "^FO160,50^BY1"
    NoteText = "^ACN^FD" + LINE + "^FS"
    Note = NotePos + NoteText

    DatePos = "^FO160,90^BY1"
    DateText = "^ACN^FD" + DATE + "^FS"
    Date = DatePos + DateText

    zpl = "^XA" + ID + Name + Note + Date + "^XZ"
    if TCP_IP2 == "CUPS":
        open("/tmp/zk-data-barcode", "w").write(zpl)
        os.system("lp -o raw /tmp/zk-data-barcode")
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP2, TCP_PORT))
        s.send(bytes(zpl, "utf-16"))
        s.close()
        
        
def PrintBigOrt(t1, t2, t3, Ort):
    print("PrintBigOrt")
    
    TCP_IP = getData("PrinterIP")  # ZBR5581684 ##### ZBR7681522
    TCP_PORT = 9100

    Ort = str(Ort)
    
    t_size = "^CF0,80"
    
    t1_pos = "^FO40,0^BY1"#40, 140, 240
    t1_text = "^ACB^FD" + t1 + "^FS"
    t1 = t_size + t1_pos + t1_text
    
    t2_pos = "^FO140,0^BY1"#20, 120, 220
    t2_text = "^ACB^FD" + t2 + "^FS"
    t2 = t_size + t2_pos + t2_text
    
    t3_pos = "^FO240,0^BY1"#20, 120, 220
    t3_text = "^ACB^FD" + t3 + "^FS"
    t3 = t_size + t3_pos + t3_text

    OrtSize = "^CF0,60"
    OrtPos = "^FO350,0^BY1"
    OrtText = "^ACB^FD" + Ort + "^FS"
    Ort = OrtSize + OrtPos + OrtText
    
    zpl = "^XA" + t1 + t2 + t3 + Ort + "^XZ"
    if TCP_IP == "CUPS":
        open("/tmp/zk-data-barcode", "w").write(zpl)
        os.system("lp -o raw /tmp/zk-data-barcode")
    else:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((TCP_IP, TCP_PORT))
        s.send(bytes(zpl, "utf-16"))
        s.close()
        
        
       

def readBarcode(directory):
    print("readBarcode(" + directory + ")")

    answer = os.popen("zbarimg " + directory).readlines()
    answer = answer[0].rstrip()
    answer = answer.split(":")[1]

    if answer[0] == "0": answer = answer[1:]

    print("[" + str(answer) + "]")

    return answer
