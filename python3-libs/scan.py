#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

import os
import time

def get_scanner():
    scanner_list = []    
    scanner_lines = os.popen("scanimage -L").readlines()
    
    for line in scanner_lines:
        line = str(line).rstrip()
        #print("line: " + str(line))
        
        if "device " in line:
            #line = line.replace("device ", "").split(" ")[0]
            #line = line.replace("'", "").replace("`", "")
            line = line.split('`')[1]
            line = line.split("'")[0]
            scanner_list.append(line)
           
    print("scanner_list: " + str(scanner_list)) 
    return scanner_list     

def scan_to_pdf(scanner_name, datei):
    print("scan_to_pdf(" + str(scanner_name) + ", " + str(datei))
    if not ".pdf" in datei.lower():
        datei = datei + ".pdf"
       
    counter = 0
    color = True
    while True: 
        if color:
            os.system("scanimage -d '" + str(scanner_name) + "' --mode Color --resolution 250 --format=jpeg > /tmp/scannedfile.jpeg && sleep 1 && convert /tmp/scannedfile.jpeg " + str(datei))
        else:
            print("Print without color")
            os.system("scanimage -d '" + str(scanner_name) + "' --format=jpeg > /tmp/scannedfile.jpeg && sleep 1 && convert /tmp/scannedfile.jpeg " + str(datei))
            
            
        if os.path.exists(datei):
            break
        else:
            print("Scanning doesn't work ?")
            counter = counter + 1
            if counter > 5:
                datei = "/etc/zk-data/assets/scan.png"
                break
            else:
                print("Retry new scan in 2s...")
                color = False
                time.sleep(2)
    
    
def scan_to_png(scanner_name, datei):
    print("scan_to_png(" + str(scanner_name) + ", " + str(datei))
    if not ".png" in datei.lower():
        datei = datei + ".png"
        
    counter = 0
    color = True    
    while True: 
        if color:
            os.system("scanimage -d '" + str(scanner_name) + "' --mode Color --resolution 250 --format=png > /tmp/scannedfile.png && sleep 1 && mv /tmp/scannedfile.png " + str(datei))
        else:
            print("Print without color")
            os.system("scanimage -d '" + str(scanner_name) + "' --format=png > /tmp/scannedfile.png && sleep 1 && mv /tmp/scannedfile.png " + str(datei))
            
        if os.path.exists(datei):
            break
        else:
            print("Scanning doesn't work ?")
            counter = counter + 1
            if counter > 5:
                datei = "/etc/zk-data/assets/scan.png"
                break
            else:
                print("Retry new scan in 2s...")
                color = False
                time.sleep(2)
            
