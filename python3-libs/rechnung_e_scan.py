#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs/")

import os
import libs.send

import time

# tesseract ./image /tmp/zk-data-tesseract.txt -l eng txt

def tesseract_txt(image_files):
    print("tesseract_txt(" + str(image_files) + ")")
    
    answer_file = ""
    
    os.system("rm /tmp/zk-data-tesseract.txt")
    os.system("rm /tmp/zk-data-tesseract-total.txt")
    
    
    for image in image_files:
        counter = 0
        while counter < 5:
            print("Converting image to file: " + str(counter) + "/5")
            
            os.system("tesseract \"" + image + "\" /tmp/zk-data-tesseract --psm 6 -l eng txt") 
            
            if os.path.exists("/tmp/zk-data-tesseract.txt"):
                answer_file = "/tmp/zk-data-tesseract.txt"
                
                open("/tmp/zk-data-tesseract-total.txt", "a").write(open("/tmp/zk-data-tesseract.txt", "r").read())
                
                counter = 6
            
            counter = counter + 1 
    
    answer_file = "/tmp/zk-data-tesseract-total.txt"
    
    return answer_file
    
def scan(txt_file):
    txt_file = str(txt_file)
    print("rechnung_e_scan -> scan(" + txt_file + ")")
    
    text_data = open(txt_file, "r").read()
    text_lines = open(txt_file, "r").readlines()
    
    lieferant = "?"
    datum = ""
    document_id = ""
    total = 0.0
    mwst = 0.0
    
    #print("text_data: " + str(text_data))
    
    if "KRAMP" in text_data:
        lieferant = "kramp"
        
        for line in text_lines:
            if "Date d'échéance" in line:
                total = line.split(" ")[-1]   
                total = total.replace(",", ".")
                total = float(total)                
                
            if "No. Facture " in line:
                d_text = line.split(" ")[-1]
                d_text = d_text.split("-")                
                datum = "20" + d_text[2] + "-" + d_text[1] + "-" + d_text[0]
                
                document_id = line.split("No. Facture ")[-1]
                document_id = document_id.split(" Date")[0]
                document_id = document_id.split(" ")[-1]
                
            if "TVA 21% de " in line:
                mwst = line.split(" ")[-1]
                mwst = mwst.replace(",", ".").rstrip()
                while True:
                    print("mwst: " + str(mwst))
                    if mwst[-1].isdigit():
                        break
                    else:
                        if mwst == "":
                            mwst = 0.0
                            break  
                        else:
                            mwst = mwst[:-1]
                    
                        
                
                mwst = float(mwst)
            
    elif "STIHL" in text_data:
        lieferant = "stihl"
        
        for line in text_lines:
            # Date du document 25.03.2021
            if "Date du document " in line and not "Commande" in line and datum == "":
                d_text = line.replace("Date du document ", "")
                d_text = d_text.split(" ")[0]
                d_text = d_text.split(".")
                datum = d_text[2] + "-" + d_text[1] + "-" + d_text[0]
             
            # Numéro du document 905753173
            if "Numéro du document " in line:
                document_id = line.split(" ")[-1]
                document_id = document_id.rstrip()
                
            # Montant total T.T.C. : EUR 32,67
            if "Montant total T.T.C." in line:
                total = line.split("EUR")[-1]
                total = total.replace(",", ".").replace(" ", "")
                total = float(total)
                
            #TVA:B3  21,0% EUR 5,67
            if "TVA:B" in line:
                mwst = line.split("EUR")[-1]
                mwst = mwst.replace(",", ".").replace(" ", "")
                mwst = float(mwst)
             
        
    elif "NRG" in text_data:
        lieferant = "nrg"
        
        for line in text_lines:
        
            # CoC:64918114 Factuurdatum 29-03-2021
            if "Factuurdatum" in line:
                d_text = line.split(" Factuurdatum ")[-1]
                d_text = d_text.replace(" ", "").split("-")
                datum = d_text[2] + "-" + d_text[1] + "-" + d_text[0]
                
            # RABONL2U Factuurnummer 21710566        
            if "Factuurnummer" in line:
                document_id = line.split(" Factuurnummer ")[-1]
                document_id = document_id.replace(" ", "").rstrip()
             
            # Totaal exclusief BIW € 1.188,00
            if "Totaal exclusief BIW" in line:
                total = line.split("€")[-1]
                total = total.replace(" ", "").replace(".", "").replace(",", ".")
                
            mwst = 0.0
        
    elif "Giant Benelux" in text_data:
        lieferant = "giant"
        
        for line in text_lines:
            datum = ""
            document_id = ""
            
            # 0% 2.563,09 ») 2.572,42 EUR
            if "0%" in line and "EUR" in line:
                total = line.replace("0% ", "")
                total = total.rstrip().split(" ")[0]
                total = total.replace(".", "").replace(",", ".")
                total = float(total)
                mwst = 0.0
        
    elif "ZEG" in text_data:
        lieferant = "zeg"
        
        for line in text_lines:  
            # Date d.facture 19.04.2021
            if "Date d.facture " in line:
                d_text = line.rstrip().split(" ")[-1]
                d_text = d_text.split(".")
                datum = d_text[2] + "-" + d_text[1] + "-" + d_text[0]
            
            # N° de facture 1939426
            if "N° de facture " in line:
                document_id = line.strip().split(" ")[-1]
            
            total = 0.0
            mwst = 0.0
        
    elif "BE47 5230 8120 8980" in text_data: # = ZAUNZ
        lieferant = "zaunz"
        
        for line in text_lines:        
            # Date facture: 2021-02-27
            if "Date facture: " in line:
                datum = line.rstrip().split(" ")[-1]
            
            # Facture n° 21 BE 4950 SOURBRODT
            if "Facture n° " in line:
                document_id = line.split("Facture n° ")[-1]
                document_id = document_id.split(" ")[0]
            
            # Total 324.0 €
            if "Total " in line and not "HTVA" in line:
                total = line.replace("Total ", "").replace(" €", "")
                total = float(total)
            
            # TVA 6% 00€
            # TVA 21% 56,23 €
            if "TVA 6% " in line or "TVA 21% " in line:
                tva_to_add = line.rstrip().split("% ")[-1]
                tva_to_add = tva_to_add.replace(",", ".").replace("€", "").rstrip()
                mwst = mwst + float(tva_to_add)
                
    elif "GROUPE VLAN S.A." in text_data or "REFERENCES S.A." in text_data:
        if "GROUPE VLAN S.A." in text_data:
            lieferant = "groupe vlan"
            
            # Contact Facture : facturation@vlan.be
            # 12/04/2021
            # FACTURE BURKARDT SPRL   
            d_text = text_data.lower().split("facture ")[1]
            d_text = d_text.split("\n")[0].rstrip()
            d_text = d_text.split("/")
            datum = d_text[2] + "-" + d_text[1] + "-" + d_text[0]
        
        if "REFERENCES S.A." in text_data:
            lieferant = "references s.a."
            
            d_text = text_data.lower().split("facture ")[0]
            d_text = d_text.split("\n")[-2].rstrip()
            d_text = d_text.split("/")
            datum = d_text[2] + "-" + d_text[1] + "-" + d_text[0]
        
            for line in text_lines:
                # N° 100155995 4950 Sourbrodt
                if "N° " in line and not "CLIENT" in line and not "TVA" in line and not "FINANCE" in line:
                    document_id = line.replace("N° ", "").split(" ")[0]
                
                # TOTAL TTC 233,14 EUR
                if "TOTAL TTC " in line:
                    total = line.split("TTC ")[-1].split(" EUR")[0]
                    total = total.replace(",", ".")
                    total = float(total)
                
                # MONTANT DE LA TVA 21% 40,46 EUR
                if "MONTANT DE LA TVA" in line:
                    mwst = line.split("TVA 21% ")[-1].split(" EUR")[0]
                    mwst = mwst.replace(",", ".")
                    mwst = float(mwst)
                
                
    elif "HILAIRE VAN DER HAE" in text_data:
        lieferant = "hilaire van der haeghe"
            
        for line in text_lines:                
            datum = ""
            document_id = ""
            total = 0.0
            mwst = 0.0
            
    elif "HARTJE" in text_data:
        lieferant = "hartje"
            
        for line in text_lines:                
            datum = ""
            document_id = ""
            total = 0.0
            mwst = 0.0
            
    elif "Herco" in text_data:
        lieferant = "herco machinery"
            
        for line in text_lines:                
            datum = ""
            document_id = ""
            total = 0.0
            mwst = 0.0
        
            
    elif "Dutch Toys Group BV" in text_data:
        lieferant = "dutch toys"
            
        for line in text_lines: 
            # Rechnungsnummer : 218973304 Rechnungsdatum : 30-03-2021     
            if "Rechnungsdatum : " in line:                      
                d_text = line.rstrip().split("Rechnungsdatum : ")[-1]
                d_text = d_text.split("-")
                datum = d_text[2] + "-" + d_text[1] + "-" + d_text[0]
                
            # Rechnungsnummer : 218973304 Rechnungsdatum : 30-03-2021     
            if "Rechnungsnummer : " in line:                      
                document_id = line.rstrip().split("Rechnungsnummer : ")[-1].split(" Rechnungsdatum")[0]
                document_id = document_id.rstrip()
            
            # Rechnungsbetrag 1.358,86
            if "Rechnungsbetrag " in line:
                total = line.split("Rechnungsbetrag ")[-1].rstrip()
                total = total.replace(".", "").replace(",", ".")
                total = float(total)
                
            mwst = 0
        
    elif "REMONDIS W.C. DECHETS 2000 SA" in text_data:
        lieferant = "remondis"
            
        for line in text_lines: 
            # Date facture: 31.08.2021
            if "Date facture: " in line:
                d_text = line.rstrip().split(": ")[-1]
                d_text = d_text.split(".")
                datum = d_text[2] + "-" + d_text[1] + "-" + d_text[0]
            
            # n° facture: 1008037
            if "n° facture: " in line:
                document_id = line.rstrip().split(": ")[-1]
            
            # TVA 21% 7,50 21,0 1,58 9,08 EUR
            if "TVA 21% " in line:
                total = line.rstrip().split(" EUR")[0]
                total = total.split(" ")[-1]
                total = total.replace(",", ".")
                total = float(total)
                
                mwst = line.rstrip().split(" EUR")[0]
                mwst = mwst.split(" ")[-2]
                mwst = mwst.replace(",", ".")
                mwst = float(mwst)
            
        
    else:
    
        datum = ""
        document_id = ""
        total = 0.0
        mwst = 0.0
    
        True
        
    answer = [lieferant, datum, document_id, total, mwst]
    print("answer: " + str(answer))
    return [lieferant, datum, document_id, total, mwst]
    
    
