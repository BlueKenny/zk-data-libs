#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.BlueFunc

import os
import requests

def upload(name, datei):
    print("upload(" + str(name) + ", " + str(datei) + ")")
    
    name = str(name).upper()
    
    datum = libs.BlueFunc.Date().split("-")
    year = datum[0]
    month = datum[1]
    day = datum[2]

    if os.path.exists(datei):
        url = 'https://zk-data-backup.zaunz.org/upload.php'

        try:
            response = requests.post( url, data={'user': name, "year": year, "month": month, "day": day}, files={'datei': open(datei,'rb') } )

        except:
            print("Error, upload not possible")
        
        #print(response.content)
        
        #link = str(response.content).split('>https://zk-data-backup.zaunz.org/files/')[-1]
        #link = link.split("</a>")[0]
        #link = "https://zk-data-backup.zaunz.org/files/" + link
        #print("link: " + link)
        #print(response.status_code)
        
        #os.system("echo \"" + link + "\" | xclip -selection c")
        #os.system("zenity --info --text \"Datei erfolgreich hochgeladen\"")
    else:
        print("datei existiert nicht")

def upload_pdf_s(name, datei):
    print("upload_pdf_s(" + str(name) + ", " + str(datei) + ")")
    
    name = str(name).upper()    

    if os.path.exists(datei):
        url = 'https://zk-data-backup.zaunz.org/upload_pdf_s.php'

        try:
            response = requests.post(url, data={'user': name}, files={'datei': open(datei,'rb')}, timeout=10)
        except:
            print("Error, upload not possible")       
    else:
        print("datei existiert nicht")
        
def upload_pdf_e(name, datei):
    print("upload_pdf_e(" + str(name) + ", " + str(datei) + ")")
    
    name = str(name).upper()    

    if os.path.exists(datei):
        url = 'https://zk-data-backup.zaunz.org/upload_pdf_e.php'

        try:
            response = requests.post(url, data={'user': name}, files={'datei': open(datei,'rb')}, timeout=10)
        except:
            print("Error, upload not possible")       
    else:
        print("datei existiert nicht")
