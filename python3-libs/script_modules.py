#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")
import os

import importlib

# import script modules
script_modules = {}
sys.path.append("/etc/zk-data-client/script/enabled/")

for script_file in os.listdir("/etc/zk-data-client/script/enabled/"):
    if ".py" in script_file:
        print("loading script_module: " + script_file)
        script_modules[script_file.replace(".py", "")] = importlib.import_module(script_file.replace(".py", ""))

def run_scripts(ip, secure_key, article_dict):
    ### script_modules ###
    for script_module in script_modules:
        print("run script_module: " + script_module)
        article_dict = script_modules[script_module].script(ip, secure_key, article_dict)

    return article_dict
