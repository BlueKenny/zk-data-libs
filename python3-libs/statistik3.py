#!/usr/bin/env python3


def create_stats(data):
    url = "/tmp/zk-data-statstik3.html"    

    html_data = open("/etc/zk-data-libs/libs/statistik3.html", "r").read()
    
    for x in range(0, 12):        
        html_data = html_data.replace("<g" + str(x+1) + "></g" + str(x+1) + ">", str(data["gewinn"][x]))
        html_data = html_data.replace("<a" + str(x+1) + "></a" + str(x+1) + ">", str(data["ausgaben"][x]))
        html_data = html_data.replace("<u" + str(x+1) + "></u" + str(x+1) + ">", str(data["umsatz"][x]))
    
    open(url, "w").write(html_data)
    return url

