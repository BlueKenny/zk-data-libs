#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc
import datetime
import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)


file_data = []

predictions = libs.send.get_all_predictions(ip, secure_key)

#new_line = []
#for prediction_titles in predictions[0]:
#    new_line.append(str(prediction_titles))
#file_data.append(";".join(new_line))

for prediction_name in predictions:
    print("prediction_name: " + str(prediction_name))
    new_line = []
    new_line.append(str(prediction_name))
    for column_name in predictions[prediction_name]:
        new_line.append(str(predictions[prediction_name][column_name]))
    file_data.append(";".join(new_line))

open("/tmp/zk_data_get_all_predictions.csv", "w").write("\n".join(file_data))
os.system("libreoffice /tmp/zk_data_get_all_predictions.csv")
