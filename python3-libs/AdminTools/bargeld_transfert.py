#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

import os

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

try: import pick
except:
    os.system("pip3 install --user pick --break-system-packages")
    import pick
    
print("Bargeld Transaktion erstellen")

datum = str(libs.BlueFunc.Date())
print("Datum der transaktion: " + datum + "\n")
uhrzeit = str(libs.BlueFunc.uhrzeit())
print("Uhrzeit der transaktion: " + uhrzeit + "\n")



betrag = input("Betrag der aus der Kasse auf ein Konto gesetzt wurde: (+) ")
if "," in betrag: betrag = betrag.replace(",", ".")
betrag = "-" + betrag
betrag = str(float(betrag))

betrag_check = pick.pick(["NEIN", "JA"], "Betrag der aus der Kasse genommen wurde: " + betrag, indicator='-> ')[0]
if betrag_check == "JA":
    index = 1
    konten = []
    while True:
        konto = libs.send.get_konto(ip, secure_key, index)
        
        if str(konto) == "-1":
            break
        else:
            # enable
            if konto["enable"]:
                konten.append(str(konto["identification"]) + ": " + str(konto["name"]))
            index = index + 1
            
    konto_in_picker = pick.pick(konten, "Auf weches Konto wurde das Bargeld eingezahlt ? ", indicator='-> ')[0]
    konto_in = "1"
    konto_name = str(konto_in_picker).split(": ")[1]

    check_string = "\n" + datum + " " + uhrzeit + "\n\n" + betrag + "€ aus der Kasse genommen und auf " + konto_name + " eingezahlt ?"

    notiz = input("Notiz: ")

    check = pick.pick(["NEIN / Abbrechen", "JA / Transaktion ausführen"], "Transaktion erstellen ? \n" +  check_string, indicator='-> ')[0]

    if "JA" in check:
        answer = libs.send.create_transaktion_free(ip, secure_key, datum, uhrzeit, betrag, konto_in, "", "Transfert von Kasse auf " + konto_name + " " + notiz)

        if str(answer) == "1":
            answer_text = "Transaktion erfolgreich erstellt"
        else:
            answer_text = "Fehler !!! Transaktion wurde nicht erstellt"
        
        t = pick.pick(["Schliessen"], answer_text, indicator='-> ')[0]
