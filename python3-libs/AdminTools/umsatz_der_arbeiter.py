#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

liste_der_arbeiter = ["1JM", "1JP", "1G", "1J", "1R", "1D", "1TR"]
liste_der_arbeiter = ["207", "208", "204", "205", "209", "203", "210"]
umsatz_pro_arbeiter = {}

for x in liste_der_arbeiter:
    umsatz_pro_arbeiter[x] = 0.0

datum = "2023-11"

index = 0

while True:
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "", "", False, datum, index)

    if "RV" in rechnung_id: break

    rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)
    for lieferschein_id in str(rechnung["lieferscheine"]).split("|"):
        if not str(lieferschein_id) == "":
            lieferschein = libs.send.get_delivery(ip, secure_key, lieferschein_id)
        
            for x in range(0, len(lieferschein["bcode"].split("|"))):
                bcode = lieferschein["bcode"].split("|")[x]
                try: preis = float(lieferschein["preis"].split("|")[x])
                except: preis = 0.0
                
                if bcode in liste_der_arbeiter:
                    umsatz_pro_arbeiter[bcode] = round(umsatz_pro_arbeiter[bcode] + preis, 2)
    
    index = index + 1
    

open("/tmp/umsatz_der_arbeiter.csv", "a").write("\n\n" + datum + "\n" + str(umsatz_pro_arbeiter))
os.system("gnome-text-editor /tmp/umsatz_der_arbeiter.csv &")
    
