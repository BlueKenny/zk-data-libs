#!/usr/bin/env python3


import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import requests

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

liste = []
datum = libs.BlueFunc.Date()
uhrzeit = libs.BlueFunc.uhrzeit()
liste.append(";;" + str(datum) + " " + str(uhrzeit) + "\n")
liste.append("L n°;datum;kunde;maschine;Einkaufswert;Verkaufswert ohne Steuern;Verkaufswert mit Steuern;user")

this_year = int(libs.BlueFunc.Date().split("-")[0])
article_cache = {}

for year in range(2020, this_year+1):
    deliverys_path = libs.send.get_deliverys_file(ip, secure_key, str(year))
    deliverys = requests.get("http://" + ip.split(":")[0] + deliverys_path).json()
    for delivery in deliverys:
        if "RV" in delivery["rechnung"] or delivery["rechnung"] == "":
        
            if not bool(delivery["angebot"]) and not bool(delivery["bestellung"]):
                print(delivery)
                
                ek = 0.0
                
                index = 0
                for bcode in delivery["bcode"].split("|"):
                    if not bcode == "":
                        anzahl = float(delivery["anzahl"].split("|")[index])
                        if bcode in article_cache:
                            ek += article_cache[bcode] * anzahl
                        else:
                            new_article = libs.send.get_article(ip, secure_key, bcode)
                            article_cache[bcode] = new_article["preisek"]
                            ek += new_article["preisek"] * anzahl
                    index += 1
                            
                ek = str(round(ek, 2)).replace(".", ",")
                
                vkh = str(round(delivery["total_htva"],2)).replace(".", ",")
                vk = str(round(delivery["total"],2)).replace(".", ",")
                
                delivery["kunde_name"] = delivery["kunde_name"][:35]
                liste.append(str(delivery["identification"]) + ";" + delivery["datum"] + ";" + delivery["kunde_name"] + ";" + delivery["maschine"] + ";" + str(ek) + ";" + str(vkh) + ";" + str(vk) + ";" +  delivery["user"])

'''
for index in range(0, 200):
    delivery = libs.send.get_delivery_not_invoiced(ip, secure_key, index)

    if delivery == {}: break
    else:
        delivery["kunde_name"] = delivery["kunde_name"][:35]
        liste.append(str(delivery["identification"]) + ";" + delivery["datum"] + ";" + delivery["kunde_name"] + ";" + delivery["maschine"] + ";" + str(delivery["total"]) + ";" +  delivery["user"])
    
'''
        
open("/tmp/zk-data_alte_offene_lieferscheine.csv", "w").write("\n".join(liste))
os.system("libreoffice /tmp/zk-data_alte_offene_lieferscheine.csv")
