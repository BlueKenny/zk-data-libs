#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc
import datetime
import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)
if not ":51515" in ip: ip = ip + ":51515"


print("Backup wird erstellt...")
pdf_path = libs.send.create_backup(ip, secure_key)

server_name = libs.send.get_server_name(ip, secure_key)
os.system("mkdir -p /tmp/" + server_name + "/Backup")

backup_file_name = pdf_path.split("/")[-1]

os.system("wget http://" + ip + "/" + pdf_path + " -O /tmp/" + server_name + "/Backup/" + backup_file_name + " && nautilus /tmp/" + server_name + "/Backup/" + backup_file_name + " &")
