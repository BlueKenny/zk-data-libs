#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs")
import libs.send
import libs.BlueFunc
import requests
import datetime
import os
import pick
import json

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year, 0)
    if not str(invoice) == "-1" and not invoice == []: years.append(year)
year = str(pick.pick(years, "Jahr ?", indicator='-> ')[0])

months = []
for month in range(1, 13):
    print(month)
    if len(str(month)) == 1: month = "0" + str(month)
    else: month = str(month)
    
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year + "-" + month, 0)
    if not str(invoice) == "-1" and not invoice == []: months.append(month)
    
months.append("Das Ganze Jahr")
    
month = str(pick.pick(months, "Monat ?", indicator='-> ')[0])

if "Jahr" in month:
    date = year
else:
    date = year + "-" + month
print("date: " + str(date))

data = {}

datei_inhalt = ["Lieferant;Einkaufspreis;Verkaufspreis (ohne MwSt);Gewinn;Prozente vom Verkaufspreis (ohne MwSt)"]
index = 0
invoices_path = libs.send.get_invoices_file(ip, secure_key, date)
invoices = requests.get("http://" + ip.split(":")[0] + invoices_path).json()

article_cache = {}
delivery_cache = {}

for invoice in invoices:
    if True:
        deliverys = invoice["lieferscheine"].split("|")        
        for delivery_id in deliverys:
            if not delivery_id in delivery_cache:
                delivery = libs.send.get_delivery(ip, secure_key, delivery_id)
                deliverys_path = libs.send.get_deliverys_file(ip, secure_key, delivery["datum"].split("-")[0])
                deliverys = requests.get("http://" + ip.split(":")[0] + deliverys_path).json()
                for d in deliverys: delivery_cache[str(d["identification"])] = d
            else:
                delivery = delivery_cache[delivery_id]
                
            index = 0
            for bcode in delivery["bcode"].split("|"):
                if not bcode == "":
                    #print("bcode: " + str(bcode))
                    if str(bcode) in article_cache:                    
                        article = article_cache[str(bcode)]
                    else:
                        article = libs.send.get_article(ip, secure_key, bcode)
                        article_cache[str(bcode)] = article
                        
                    if not article == {}:
                        quantity = float(delivery["anzahl"].split("|")[index])
                        preisek = float(article["preisek"]) * quantity
                        preisvkh = float(delivery["preis_htva"].split("|")[index]) * quantity
                        
                        if article["lieferant"] in data:
                            # ADD
                            data[article["lieferant"]]["preisek"] += preisek
                            data[article["lieferant"]]["preisvkh"] += preisvkh
                        else:
                            # New
                            data[article["lieferant"]] = {"preisek": preisek, "preisvkh": preisvkh}
                        
                        data[article["lieferant"]]["preisek"] = round(data[article["lieferant"]]["preisek"], 2)
                        data[article["lieferant"]]["preisvkh"] = round(data[article["lieferant"]]["preisvkh"], 2)
                        
                index += 1

for lieferant in data:
    print("lieferant: " + str(lieferant))
    line = []
    line.append(str(lieferant))
    line.append(str(data[lieferant]["preisek"]).replace(".", ","))
    line.append(str(data[lieferant]["preisvkh"]).replace(".", ","))
    
    gewinn = data[lieferant]["preisvkh"] - data[lieferant]["preisek"]
    gewinn = round(gewinn, 2)
    line.append(str(gewinn).replace(".", ","))
    
    try: prozente = gewinn / data[lieferant]["preisvkh"]
    except: prozente = 0.0
    prozente = round(prozente * 100, 2)
    line.append(str(prozente).replace(".", ",") + " %")
    
    datei_inhalt.append(";".join(line))
        
open("/tmp/umsatz_pro_lieferant_" + date + ".csv", "w").write("\n".join(datei_inhalt))
os.system("libreoffice /tmp/umsatz_pro_lieferant_" + date + ".csv")
