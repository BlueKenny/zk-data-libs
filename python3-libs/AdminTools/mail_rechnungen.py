#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.sendmail

import time

try: import pick
except:
    os.system("pip3 install --user pick")
    import pick

title = "ZK-DATA Server \n\n"
ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

auswahl = ["Eingang", "Ausgang"]
eingang_oder_ausgang = pick.pick(auswahl, title + "Eingang oder Ausgangsrechnungen senden: ", indicator='-> ')[0]
print(eingang_oder_ausgang)

jahre = []
for x in range(2020, 2030):
    if eingang_oder_ausgang == "Eingang": rechnung_id = libs.send.search_rechnung_e(ip, secure_key, "", "", "", False, x, 0)  
    if eingang_oder_ausgang == "Ausgang": rechnung_id = libs.send.search_rechnung(ip, secure_key, "", "", False, x, 0)    
    if not rechnung_id == -1: 
        jahre.append(x)
jahr = pick.pick(jahre, title + "Jahr auswählen: ", indicator='-> ')[0]
jahr = str(jahr)

monate = []
for x in range(1, 13):
    x = str(x)
    if len(x) == 1: x = "0" + x       
    if eingang_oder_ausgang == "Eingang": rechnung_id = libs.send.search_rechnung_e(ip, secure_key, "", "", "", False, jahr + "-" + x, 0) 
    if eingang_oder_ausgang == "Ausgang": rechnung_id = libs.send.search_rechnung(ip, secure_key, "", "", False, jahr + "-" + x, 0) 
    if not rechnung_id == -1: monate.append(x)
monat = pick.pick(monate, title + "Monat auswählen: ", indicator='-> ')[0]

datum = jahr + "-" + monat

mail_to = input("\nemail to: \n")
mail_from = input("\nemail from: \n")

if not os.path.exists("/tmp/" + datum): os.mkdir("/tmp/" + datum)

index = 0
while True:
    if eingang_oder_ausgang == "Eingang": rechnung_id = str(libs.send.search_rechnung_e(ip, secure_key, "", "", "", False, datum, index))
    if eingang_oder_ausgang == "Ausgang": rechnung_id = libs.send.search_rechnung(ip, secure_key, "R", "", False, datum, index)
    
    if str(rechnung_id) == "-1": break
    else:        
        if eingang_oder_ausgang == "Eingang": rechnung = libs.send.get_rechnung_e(ip, secure_key, rechnung_id)
        if eingang_oder_ausgang == "Ausgang": rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)
        
        if datum in rechnung["datum"]:
            #### Download
            if eingang_oder_ausgang == "Eingang":
                if not os.path.exists("/tmp/" + datum + "/PDF_E/" + rechnung_id + ".pdf"):
                    url = libs.send.get_invoice_e_pdf(ip, secure_key, rechnung_id)
                    print("url: " + str(url))
                    
                    if not os.path.exists("/tmp/" + datum + "/PDF_E/"):
                        os.mkdir("/tmp/" + datum + "/PDF_E/")
                        
                    os.system("wget -N http://" + ip.split(":")[0] + url + " -P /tmp/" + datum + "/PDF_E/")
                    libs.sendmail.sendmail("Burkardt Rechnung " + rechnung_id, " ", mail_from, mail_to,  "/tmp/" + datum + "/PDF_E/RE" + rechnung_id + ".pdf")
                    time.sleep(5)
            #### Download
            if eingang_oder_ausgang == "Ausgang":
                if not os.path.exists("/tmp/" + datum + "/PDF_S/" + rechnung_id + ".pdf"):
                    url = libs.send.get_invoice_pdf(ip, secure_key, rechnung_id)
                    print("url: " + str(url))
                    
                    if not os.path.exists("/tmp/" + datum + "/PDF_S/"):
                        os.mkdir("/tmp/" + datum + "/PDF_S/")
                        
                    os.system("wget -N http://" + ip.split(":")[0] + url + " -P /tmp/" + datum + "/PDF_S/")
                    libs.sendmail.sendmail("Burkardt Rechnung " + rechnung_id, " ", mail_from, mail_to,  "/tmp/" + datum + "/PDF_S/" + rechnung_id + ".pdf")
                    time.sleep(5)
        
        index = index + 1

if eingang_oder_ausgang == "Ausgang":
    index = 0
    while True:
        rechnung_id = libs.send.search_rechnung(ip, secure_key, "K", "", False, datum, index)
        
        if str(rechnung_id) == "-1": break
        else:
            rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)        
            if datum in rechnung["datum"]:
                #### Download
                if not os.path.exists("/tmp/" + datum + "/PDF_DIR/" + rechnung_id + ".pdf"):
                    url = libs.send.get_invoice_pdf(ip, secure_key, rechnung_id)
                        
                    if not os.path.exists("/tmp/" + datum + "/PDF_DIR/"):
                        os.mkdir("/tmp/" + datum + "/PDF_DIR/")
                            
                    os.system("wget -N http://" + ip.split(":")[0] + url + " -P /tmp/" + datum + "/PDF_DIR/")
                    libs.sendmail.sendmail("Burkardt Kassenverkauf " + rechnung_id, " ", mail_from, mail_to,  "/tmp/" + datum + "/PDF_DIR/" + rechnung_id + ".pdf")
                    time.sleep(5)
             
            index = index + 1
