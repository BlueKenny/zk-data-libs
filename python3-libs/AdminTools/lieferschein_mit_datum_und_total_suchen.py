#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

import datetime
import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

#datum = input("\nDatum der Suche ? beispiel: 2023-06-14\n")

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    years.append(year)
selected_year = pick.pick(years, "Jahr ?", indicator='-> ')[0]

months = []
for month in range(1, 13):
    month = str(month)
    if len(month) == 1: month = "0" + month
    print(month)
    months.append(month)
selected_month = pick.pick(months, "Monat ?", indicator='-> ')[0]

days = []
for day in range(1, 32):
    day = str(day)
    if len(day) == 1: day = "0" + day
    print(day)
    days.append(day)
selected_day = pick.pick(days, "Tag ?", indicator='-> ')[0]

datum = str(selected_year) + "-" + str(selected_month) + "-" + str(selected_day)
print("datum: " + datum)

total = input("\nSumme des Lieferscheins ? beispiel: 15 oder 15.0\n")
total = float(total.replace(",", "."))

liste = []
for delivery in libs.send.search_delivery_by_changed_date_and_total(ip, secure_key, datum, total):
    liste.append(str(delivery))
open("/tmp/lieferschein_mit_datum_und_total_suchen", "w").write("\n".join(liste))

os.system("libreoffice /tmp/lieferschein_mit_datum_und_total_suchen")

