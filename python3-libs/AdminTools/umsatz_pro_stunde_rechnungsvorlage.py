#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs")
import libs.send
import datetime
import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

erste_linie = ["datum"]
for x in range(0, 25):
    x = str(x)
    if len(x) == 1: x = "0" + x
    erste_linie.append("Stunde " + x)
    
data = [";".join(erste_linie)]

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year, 0)
    if not str(invoice) == "-1" and not invoice == []: years.append(year)
selected_year = str(pick.pick(years, "Jahr ?", indicator='-> ')[0])

months = []
for month in range(1, 13):
    month = str(month)
    if len(month) == 1: month = "0" + month
    print(month)
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, selected_year + "-" + month, 0)
    if not str(invoice) == "-1" and not invoice == []: months.append(month)
selected_month = str(pick.pick(months, "Monat ?", indicator='-> ')[0])


date = selected_year + "-" + selected_month
print("date: " + str(date))

liste = libs.send.get_stats_per_hour(ip, secure_key, date)
for date in liste:
    print("date: " + str(date))

    stunden = {}
    linie = [str(date)]
    for stunde in range(0, 25):
        stunde = str(stunde)
        if len(stunde) == 1: stunde = "0" + stunde        
        print("stunde: " + str(stunde))

        if stunde in liste[date]:
            wert = str(round(liste[date][stunde], 2))
            wert = wert.replace(".", ",")
            linie.append(wert)
        else:
            linie.append("")
        
        
    data.append(";".join(linie))
        
open("/tmp/umsatz_pro_stunde_rechnungsvorlage.csv", "w").write("\n".join(data))
os.system("libreoffice /tmp/umsatz_pro_stunde_rechnungsvorlage.csv")
