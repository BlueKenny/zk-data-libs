#!/usr/bin/env python3
import sys
import os
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

heute = libs.BlueFunc.Date()
datum = input("Von welchem Tag soll eine inventur angezeigt werden ?\nEnter drucken für HEUTE -> " + heute + "\n")
if datum == "": datum = heute


print("Send print_article_stock(" + str(datum) + ")")
answer = libs.send.print_article_stock(ip, secure_key, datum)
print(answer)

cmd1 = "wget http://" + ip.split(":")[0] + answer + " -O /tmp/print_article_stock.csv"
print(cmd1)
os.system(cmd1)

cmd2 = "libreoffice /tmp/print_article_stock.csv"
print(cmd2)
os.system(cmd2)

