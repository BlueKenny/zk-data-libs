#!/usr/bin/env python3
import sys
import os
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

heute = libs.BlueFunc.Date()
datum = input("Von welchem Tag soll eine inventur angezeigt werden ?\nEnter drucken für HEUTE -> " + heute + "\n")
if datum == "": datum = heute


print("Send print_article_stock(" + str(datum) + ")")
answer = libs.send.print_article_stock(ip, secure_key, datum)
print(answer)

cmd1 = "wget http://" + ip.split(":")[0] + answer + " -O /tmp/print_article_stock.csv"
print(cmd1)
os.system(cmd1)

data = open("/tmp/print_article_stock.csv", "r").readlines()
new_file_data = ["categorie;beschreibung;wert;" + datum]
stock_pro_categorie = {}

cat_names = {}
for cn in libs.send.get_article_categories(ip, secure_key):
    print(cn)
    cat_names[int(cn["identification"])] = cn["name"]
    
del data[0]

for line in data:
    cat = int(line.split(";")[2])
    wert = float(line.split(";")[5].replace(",", ".").rstrip())
    if cat in stock_pro_categorie:
        stock_pro_categorie[cat] += wert
    else:
        stock_pro_categorie[cat] = wert
        

for cat in sorted(stock_pro_categorie):
    new_line = str(cat) + ";" + cat_names[cat] + ";" + str(round(stock_pro_categorie[cat], 2)).replace(".", ",")
    print("cat " + str(cat) + ": " + str(round(stock_pro_categorie[cat], 2)))
    new_file_data.append(new_line)
        
open("/tmp/print_article_stock_pro_categorie.csv", "w").write("\n".join(new_file_data))
os.system("libreoffice /tmp/print_article_stock_pro_categorie.csv")
