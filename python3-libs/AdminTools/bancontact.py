#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc
import datetime
import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

index = 0

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    transaktion = libs.send.search_transaktion_by_date(ip, secure_key, str(year), 0)
    if not str(transaktion) == "-1" and not transaktion == []: years.append(year)
selected_year = pick.pick(years, "Jahr ?", indicator='-> ')[0]

months = []
for month in range(1, 13):
    month = str(month)
    if len(month) == 1: month = "0" + month
    print(month)
    transaktion = libs.send.search_transaktion_by_date(ip, secure_key, str(selected_year) + "-" + month , 0)
    if not str(transaktion) == "-1" and not transaktion == []: months.append(month)
selected_month = pick.pick(months, "Monat ?", indicator='-> ')[0]

days = []
for day in range(1, 32):
    day = str(day)
    if len(day) == 1: day = "0" + day
    print(day)
    transaktion = libs.send.search_transaktion_by_date(ip, secure_key, str(selected_year) + "-" + selected_month + "-" + day , 0)
    if not str(transaktion) == "-1" and not transaktion == []: days.append(day)
days.append("Ganzer Monat")
selected_day = pick.pick(days, "Tag ?", indicator='-> ')[0]

transaktions = []

if selected_day == "Ganzer Monat":
    datum = str(selected_year) + "-" + str(selected_month)
else:
    datum = str(selected_year) + "-" + str(selected_month) + "-" + str(selected_day)
print("datum: " + datum)

while True:
    transaktion = libs.send.search_transaktion_by_date(ip, secure_key, datum, index)
    #print(transaktion)
    
    if str(transaktion) == "-1" or transaktion == []:
        break
    else:
        konto = libs.send.get_konto(ip, secure_key, transaktion["konto_in"]-1)
        transaktion["konto_name"] = str(konto["name"])
               
        #if transaktion["konto_in"] == 2: 
        if "bancontact" == str(konto["name"]).lower(): # is bancontact
            transaktions.append(transaktion)       
        
        index = index + 1
        
        
data = []
split = ";"

first_line_data = []
for key in libs.send.get_transaktion(ip, secure_key, 1):
    first_line_data.append(key)
data.append(split.join(first_line_data))

for transaktion in transaktions:
    print("transaktion: " + str(transaktion))
     
    line_data = []
    for key in transaktion:
        if key == "betrag":
            transaktion["betrag"] = str(transaktion["betrag"]).replace(".", ",")
    
        line_data.append(str(transaktion[key]))
    data.append(split.join(line_data))
     
    

open("/tmp/zk_data_bancontact.csv", "w").write("\n".join(data))
os.system("libreoffice /tmp/zk_data_bancontact.csv")
