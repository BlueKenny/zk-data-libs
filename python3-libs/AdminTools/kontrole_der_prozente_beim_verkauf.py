#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs")
import libs.send
import libs.BlueFunc
import requests
import datetime
import os
import pick
import json

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year, 0)
    if not str(invoice) == "-1" and not invoice == [] and not str(invoice) == "": years.append(year)
year = str(pick.pick(years, "Jahr ?", indicator='-> ')[0])

months = []
for month in range(1, 13):
    print(month)
    if len(str(month)) == 1: month = "0" + str(month)
    else: month = str(month)
    
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year + "-" + month, 0)
    if not str(invoice) == "-1" and not invoice == []: months.append(month)
month = str(pick.pick(months, "Monat ?", indicator='-> ')[0])

date = year + "-" + month
print("date: " + str(date))


datei_inhalt = ["Datum;Rechnung;Kunde;Identification;Name;Anzahl;Einkaufspreis;Verkaufspreis HTVA;Original Verkaufspreis HTVA;Rabatt für den Kunden (%);Gewinn (€);Gewinn (%)"]
index = 0
invoices_path = libs.send.get_invoices_file(ip, secure_key, date)
invoices = requests.get("http://" + ip.split(":")[0] + invoices_path).json()

for invoice in invoices:
    print("invoice: " + str(invoice))

    deliverys = invoice["lieferscheine"].split("|")        
    for delivery_id in deliverys:
        if not delivery_id.rstrip() == "":
            delivery = libs.send.get_delivery(ip, secure_key, delivery_id)
            index = 0
            for bcode in delivery["bcode"].split("|"):
                if not bcode == "":
                    #print("bcode: " + str(bcode))
                    article = libs.send.get_article(ip, secure_key, bcode)
                    if not article == {}:
                        if True:
                            einkaufspreis = round(float(article["preisek"]), 2)
                            verkaufspreis_htva = round(float(delivery["preis_htva"].split("|")[index]), 2)
                            originalpreis_htva = round(float(article["preisvkh"]), 2)
                            print(str(article["identification"]) + " -> " + str(verkaufspreis_htva) + "/" + str(originalpreis_htva))
                            new_line = []
                            new_line.append(delivery["datum"])
                            new_line.append(str(invoice["identification"]))
                            new_line.append(str(invoice["kunde_name"]))
                            new_line.append(str(article["identification"]))
                            new_line.append(article["name_de"])
                            new_line.append(str(delivery["anzahl"].split("|")[index]).replace(".", ","))
                            new_line.append(str(einkaufspreis).replace(".", ","))
                            new_line.append(str(verkaufspreis_htva).replace(".", ","))
                            new_line.append(str(originalpreis_htva).replace(".", ","))
                            
                            if not originalpreis_htva == 0.0:
                                prozente = -(100*(verkaufspreis_htva/originalpreis_htva)-100)
                                prozente = round(prozente, 2)
                            else:
                                prozente = "OL"                                
                            new_line.append(str(prozente).replace(".", ","))
                               
                            gewinn = round(verkaufspreis_htva - einkaufspreis, 2)
                            new_line.append(str(gewinn).replace(".", ","))
                            
                            if not originalpreis_htva == 0.0:
                                prozente = (gewinn/originalpreis_htva)*100
                                prozente = round(prozente, 2)
                            else:
                                prozente = "OL"                                
                            new_line.append(str(prozente).replace(".", ","))
                            
                            datei_inhalt.append(";".join(new_line))
                index += 1
   
filename = "/tmp/kontrole_der_prozente_beim_verkauf_" + str(date) + ".csv"
open(filename, "w").write("\n".join(datei_inhalt))
os.system("libreoffice " + filename)
