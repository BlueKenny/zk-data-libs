#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")

import libs.send
import os

import requests

try: import pick
except:
    os.system("pip3 install --user pick")
    import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

title = "ZK-DATA Server \n\n"
auswahl = ["Einzelsuche", "Abschnitt", "Jahr"]
modus = pick.pick(auswahl, title + "Modus: Einzelsuche oder ganzer abschnitt\n\nEinzelsuche: es wird immer nur ein Lieferschein kontrolliert mit der Lieferscheinnummer\nAbschnitt: die Lieferschein ab n° X bis Y werden kontrolliert", indicator='-> ')[0]
print(modus)

if modus == "Einzelsuche":
    while True:
        delivery_id = input("\nLieferschein nummer ? \n")
        delivery_id = int(delivery_id)

        paid = False
        delivery = libs.send.get_delivery(ip, secure_key, delivery_id)

        if not delivery["rechnung"] == "" and not "RV" in delivery["rechnung"]:
            invoice = libs.send.get_invoice(ip, secure_key, delivery["rechnung"])
            print("\nRest: " + str(invoice["rest"]))
            if float(invoice["rest"]) == 0.0:
                paid = True
                print("---------------------")
                print("\n Bezahlt :) \n")
                print("---------------------")
        else:
            print("NICHT BERECHNET")

elif modus == "Abschnitt":
    start_id = input("\nBei Folgender Lieferschein nummer anfangen: \n")
    start_id = float(start_id)
    check_start_delivery = libs.send.get_delivery(ip, secure_key, start_id)
    if check_start_delivery == {}:
        print("Lieferschein " + str(start_id) + " existiert nicht oder Server nicht erreichbar!")
    else:
        end_id = input("\nLieferscheine bis zu folgender nummer kontrollieren: \n")
        end_id = float(end_id)
        check_end_delivery = libs.send.get_delivery(ip, secure_key, end_id)
        #if False:#if check_end_delivery == {}:
        #    print("Lieferschein " + str(end_id) + " existiert nicht oder Server nicht erreichbar!")
        if True: #else:
            if end_id < start_id:
                print("Anfangslieferscheinnummer muss größer sein als die End Nummer !")
            else:
                liste_nicht_bezahlt = []
                delivery_id = start_id
                while True:
                    delivery = libs.send.get_delivery(ip, secure_key, delivery_id)
                    if not delivery == {}:
                        if not delivery["rechnung"] == "" and not "RV" in delivery["rechnung"]:
                            invoice = libs.send.get_invoice(ip, secure_key, delivery["rechnung"])
                            if float(invoice["rest"]) > 0.0:
                                liste_nicht_bezahlt.append(str(int(delivery_id)) + ";" + str(invoice["rest"]))
                        else:
                            if float(delivery["total"]) > 0.0:
                                if not delivery["angebot"] == 1 and not delivery["bestellung"] == 1:
                                    liste_nicht_bezahlt.append(str(int(delivery_id)) + ";" + str(delivery["total"]))
                        
                    if delivery_id == end_id: break
                    else: delivery_id = int(delivery_id) + 1
                    
                open("/tmp/liste_nicht_bezahlt.csv", "w").write("\n".join(liste_nicht_bezahlt))
                os.system("gnome-text-editor /tmp/liste_nicht_bezahlt.csv")
else:
    years = []
    deliverys_data = {}
    this_year = int(libs.BlueFunc.date_year())
    for year in range(2020, this_year + 1):
        path = libs.send.get_deliverys_file(ip, secure_key, year)
        r = requests.get("http://" + ip.split(":")[0] + path)
        #print(r.text)
        if str(r.text) == "[]": print("LEER")
        else:
            years.append(year)
            deliverys_data[str(year)] = r.json()

    year = pick.pick(years, title + "Jahr auswählen\n", indicator='-> ')[0]
    not_paid_list = []
    for delivery in deliverys_data[str(year)]:
        print("delivery: " + str(delivery))
        if not delivery["bestellung"] and not delivery["angebot"]:
            print("True")
            if "RV" in delivery["rechnung"] or delivery["rechnung"] == "":
                print("delivery " + str(delivery["identification"]) + " is NOT in invoice")
                not_paid_list.append(str(delivery["identification"]) + ";" + str(delivery["kunde_name"]) + ";" + str(delivery["total"]))
            else:
                print("delivery " + str(delivery["identification"]) + " IS in invoice")
                print(delivery)
                invoice = libs.send.get_invoice(ip, secure_key, delivery["rechnung"])
                if not invoice["rest"] == 0.0:
                    not_paid_list.append(str(delivery["identification"]) + ";" + str(delivery["kunde_name"]) + ";" + str(delivery["total"]))
                    
    open("/tmp/zk_data_not_paid_list.csv", "w").write("\n".join(not_paid_list))
    os.system("libreoffice /tmp/zk_data_not_paid_list.csv")
    
