#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import datetime

import pick
import json

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

#NON-PROF,944249648,2021-11-18,ZipCode_1,yes,no
#PROF,329441516,2021-11-18,ZipCode_2,yes,no

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    transaktion = libs.send.search_transaktion_by_date(ip, secure_key, str(year), 0)
    if not str(transaktion) == "-1" and not transaktion == []: years.append(year)
selected_year = str(pick.pick(years, "Jahr ?", indicator='-> ')[0])

complete_sn_list = []
sn_file_path = libs.send.search_sn_by_dates(ip, secure_key, selected_year + "-01", selected_year + "-13")
filename = sn_file_path.split("/")[-1]
os.system("cd /tmp/ && wget -N http://" + ip.split(":")[0] + sn_file_path)
#os.system("libreoffice /tmp/" + filename)
#complete_sn_list.append(sn)
complete_sn_list = json.loads(open("/tmp/" + filename).readlines()[0])


file_data = ["CustomerType,SerialNumber,SalesDate,ZipCode,VTE_SF_10 VTE_SF_10_J,VTE_SF_10 VTE_SF_10_N"]
for sn in complete_sn_list:
    print(sn)
    
    data = libs.send.get_sn_data(ip, secure_key, sn)
    for line in data:
        print(line)
        file_data.append("NON-PROF," + sn + "," + line["date_invoice"] + ",4950,yes,no")
        
open("/tmp/sn_data.csv", "w").write("\n".join(file_data))
os.system("libreoffice /tmp/sn_data.csv")

