#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs")
import libs.send
import pathlib
import pick
import libs.RoundUp

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

home_dir = str(pathlib.Path.home())
csv_files = []
for file in os.listdir(home_dir + "/Downloads/"):
    if file.lower().endswith(".csv"):
        csv_files.append(file)

datei = pick.pick(csv_files, "Welche Datei aus dem Downloads ordner soll importiert werden ?", indicator='-> ')[0]

data = open(home_dir + "/Downloads/" + datei, "r").readlines()  
first_line = data[0].rstrip()
headers = []
spliter = ";"

modes = ["artikel"]
mode = pick.pick(modes, "Um welchen inhalt geht es ?", indicator='-> ')[0]

print("mode: " + mode)
if mode == "artikel":
    print("Headers lines")
    posible_headers = []
    for i in libs.send.get_article(ip, secure_key, 0): posible_headers.append(i)
    
    for data_first_line in first_line.split(spliter):
        #print(data_first_line)
        if data_first_line in posible_headers:
            headers.append(data_first_line)
        else:
            select_header = pick.pick(posible_headers, "Wofür steht \"" + data_first_line + "\" ?", indicator='-> ')
            selected_header = select_header[0]
            del posible_headers[select_header[1]]            
            headers.append(selected_header)

    if not "identifcation" in headers:
        search_word = pick.pick(headers, "Es wurde keine identification für die Artikel benutzt \n was soll benutzt werden um eine suche zu starten ?", indicator='-> ')[0]
        print("search_word: " + str(search_word))
    
    del data[0] # remove first line in data
    for line in data:
        line = line.rstrip()
        print("line: " + str(line))        
        
        dict_to_send = {}
        index = 0
        for data_of_line in line.split(spliter):
            #print("data_of_line:" + str(data_of_line))
                        
            dict_to_send[headers[index]] = data_of_line
            index += 1
                 
        if not "identifcation" in headers:
            print("Keine identification definiert !")
            dict_to_send["identification"] = ""
            
            search_string = ""
            for character in dict_to_send[search_word]:
                if character.isalpha() or character.isdigit():
                    search_string = search_string + str(character)
            search_string = search_string.upper()
                
            search_list = libs.send.search_article(ip, secure_key, {"suche": search_string})
            if len(search_list) == 0:
                new_article = libs.send.GetID(ip, secure_key)
                dict_to_send["identification"] = new_article["identification"]
                libs.send.set_article(ip, secure_key, dict_to_send)   
            else:
                for i in search_list:
                    art = libs.send.get_article(ip, secure_key, i)
                
                    if art[search_word] == search_string:
                        dict_to_send["identification"] = i
                        
                if not dict_to_send["identification"] == "":
                    print("dict_to_send: " + str(dict_to_send))
                    libs.send.set_article(ip, secure_key, dict_to_send)            
                else:
                    open(home_dir + "/Downloads/" + datei + "_TODO.csv", "a").write(line + "\n")
        else: 
            print("dict_to_send: " + str(dict_to_send))
            libs.send.set_article(ip, secure_key, dict_to_send)
            
        #input("\n\nweiter ?")
        print("\n")
if os.path.exists(home_dir + "/Downloads/" + datei + "_TODO.csv"): os.system("libreoffice " + home_dir + "/Downloads/" + datei + "_TODO.csv")
