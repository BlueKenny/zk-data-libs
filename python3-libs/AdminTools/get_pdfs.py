#!/usr/bin/env python3

import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

try: import pick
except:
    os.system("pip3 install --user pick")
    import pick

import time

title = "ZK-DATA Server \n\n"
ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

jahre = []
for x in range(2020, 2030):
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "", "", False, x, 0)    
    if not rechnung_id == -1: 
        jahre.append(x)
jahr = pick.pick(jahre, title + "Jahr auswählen: ", indicator='-> ')[0]
jahr = str(jahr)

monate = []
for x in range(1, 13):
    x = str(x)
    if len(x) == 1: x = "0" + x
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "", "", False, jahr + "-" + x, 0)    
    if not rechnung_id == -1: monate.append(x)
monat = pick.pick(monate, title + "Monat auswählen: ", indicator='-> ')[0]

datum = jahr + "-" + monat

if not os.path.exists("/tmp/" + datum): os.mkdir("/tmp/" + datum)

index = 0
while True:    
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "R", "", False, datum, index)    
    
    if rechnung_id == -1: break
    else:
        rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)
        
        if datum in rechnung["datum"]:

            #### Download
            if not os.path.exists("/tmp/" + datum + "/PDF_S/" + rechnung_id + ".pdf"):
                url = libs.send.get_invoice_pdf(ip, secure_key, rechnung_id)
                print("url: " + str(url))
                
                if not os.path.exists("/tmp/" + datum + "/PDF_S/"):
                    os.mkdir("/tmp/" + datum + "/PDF_S/")
                    
                os.system("wget -N http://" + ip.split(":")[0] + url + " -P /tmp/" + datum + "/PDF_S/")
                time.sleep(2)
        
        index = index + 1
index = 0
while True:    
    rechnung_id = libs.send.search_rechnung(ip, secure_key, "K", "", False, datum, index)    
    
    if rechnung_id == -1: break
    else:
        rechnung = libs.send.get_rechnung(ip, secure_key, rechnung_id)        
        if datum in rechnung["datum"]:
            #### Download
            if not os.path.exists("/tmp/" + datum + "/PDF_DIR/" + rechnung_id + ".pdf"):
                url = libs.send.get_invoice_pdf(ip, secure_key, rechnung_id)
                    
                if not os.path.exists("/tmp/" + datum + "/PDF_DIR/"):
                    os.mkdir("/tmp/" + datum + "/PDF_DIR/")
                        
                os.system("wget -N http://" + ip.split(":")[0] + url + " -P /tmp/" + datum + "/PDF_DIR/")
                time.sleep(2)
         
        index = index + 1


index = 0
rechnung_e_id = 0
while not rechnung_e_id == -1:    
    rechnung_e_id = libs.send.search_rechnung_e(ip, secure_key, "", "", "", False, datum, index)
    rechnung_e_id = int(rechnung_e_id)

    if not rechnung_e_id == -1:
        rechnung_e = libs.send.get_rechnung_e(ip, secure_key, rechnung_e_id)            
        
        file = "/tmp/" + datum + "/PDF_E/" + str(rechnung_e_id) + ".pdf"
        file_large = "/tmp/" + datum + "/PDF_E/" + str(rechnung_e_id) + "LARGE.pdf"
        print("file: " + str(file))
        
        while not os.path.exists(file):
            print("Download " + str(file))
            # new
            url_to_pdf = libs.send.get_invoice_e_pdf(ip, secure_key, rechnung_e_id)
            
            print("url_to_pdf: " + str(url_to_pdf))            
               
            if not os.path.exists("/tmp/" + datum + "/PDF_E"): os.mkdir("/tmp/" + datum + "/PDF_E")
            
            url_to_pdf = "http://" + ip.split(":")[0] + "/" + url_to_pdf
            os.system("wget -O " + file_large + " " + url_to_pdf + " && ps2pdf " + file_large + " " + file + " && rm " + file_large)
            time.sleep(2)
          
            
        
        index = index + 1
