#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

print("")
alter_ort = input("Alter ORT ? ")
alter_ort = alter_ort.upper()
print("Alter Ort: " + str(alter_ort))


print("")
neuer_ort = input("Neuer ORT ? ")
neuer_ort = neuer_ort.upper()

print("Neuer Ort: " + str(neuer_ort))

print("")

while True:
    artikel_liste = libs.send.SearchArt(ip, secure_key, {"ort": alter_ort})
    
    for artikel in artikel_liste:
        print("artikel: " + str(artikel))
        
        art = libs.send.get_article(ip, secure_key, artikel)
        art["ort"] = neuer_ort
        
        libs.send.set_article(ip, secure_key, art)
    
    if artikel_liste == []: break
    
input("Fertig, ENTER Drucken zum Beenden")
