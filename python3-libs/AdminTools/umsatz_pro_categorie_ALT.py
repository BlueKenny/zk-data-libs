#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")

import os

import libs.send
import libs.BlueFunc

import datetime
import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year, 0)
    if not str(invoice) == "-1" and not invoice == []: years.append(year)
year = str(pick.pick(years, "Jahr ?", indicator='-> ')[0])

categories = libs.send.get_article_categories(ip, secure_key)
csv_data = []

first_line = [""]
profit_data = {}
for month in range(1, 13):    
    month = str(month)
    if len(month) == 1: month = "0" + month
    date = year + "-" + month
    
    first_line.append(date)    
    profit_data[date] = libs.send.get_stats_categorieprofit(ip, secure_key, date)
    print(profit_data[date])
    
csv_data.append(";".join(first_line))

for categorie in categories:
    cat_id = str(categorie["identification"])
    line_list = [cat_id + " " + categorie["name"]]

    for month in range(1, 13):    
        month = str(month)
        if len(month) == 1: month = "0" + month
        date = year + "-" + month
        
        try: wert = profit_data[date][cat_id]
        except: wert = 0.0
        
        
        line_list.append(str(str(wert).replace(".", ",")))
    
    csv_data.append(";".join(line_list))
    
        
        
open("/tmp/umsatz_pro_categorie.csv", "w").write("\n".join(csv_data))
os.system("libreoffice /tmp/umsatz_pro_categorie.csv")
