#!/usr/bin/env python3


import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

liste = []
datum = libs.BlueFunc.Date()
uhrzeit = libs.BlueFunc.uhrzeit()

user_liste = libs.send.getUserList(ip, secure_key)
user = pick.pick(user_liste, "Benutzer ?", indicator='-> ')[0]

finish = pick.pick(["0: Nicht Fertige", "1: Fertige", "2: Alle"], "Welche Lieferscheinen sollen angezeigt werden ?", indicator='-> ')[0]
if finish == "0: Nicht Fertige":
    fertig = 0
elif finish == "1: Fertige":
    fertig = 1
else:
    fertig = 2

delivery_liste = libs.send.search_delivery(ip, secure_key, {"fertig": fertig, "eigene": False, "search_user": user, "limit": 200})

liste.append("L n°;;" + str(user) + " " + str(datum) + " " + str(uhrzeit) + "\n")
liste.append("L n°;datum;kunde;maschine;wert;user")

for delivery_id in delivery_liste:
    print("delivery_id: " + str(delivery_id))
        
    delivery = libs.send.get_delivery(ip, secure_key, delivery_id)

    if delivery == {}: break
    else:
        delivery["kunde_name"] = delivery["kunde_name"][:35]
        
        liste.append(str(delivery["identification"]) + ";" + delivery["datum"] + ";" + delivery["kunde_name"] + ";" + delivery["maschine"] + ";" + str(delivery["total"]) + ";" + delivery["user"])
    

        
open("/tmp/zk-data_alte_offene_lieferscheine.csv", "w").write("\n".join(liste))
os.system("libreoffice /tmp/zk-data_alte_offene_lieferscheine.csv")


