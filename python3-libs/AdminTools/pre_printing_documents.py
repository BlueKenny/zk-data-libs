#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc
import os
try: import pick
except: os.system("pip3 install pick --break-system-packages")
import requests

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

pdf_path = libs.send.get_pdfs_file(ip, secure_key)
r = requests.get("http://" + ip.split(":")[0] + pdf_path)
pdfs_done = r.json()

deliverys = []
invoices = []
kassenbuch = []

year = libs.BlueFunc.Date().split("-")[0]
path = libs.send.get_deliverys_file(ip, secure_key, year)
url = "http://" + ip.split(":")[0] + path
print("url: " + url)
r = requests.get(url)
delivery_data = r.json()
for delivery in delivery_data:
    #print(delivery)
    if not "L" + str(delivery["identification"]) in pdfs_done:
        deliverys.append(str(delivery["identification"]))

    if not str(delivery["rechnung"]) == "":
        if not str(delivery["rechnung"]) in pdfs_done:
            invoices.append(str(delivery["rechnung"]))

new_date = libs.send.get_kassenbuch_date_last(ip, secure_key, 1)
while year in str(new_date):
    if not "KB1-" + str(new_date) in pdfs_done:
        kassenbuch.append(new_date)
    new_date = str(libs.send.get_kassenbuch_date_before(ip, secure_key, 1, new_date))

total = len(deliverys) + len(invoices) + len(kassenbuch)
print("total: " + str(total))
index = 1

for x in kassenbuch:
    libs.send.get_kassenbuch_pdf(ip, secure_key, 1, x)
    print("progress: " + str(index) + "/" + str(total))
    index += 1
    
for x in deliverys:
    libs.send.get_delivery_pdf(ip, secure_key, x)
    print("progress: " + str(index) + "/" + str(total))
    index += 1
    
for x in invoices:
    libs.send.get_invoice_pdf(ip, secure_key, x)
    print("progress: " + str(index) + "/" + str(total))
    index += 1

