#! /usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs/")

import libs.send

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

libs.send.close(ip, secure_key)
