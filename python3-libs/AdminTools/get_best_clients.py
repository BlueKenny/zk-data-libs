#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")

import libs.send
import os

data = []


ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

liste = libs.send.get_best_clients(ip, secure_key, 200, 365)
for identification in liste:
    print(liste[identification])
    name = liste[identification]["name"]
    firma = liste[identification]["firma"]
    quantity = liste[identification]["quantity"]
    total = liste[identification]["total"]
    
    data.append(str(identification) + ";" + str(name) + ";" + str(firma) + ";" + str(quantity) + ";" + str(total).replace(".", ","))
    
open("/tmp/get_best_clients.csv", "w").write("\n".join(data))
os.system("libreoffice /tmp/get_best_clients.csv")
