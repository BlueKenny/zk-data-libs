#!/usr/bin/env python3
import os
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send
import libs.BlueFunc

import datetime

import pick

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

minimum_vorschlag = pick.pick(["Ja", "Nein"], "Minimum Vorschlagen: ?", indicator='-> ')[0]
if minimum_vorschlag == "Ja": minimum_vorschlag = True
else: minimum_vorschlag = False


marchants = libs.send.get_marchants(ip, secure_key)
marchants = sorted(marchants)

selected_marchant = pick.pick(marchants, "Lieferant: ?", indicator='-> ')[0]

article_list = libs.send.search_article(ip, secure_key, {"lieferant": selected_marchant, "limit": 500})
    
data = []
split = ";"
this_year = libs.BlueFunc.date_year()
this_month = libs.BlueFunc.date_month()

first_line = [ "identification",
               "groesse",
               "artikel",
               "name_de",
               "anzahl",
               "minimum",
               "tva",
               "preisek",
               "preisvkh",
               "preisvk"]
if minimum_vorschlag: first_line.append("minimum_vorschlag")
line = split.join(first_line)
data.append(line)

for identification in article_list:
    print("identification: " + str(identification))
    article = libs.send.get_article(ip, secure_key, identification)
    new_list = []
    for i in first_line:
        if i == "minimum_vorschlag":
            sell_counter = 0
            total_counter = 0
            for year in range(int(this_year)-1, int(this_year)+1):
                print("year: " + str(year))
                for month in range(1, 13):
                    if len(str(month)) == 1: date = str(year) + "-0" + str(month)
                    else: date = str(year) + "-" + str(month)
                    
                    sell_number = libs.send.get_article_sell(ip, secure_key, identification, date)
                    if total_counter == 0:
                        if sell_number > 0:
                            sell_counter = sell_number
                            total_counter = 1
                    else:
                        sell_counter += sell_number
                        total_counter += 1
                    
                    if this_year + "-" + this_month == date:
                        break
                        
            print("sell_counter: " + str(sell_counter))
            print("total_counter: " + str(total_counter))
            if total_counter == 0: average_sell = 0.0
            else: average_sell = round(sell_counter / total_counter, 2)
            print("average_sell: " + str(average_sell))
            new_list.append(str(average_sell).replace(".", ","))
        else:
            new_list.append(str(article[i]).replace(".", ","))
        
    line = split.join(new_list)
    data.append(line)

open("/tmp/zk_neue_bestellung.csv", "w").write("\n".join(data))
os.system("libreoffice /tmp/zk_neue_bestellung.csv")
