#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs")
import libs.send
import libs.BlueFunc
import requests
import datetime
import os
import pick
import json

ip = sys.argv[1]
secure_key = sys.argv[2]
if not "." in ip: ip = libs.send.get_ip_of_server(ip)

years = []
for year in range(2020, datetime.date.today().year+1):
    print(year)
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year, 0)
    if not str(invoice) == "-1" and not invoice == []: years.append(year)
year = str(pick.pick(years, "Jahr ?", indicator='-> ')[0])

months = []
for month in range(1, 13):
    print(month)
    if len(str(month)) == 1: month = "0" + str(month)
    else: month = str(month)
    
    invoice = libs.send.search_invoice(ip, secure_key, "", "", False, year + "-" + month, 0)
    if not str(invoice) == "-1" and not invoice == []: months.append(month)
    
months.append("Das Ganze Jahr")
    
month = str(pick.pick(months, "Monat ?", indicator='-> ')[0])

if "Jahr" in month:
    date = year
else:
    date = year + "-" + month
    
print("date: " + str(date))

data = []
new_line = []
test_invoice_e = libs.send.get_invoice_e(ip, secure_key, 1)
for key in test_invoice_e: new_line.append(key)
data.append(";".join(new_line))


index = 0
while True:

    invoice_e_id = libs.send.search_invoice_e(ip, secure_key, "", "", "", False, date, index)
    if invoice_e_id == -1: break
    
    invoice_e = libs.send.get_invoice_e(ip, secure_key, invoice_e_id)
    new_line = []
    
    for key in invoice_e: new_line.append(str(invoice_e[key]))
    
    data.append(";".join(new_line))
    
    
    index += 1

open("/tmp/rechnung_e_" + date + ".csv", "w").write( "\n".join(data) )
os.system("libreoffice /tmp/rechnung_e_" + date + ".csv")

quit()
selected_marchant = pick.pick(marchants, "Lieferant: ?", indicator='-> ')[0]

datei_inhalt = ["Datum;Rechnung;Identification;Name;Anzahl;Einzelpreis EK;Einzelpreis HTVA;Gesamt HTVA"]
index = 0
invoices_path = libs.send.get_invoices_file(ip, secure_key, date)
invoices = requests.get("http://" + ip.split(":")[0] + invoices_path).json()

article_cache = {}
delivery_cache = {}

for invoice in invoices:
    if True:
        deliverys = invoice["lieferscheine"].split("|")        
        for delivery_id in deliverys:
            if not delivery_id in delivery_cache:
                delivery = libs.send.get_delivery(ip, secure_key, delivery_id)
                deliverys_path = libs.send.get_deliverys_file(ip, secure_key, delivery["datum"].split("-")[0])
                deliverys = requests.get("http://" + ip.split(":")[0] + deliverys_path).json()
                for d in deliverys: delivery_cache[str(d["identification"])] = d
            else:
                delivery = delivery_cache[delivery_id]
                
            index = 0
            for bcode in delivery["bcode"].split("|"):
                if not bcode == "":
                    if str(bcode) in article_cache:                    
                        article = article_cache[str(bcode)]
                    else:
                        article = libs.send.get_article(ip, secure_key, bcode)
                        article_cache[str(bcode)] = article
                        
                    if not article == {}:
                        if str(selected_marchant) == str(article["lieferant"]):
                            new_line = []
                            new_line.append(invoice["datum"])
                            new_line.append(str(invoice["identification"]))
                            new_line.append(str(article["identification"]))
                            new_line.append(article["name_de"])
                            new_line.append(str(delivery["anzahl"].split("|")[index]).replace(".", ","))
                            new_line.append(str(article["preisek"]).replace(".", ","))
                            new_line.append(str(delivery["preis_htva"].split("|")[index]).replace(".", ","))
                            
                            total_htva = float(delivery["anzahl"].split("|")[index]) * float(delivery["preis_htva"].split("|")[index])
                            new_line.append(str(total_htva).replace(".", ","))
                            
                            datei_inhalt.append(";".join(new_line))
                index += 1
                            
open("/tmp/noname.csv", "w").write("\n".join(datei_inhalt))
os.system("libreoffice /tmp/noname.csv")
