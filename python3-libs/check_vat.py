#!/usr/bin/env python3

import requests
import time
#import json

# https://ec.europa.eu/taxation_customs/vies/#/technical-information
## ???

# curl --url 'https://euvatapi.com/api/v1/validate?access_key=YOUR_ACCESS_KEY&vat_number=LU26375245'
api_key = "9de5eab034023d03f8efc998992ea8a9"
url = "https://euvatapi.com/api/v1/validate?access_key=" + api_key + "&vat_number="

valid_time_max = 7776000 # valid for 90 days
valid_time_min = 600 # valid for 10min

def check_tva_online(tva): # return list with [bool(sucess), string(Information)]
    print("check_tva_online(" + str(tva) + ")")
    tva = str(tva).upper().rstrip().replace(".", "").replace(" ", "").replace(",", "").replace(" ", "")
    timestamp = str(valid_time_min + time.time())
    answer = {"identification": tva, "status": 0, "status_text": "Not checked", "timestamp": timestamp}
    
    if len(tva) > 6:
        tva_prefix = tva[0] + tva[1]
        tva_number = tva[2:]
        print("tva: (" + tva_prefix + ", " + tva_number + ")")
        
        if tva_prefix == "BE":
            if len(tva) == 12 or len(tva) == 11:
                try:
                    r = requests.get("https://kbopub.economie.fgov.be/kbopub/zoeknummerform.html?lang=en&nummer=" + tva_number + "&actionLu=Suche", timeout=10)
                    data = r.text
                except:
                    data = ""
                    
                if '<span class="pageactief">Active</span>' in data:
                    print("vat: actif")
                    answer["status"] = 1
                    answer["status_text"] = "BE: MwSt n° ist gültig"
                    answer["timestamp"] = str(valid_time_max + time.time()) 
                elif '<span class="pagenietactief">Stopped</span>' in data:
                    print("vat: not actif")
                    answer["status_text"] = "BE: MwSt n° ist nicht mehr Aktif"
                    answer["timestamp"] = str(valid_time_max + time.time())
                elif 'The number is invalid.' in data:
                    print("vat: invalid")
                    answer["status_text"] = "BE: MwSt n° existiert nicht"
                    answer["timestamp"] = str(valid_time_max + time.time())
                else:
                    print("vat: error")
                    print(data)
                    answer["status_text"] = "Fehler ?"
                    answer["timestamp"] = str(valid_time_min + time.time())
            else:
                answer["status_text"] = "BE: MwSt n° hat nicht die richtige laenge"
        else:
            print("EU Vat")
            try:
                r = requests.get(url + tva, timeout=10)
                data = r.json()
                #print(data)
            except:
                data = ""
                
            if "valid" in data:
                if bool(data["valid"]):
                    print("valid:" + str(bool(data["valid"])))
                    land = data["country_code"]
                    answer["status"] = 1
                    answer["status_text"] = land + ": MwSt n° ist gültig"
                    answer["timestamp"] = str(valid_time_max + time.time())          
                else:
                    print("format_valid:" + str(bool(data["format_valid"])))
                    if bool(data["format_valid"]):
                        answer["status_text"] = "MwSt n° existiert nicht"
                        answer["timestamp"] = str(valid_time_max + time.time())
                    else:
                        answer["status_text"] = "MwSt hat nicht das richtige format"
                        answer["timestamp"] = str(valid_time_max + time.time())
            else:
                print("Error ?")
                answer["status_text"] = "MwSt konnte nicht uberprüft werden"
                answer["timestamp"] = str(valid_time_min + time.time())
    else:
        answer["status_text"] = "MwSt n° ist zu kurz"
        answer["timestamp"] = str(valid_time_max + time.time())
       
    return answer
