#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.BlueFunc

import os
import requests

def sendmail(subject, message, mail_from, mail_to, file):
    if file == "": result = True
    else:
        if os.path.exists(file): result = True
        else: result = False

    if not "@" in mail_from and not "@" in mail_to: result = False
    
    if result:
        url = 'http://sendmail.zaunz.org/upload.php'
        if file == "":
            try:
                response = requests.post(url, data={'subject': subject, "message": message, "from": mail_from, "to": mail_to}, timeout=6)
            except:
                print("Error, mail sending not possible")         
        else:
            try:
                response = requests.post(url, data={'subject': subject, "message": message, "from": mail_from, "to": mail_to}, files={'datei': open(file,'rb')}, timeout=10)
                print("Send email [" + subject + "] to " + mail_to)
                #print(response.text)
            except:
                print("Error, upload not possible") 

    return result
    
   

