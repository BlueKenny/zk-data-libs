#!/usr/bin/env python3
import os
import time

import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

ip = str(sys.argv[1])
secure_key = str(sys.argv[2])
ID = str(sys.argv[3])

os.system("echo '' > /tmp/zk-data-move.csv")
file_data = []

next_move = -1

for x in range(0, 50):
#while True:
    line = []
    move = libs.send.get_article_move(ip, secure_key, next_move, ID)

    if next_move == -1:
        first_line = []
        for key in move:
            first_line.append(key)
        first_line = ";".join(first_line)
        file_data.append(first_line)

    if not move["identification"] == "0":
        # fix for libreoffice
        move["start"] = str(move["start"]).replace(".", ",")
        move["end"] = str(move["end"]).replace(".", ",")
        move["preisek"] = str(move["preisek"]).replace(".", ",")
        move["preisvkh"] = str(move["preisvkh"]).replace(".", ",")
        move["preisvk"] = str(move["preisvk"]).replace(".", ",")

        for var in move:
            #print("var " + str(var))
            line.append(str(move[var]))
        line = ";".join(line)
        file_data.append(line)

        next_move = move["identification"]
    else: break

file_data = "\n".join(file_data)
print(file_data)

os.system("echo \"" + file_data + "\" > /tmp/zk-data-move.csv && libreoffice /tmp/zk-data-move.csv")
