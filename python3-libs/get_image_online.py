#!/usr/bin/env python3

import os
import requests
import random

try: from icrawler.builtin import GoogleImageCrawler
except: os.system("pip3 install icrawler --break-system-packages")

urls = ["https://file.zaunz.org/", "https://file.burkardt.be/"]
no_image = ""

def search_image_online(search):
    directory = "/tmp/article_images/" + str(search).replace(" ", "")
    if not os.path.exists(directory):
        os.system("mkdir -p \"" + directory + "\"")
        
    google_crawler = GoogleImageCrawler(storage={'root_dir': directory})
    google_crawler.crawl(keyword=search, max_num=1)
    
    for i_format in ["png", "jpg"]:
        if os.path.exists(directory + "/000001." + i_format):
            file = directory + "/000001." + i_format
            
            random_index = random.choice(range(len(urls)))
            url = urls[random_index]
            response = requests.post( url + "upload.php", files={'datei': open(file,'rb') } )
                
            link = str(response.content).split('>' + url + 'files/')[-1]
            link = link.split("</a>")[0]
            link = url + "files/" + link
            
            return link
    return no_image
            
def test_image_link(link):
    try:
        r = requests.get(link)
        if r.status_code == 200: return True
        else: return False
    except: return False
