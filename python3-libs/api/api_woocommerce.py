#!/usr/bin/env python3

import sys
sys.path.append("/etc/zk-data-libs/")
import os
import requests
import json
import libs.BlueFunc

headers = {'Content-Type': 'application/json', "charset": "UTF-8"}

max_tries = 3

# def get_info_from_order(api_url, api_username, api_key, order):
# def create_client(api_url, api_username, api_key):
# def set_client(api_url, api_username, api_key, client_id, client_dict):
# def get_client_from_order(api_url, api_username, api_key, order):
# def create_order(api_url, api_username, api_key):
# def get_order(api_url, api_username, api_key, order_id):
# def set_orders(api_url, api_username, api_key, order_id, order):
# def get_orders(api_url, api_username, api_key):
# def search_product(api_url, api_username, api_key, bcode):
# def set_reviews_allowed(api_url, api_username, api_key, identification):
# def set_product_data(api_url, api_username, api_key, stock_id, data):
# def set_product_name(api_url, api_username, api_key, stock_id, name):
# def create_categorie(api_url, api_username, api_key, name):
# def get_categories(api_url, api_username, api_key):
# def set_price(api_url, api_username, api_key, stock_id, price):
# def set_stock(api_url, api_username, api_key, stock_id, quantity):
# def create_product(api_url, api_username, api_key, article_dict):

def get_info_from_order(api_url, api_username, api_key, order):
    print("get_info_from_order(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(order) + ")")

    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    
    url = api_url + "orders/" + str(order) + "/?" + key_line
    #print("url: " + str(url))
    
    r = requests.get(url, data="", headers=headers)
    order = json.loads(r.text)
    line_items = order["line_items"]
    info = []
    
    info.append("WooCommerce order " + str(order["id"]))
    
    for line in line_items:
        #print("line: " + str(line))
        
        unit_price = round((float(line["total"]) + float(line["total_tax"])) / float(line["quantity"]), 2)
        info.append(str(line["quantity"]) + "x " + str(line["sku"]) + " " + str(line["name"]) + " : " + str(unit_price) + " €")        
    
    #print("line_items: " + str(line_items))
    return "\n".join(info)

def get_order(api_url, api_username, api_key, order_id):
    print("get_order(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(order_id) + ")")
            
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "orders/" + str(order_id) + "?" + key_line
    
    for x in range(0, max_tries):
        try:
            r = requests.get(url, headers=headers)
            answer = json.loads(r.text)
            break
        except:
            answer = None
    
    return answer
    
def set_client(api_url, api_username, api_key, client_id, client_dict):
    print("set_client(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(client_id) + ", " + str(client_dict) + ")")

    new_dict = {}
    new_dict["first_name"] = client_dict["name"]
    new_dict["last_name"] = ""
    if client_dict["email2"] == "":
        client_dict["email2"] = client_dict["name"].replace(" ", "") + "_" + str(client_id) + "@zkdata.be"
        client_dict["email2"] = client_dict["email2"].replace("(", "").replace(")", "")
        client_dict["email2"] = client_dict["email2"].replace("-", "")
        client_dict["email2"] = client_dict["email2"].replace("+", "")
        client_dict["email2"] = client_dict["email2"].replace("Ë", "E").replace("Ê", "E").replace("É", "E").replace("È", "E")
    new_dict["email"] = client_dict["email2"].lower()
    print("email2: " + client_dict["email2"])
      
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "customers/" + str(client_id) + "?" + key_line
    r = requests.post(url, data=json.dumps(new_dict), headers=headers)
    #print(r.text)
    
    answer = json.loads(r.text)
    if not "id" in answer: print("ERROR -> " + str(answer))
    
    return answer["id"]
    
def create_client(api_url, api_username, api_key):
    print("create_client(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ")")

    new_dict = {}
    new_dict["first_name"] = "unknown"
    new_dict["email"] = "unknown@" + str(libs.BlueFunc.timestamp()) + ".be"
            
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "customers/?" + key_line
    
    for x in range(0, max_tries):
        try:
            r = requests.post(url, data=json.dumps(new_dict), headers=headers)
            answer = json.loads(r.text)
            answer = answer["id"]
            break
        except:
            answer = None
            
    return answer

def get_client_from_order(api_url, api_username, api_key, order):
    print("get_client_from_order(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(order) + ")")

    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    
    url = api_url + "orders/" + str(order) + "/?" + key_line
    r = requests.get(url, data="", headers=headers)
    order = json.loads(r.text)
    
    client = {}
    
    client["identification"] = order["customer_id"]
    client["name"] = order["billing"]["last_name"] + " " + order["billing"]["first_name"]
    client["adresse"] = order["billing"]["address_1"]
    client["land"] = order["billing"]["country"]
    client["plz"] = order["billing"]["postcode"]
    client["ort"] = order["billing"]["city"]
    if "email" in order["shipping"]:
        client["email2"] = order["shipping"]["email"]
    client["email3"] = order["billing"]["email"]
    client["tel1"] = order["billing"]["phone"]
    client["tel2"] = order["shipping"]["phone"]
    
    return client

def set_order(api_url, api_username, api_key, order_id, order_dict):
    print("set_order(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(order_id) + ", " + str(order_dict) + ")")

    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "orders/" + str(order_id) + "?" + key_line
    r = requests.post(url, data=json.dumps(order_dict), headers=headers)
    #print(r.text)

    answer = json.loads(r.text)
    if not "id" in answer: print("ERROR -> " + str(answer))

    return True #answer["id"]
    
def create_order(api_url, api_username, api_key):
    print("create_order(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ")")

    new_dict = {}
    new_dict["set_paid"] = False
    #new_dict["created_via"] = "zk-data"
    
            
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "orders/?" + key_line
    r = requests.post(url, data=json.dumps(new_dict), headers=headers)
    #print(r.text)
    
    answer = json.loads(r.text)
    if not "id" in answer: print("ERROR -> " + str(answer))
    
    return answer["id"]
    
def get_orders(api_url, api_username, api_key):
    print("get_orders(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ")")
    
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    page = 1
    orders = []
    
    while True:    
        url = api_url + "orders?" + key_line + "&page=" + str(page)
        
        r = requests.get(url, data="", headers=headers)
        new_orders = json.loads(r.text)
        print("url: " + str(url))

        if len(new_orders) == 0:
            break
        else:
            #print("new_orders: " + str(new_orders))
            for order in new_orders:
                #print("order id: " + str(order))
                orders.append(order["id"])
            page = page + 1

    return reversed(orders)

def search_product(api_url, api_username, api_key, bcode):
    print("search_product(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(bcode) + ")")

    bcode = str(bcode)
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "products?" + key_line + "&sku=" + str(bcode)
    
    print("url: " + str(url))
    
    for x in range(0, max_tries):
        try:
            r = requests.get(url, data="", headers=headers)
            products = json.loads(r.text)
            products = list(products)
        
            if len(products) == 0:
                answer = None
            else:        
                if "type" in products[0]:
                    if products[0]["type"] == "variation":        
                        answer = str(products[0]["parent_id"]) + "/variations/" + str(products[0]["id"])
                    else:
                        answer = str(products[0]["id"])
                else:
                    answer = None
            break
        except:
            print("ERROR in search_product (try:" + str(x) + ")")
            answer = None
   
    print("answer: " + str(answer))
    return answer
        
def set_reviews_allowed(api_url, api_username, api_key, identification):
    print("set_reviews_allowed(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(identification) + ")")
    
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "products?" + key_line + "&sku=" + str(identification)
    print("url: " + str(url))
    r = requests.get(url, data="", headers=headers)
    products = json.loads(r.text)
    products = list(products)
    
    if len(products) == 1:                    
        print("products[0]: " + str(products[0]))
        if "type" in products[0]:
            if products[0]["type"] == "variation":        
                stock_id = str(products[0]["parent_id"]) + "/variations/" + str(products[0]["id"])
            else:
                stock_id = str(products[0]["id"])
        
            print("Set reviews_allowed to True" )        
            key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
            url = api_url + "products/" + str(stock_id) + "?" + key_line
            r = requests.put(url, data=json.dumps({"reviews_allowed": True}), headers=headers)
    
    if len(products) == 2:
        print("products[1]: " + str(products[1]))
        if "type" in products[1]:
            if products[1]["type"] == "variation":        
                stock_id = str(products[1]["parent_id"]) + "/variations/" + str(products[1]["id"])
            else:
                stock_id = str(products[1]["id"])
        
            print("Set reviews_allowed to True" )        
            key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
            url = api_url + "products/" + str(stock_id) + "?" + key_line
            r = requests.put(url, data=json.dumps({"reviews_allowed": True}), headers=headers)

        
def set_product_data(api_url, api_username, api_key, stock_id, data):
    print("set_product_data(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(stock_id) + ", " + str(data) + ")")
    
    if not stock_id == 0 and not stock_id == None:
        key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
        url = api_url + "products/" + str(stock_id) + "?" + key_line
        r = requests.put(url, data=json.dumps(data), headers=headers)
        #print(r.text)
        answer = json.loads(r.text)
    else:
        answer = json.loads({"stock_id": "Invalid"})
    return answer
        
def set_product_name(api_url, api_username, api_key, stock_id, name):
    print("set_product_name(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(stock_id) + ", " + str(name) + ")")
    
    if not stock_id == 0 and not stock_id == None:
        key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
        url = api_url + "products/" + str(stock_id) + "?" + key_line
        r = requests.put(url, data=json.dumps({"name": str(name)}), headers=headers)
        #print(r.text)
        answer = json.loads(r.text)
    else:
        answer = json.loads({"stock_id": "Invalid"})
    return answer
        
def create_categorie(api_url, api_username, api_key, name):
    print("get_categories(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(name) + ")")
    
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "products/categories" + "?" + key_line
    r = requests.post(url, data=json.dumps({"name": str(name)}), headers=headers)
    #print(r.text)
    answer = json.loads(r.text)
    
    return answer
      
def get_categories(api_url, api_username, api_key):
    print("get_categories(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ")")
    
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "products/categories" + "?" + key_line
    r = requests.get(url, headers=headers)
    #print(r.text)
    answer = json.loads(r.text)
    
    return answer
    
def set_product_price(api_url, api_username, api_key, stock_id, price):
    print("set_product_price(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(stock_id) + ", " + str(price) + ")")
    
    if not stock_id == 0 and not stock_id == None:          
        price = float(price)     
        price = str(price)
        
        print("Set price to " + str(price) )
        
        key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
        url = api_url + "products/" + str(stock_id) + "?" + key_line
        r = requests.put(url, data=json.dumps({"regular_price": price}), headers=headers)
        #print(r.text)
        answer = json.loads(r.text)
    else:
        answer = json.loads({"stock_id": "Invalid"})
    return answer
         
def set_stock(api_url, api_username, api_key, stock_id, quantity):
    print("set_stock(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(stock_id) + ", " + str(quantity) + ")")
    
    if not stock_id == 0 and not stock_id == None:  
        
        quantity = float(quantity) # remove decimals
        quantity = str(quantity).split(".")[0]
        quantity = int(quantity)
        
        print("Stock set to " + str(quantity) )
        
        key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
        url = api_url + "products/" + str(stock_id) + "?" + key_line
        r = requests.put(url, data=json.dumps({"stock_quantity": quantity}), headers=headers)
        #print(r.text)
        answer = json.loads(r.text)
    else:
        answer = json.loads({"stock_id": "Invalid"})
    return answer

def create_product(api_url, api_username, api_key, article_dict):
    print("create_product(" + str(api_url) + ", " + str(api_username) + ", " + str(api_key) + ", " + str(article_dict) + ")")

    new_dict = {}
    new_dict["name"] = str(article_dict["name_de"])
    new_dict["type"] = "simple"
    new_dict["regular_price"] = str(article_dict["preisvk"])
    #new_dict["description"] = "Pellentesque"
    #new_dict["short_description"] = "habitant"
    new_dict["sku"] = str(int(article_dict["identification"]))
            
    key_line = "consumer_key=" + api_username + "&consumer_secret=" + api_key
    url = api_url + "products/?" + key_line
    r = requests.post(url, data=json.dumps(new_dict), headers=headers)
    #print(r.text)
    
    answer = json.loads(r.text)
    if not "id" in answer: print("ERROR -> " + str(answer))
    
    return answer["id"]
