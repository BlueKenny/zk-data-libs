#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")
import os

data_dir = "/etc/zk-data/qml/libs/plz_ort_data/"


def search(land, suche):
    print("plz_ort.search(" + str(land) + ", " + str(suche) + ")")
    answer = []
    land = str(land).lower()
    if os.path.exists(data_dir + land + ".csv"):
        
        for line in open(data_dir + land + ".csv", "r").readlines():
            line = line.rstrip()
            
            if ";" in line:
                if suche.lower() in line.lower() or suche == "":
                    line = line.split(";")
                    answer.append({"plz": str(line[0]), "ort": str(line[1])})
        

    return answer
