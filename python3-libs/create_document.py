#!/usr/bin/env python3
import sys
sys.path.append("/etc/zk-data-libs/")
import libs.send

import os


trennzeichen = ";"

def get_list_invoices(ip, secure_key, identification, client, only_rest, only_due):
    invoices = ["identification" + trennzeichen + "date" + trennzeichen + "date_todo" + trennzeichen + "client_identification" + trennzeichen + "client_name" + trennzeichen + "vat" + trennzeichen + "tel1" + trennzeichen + "rest" + trennzeichen + "total"]

    index = 0
    while True:
        query_id = libs.send.search_invoice(ip, secure_key, identification, client, only_rest, "", index)
        if query_id == -1: break
        else:
            invoice = libs.send.get_invoice(ip, secure_key, query_id)
            kunde = libs.send.get_client(ip, secure_key, invoice["kunde_id"])
            
            line = query_id + trennzeichen + invoice["datum"] + trennzeichen + invoice["datum_todo"] + trennzeichen + invoice["kunde_id"] + trennzeichen + invoice["kunde_name"] + trennzeichen + kunde["tva"] + trennzeichen + str(kunde["tel1"]) + trennzeichen + str(invoice["rest"]).replace(".", ",") + trennzeichen + str(invoice["total"]).replace(".", ",")
            invoices.append(line)
            index = index + 1
    
    print(len(invoices))
    open("/tmp/invoices.csv", "w").write("\n".join(invoices))
    os.system("libreoffice /tmp/invoices.csv")
    
