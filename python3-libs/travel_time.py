#! /usr/bin/env python3
import time

import sys
sys.path.append("/etc/zk-data-libs/")

import libs.send
import libs.BlueFunc

import requests
import json

def get_coordinations(ort, adr):
    ort = str(ort).replace(" ", "+")
    adr = str(adr).replace(" ", "+")
    print("get_coordinations(" + ort + ", " + adr + ")")
    
    answer = requests.get("https://nominatim.openstreetmap.org/search?q=" + ort + "," + adr + "&format=json&polygon=1&addressdetails=1")
    #print(answer.text)
    
    lat = json.loads(answer.text)[0]["lat"]
    lon = json.loads(answer.text)[0]["lon"]
    
    return {"lon": lon, "lat": lat}

def get_distance(cord1, cord2):
    print("get_distance(" + str(cord1) + ", " + str(cord2) + ")")
    answer = requests.get("http://router.project-osrm.org/route/v1/driving/" + str(cord1["lon"]) + "," + str(cord1["lat"]) + ";" + str(cord2["lon"]) + "," + str(cord2["lat"]) + "?overview=false")
    #print(answer.text)
    
    duration = json.loads(answer.text)["routes"][0]["legs"][0]["duration"]
    distance = json.loads(answer.text)["routes"][0]["legs"][0]["distance"]
    
    duration = round(duration / 60, 1)
    distance = round(distance / 1000, 1)
    
    return {"distance": distance, "duration": duration}

#cord_burk = get_coordinations("SOURBRODT", "RUE DE LA STATION 29")
#print("cord_burk: " + str(cord_burk))

#cord_client = get_coordinations("BULLINGEN", "HONSFELD 442")
#print("cord_client: " + str(cord_client))

#distance = get_distance(cord_burk, cord_client)
#print("distance: " + str(distance))

