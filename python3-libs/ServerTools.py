#!/usr/bin/env python3

import os

import sys
sys.path.append("/etc/zk-data-libs/")

import libs.BlueFunc

#libs.ServerTools.AddToMinimum(Anzahl, RestAnzahl, Artikel, Lieferant, Name, Identification)
#libs.ServerTools.CheckIfPrint()
#
#Date()
#Timestamp()
#getFreieBewegungID()
#SearchKunden(Dict) ####ToDo
#SearchArt(Dict)
#
#
#linie 290

from datetime import datetime

import requests
import random

import time

def set_server_ip(server_name):
    print("ServerTools: set_server_ip(" + str(server_name) + ")")

    server_name = str(server_name).lower()
    
    data_dir = "/etc/zk-data-server/data/" + server_name + "/"
    print("data_dir: " + data_dir)
    
    if not server_name == "none":
        server_key = str(libs.BlueFunc.BlueLoad("server_key", data_dir)).upper()
        if server_key == "NONE":
            server_key = str(random.randint(100000000, 900000000))
            libs.BlueFunc.BlueSave("server_key", server_key, data_dir)
        
        ip_intern = os.popen("hostname -I").readlines()[0].split(" ")[0]
        print("ip_intern: " + str(ip_intern)) 
        
        ip_extern = os.popen("curl --connect-timeout 4000 -4 icanhazip.com").readlines()        
        try:
            ip_extern = ip_extern[0].rstrip()
            print("ip_extern: " + str(ip_extern))
            r = requests.get( "http://zk-data.zaunz.org/set_ip.php?ip_extern=" + str(ip_extern) + "&ip_intern=" + str(ip_intern) + "&name=" + server_name + "&key=" + server_key )
        except:
            print("Connection Timeout")
        #print("result: " + str(r.text))
 
def export_to_web_tmp(source_file):
    answer_path = "/tmp/" + str(random.randint(100000, 999999)) + "/" + str(random.randint(100000, 999999)) + "/"
    destination_path = "/etc/zk-data-server/web" + answer_path
    filename = source_file.split("/")[-1]
    answer_path = answer_path + filename
    if not os.path.exists(destination_path):
        os.system("mkdir -p " + destination_path)
        os.system("chmod -R 777 " + destination_path)
    os.system("cp " + source_file + " " + destination_path)
    return answer_path
    
def create_list_for_stats(dict_of_dates):
    print("create_list_for_stats(" + str(dict_of_dates) + ")")

    list_x = []
    list_y = []
    
    if len(dict_of_dates) < 20: # sort by dates
        print("sort by day")
        for date in sorted(dict_of_dates):
            list_x.append(str(date))
            list_y.append(dict_of_dates[date])
            
    elif len(dict_of_dates) < 40: # sort by weeks TODO
        print("sort by weeks")
        dict_of_dates_by_month = {}
        for date in sorted(dict_of_dates):
            date_split = date.split("-")
            f_date = date_split[0] + "-" + date_split[1]
            if f_date in dict_of_dates_by_month:
                dict_of_dates_by_month[f_date] = round(dict_of_dates_by_month[f_date] + dict_of_dates[date], 2)
            else:
                dict_of_dates_by_month[f_date] = round(dict_of_dates[date], 2)
        
        for date in sorted(dict_of_dates_by_month):
            list_x.append(str(date))
            list_y.append(dict_of_dates_by_month[date])
            
    elif len(dict_of_dates) < 600: # sort by months
        print("sort by month")
        dict_of_dates_by_month = {}
        for date in sorted(dict_of_dates):
            date_split = date.split("-")
            f_date = date_split[0] + "-" + date_split[1]
            if f_date in dict_of_dates_by_month:
                dict_of_dates_by_month[f_date] = round(dict_of_dates_by_month[f_date] + dict_of_dates[date], 2)
            else:
                dict_of_dates_by_month[f_date] = round(dict_of_dates[date], 2)
        
        for date in sorted(dict_of_dates_by_month):
            list_x.append(str(date))
            list_y.append(dict_of_dates_by_month[date])
            
    else: # sort by years
        print("sort by years")
        dict_of_dates_by_years = {}
        for date in sorted(dict_of_dates):
            date_split = date.split("-")
            f_date = date_split[0]
            if f_date in dict_of_dates_by_years:
                dict_of_dates_by_years[f_date] = round(dict_of_dates_by_years[f_date] + dict_of_dates[date], 2)
            else:
                dict_of_dates_by_years[f_date] = round(dict_of_dates[date], 2)
        
        for date in sorted(dict_of_dates_by_years):
            list_x.append(str(date))
            list_y.append(dict_of_dates_by_years[date])
         
    return [list_x, list_y]   


def convert_ods_to_pdf(list_of_files):
    print("convert_ods_to_pdf(" + str(list_of_files) + ")")
    timestamp = str(libs.BlueFunc.Timestamp())
    cmd = ["pdftk"]
    for file in list_of_files:
        if os.path.exists(file): print(file + " exists")
        else: print(file + " doesn't exists !!!!!!!!!!")
        
        if ".ods" in file:
            os.system("cd /tmp/ && libreoffice --convert-to pdf:writer_pdf_Export " + file)
            new_file = file.replace(".ods", ".pdf")
        elif ".pdf" in file:
            new_file = file
            
        if os.path.exists(new_file): print(new_file + " created")
        else: print("creation of " + new_file + " failed")
            
        cmd.append(new_file)
        
    cmd = " ".join(cmd)
    final_pdf = "/tmp/" + timestamp + ".pdf"
    os.system(cmd + " cat output " + final_pdf)
    if os.path.exists(final_pdf): print("file " + final_pdf + " created")
    return final_pdf

def write_ods_file(server_name, default_ods_file, content, image_list):
    print("write_ods_file(" + str(default_ods_file) + ", content..., " + str(image_list) + ")")
    
    timestamp = str(libs.BlueFunc.Timestamp())
    new_ods_dir = "/tmp/prepare_ods_" + timestamp
    new_ods_filename = "/tmp/" + timestamp + ".ods"
    
    default_file_dir = "/tmp/" + server_name + "/" + default_ods_file.split("/")[-1] + "/"
    
    os.system("cp -r " + default_file_dir + " " + new_ods_dir)
    open(new_ods_dir + "/content.xml", "w").write(content)
    
    if not image_list == []:
        for i in range(0, len(image_list)):
            path_of_old_image = new_ods_dir + "/" + image_list[i][0]
            path_of_new_image = image_list[i][1]
            os.system("cp " + path_of_new_image + " " + path_of_old_image)
                       
    #os.system("cd " + new_ods_dir + " && zip -r -q " + new_ods_filename + " * ")
    os.system("cd " + new_ods_dir + " && zip -r -q " + new_ods_filename + " * && rm -r " + new_ods_dir)
    
    if not os.path.exists(new_ods_filename):
        print("Error ? File " + new_ods_filename + " don't exists!!!!!")
    else:
        print("File " + new_ods_filename + " created")
        
    return new_ods_filename
    
def get_ods_content(server_name, default_ods_file):
    if os.path.exists(default_ods_file) and ".ods" in default_ods_file:
        if not os.path.exists("/tmp/" + server_name): os.system("mkdir -p /tmp/" + server_name)
        
        default_file_dir = "/tmp/" + server_name + "/" + default_ods_file.split("/")[-1] + "/"
        
        if not os.path.exists(default_file_dir):
            os.system("unzip -q " + default_ods_file + " -d " + default_file_dir)
            
        data = open(default_file_dir + "content.xml", "r").read()
        
    else:
        print("ERROR in get_ods_content(...)")
        data = "ERROR"
    
    return data
    
    
def Date():
    Datum = str(datetime.now().strftime('%Y-%m-%d-%H-%M-%S'))
    print("Date(): " + Datum)
    return Datum

def AddToMinimum(Anzahl, RestAnzahl, Artikel, Lieferant, Name, Identification): # to be removed ! No more in use
    Anzahl = str(Anzahl)
    RestAnzahl = str(RestAnzahl)
    Artikel = str(Artikel)
    Lieferant = str(Lieferant)
    Name = str(Name)
    Identification = str(Identification)
    print("AddToMinimum(" + Anzahl + ", " + RestAnzahl + ", " + Artikel + ", " + Lieferant + ", " + Name + ", " + Identification + ")")

    try: Inhalt = open("DATA/MINIMUM", "r").read()
    except:
        Inhalt = []
        print("Keine DATA/MINIMUM Datei")
    SchonInDerDatei = False
    for eachLine in Inhalt.split("\n"):
        print("eachLine: " + str(eachLine))
        if "[" + Identification + "]" in eachLine:
            SchonInDerDatei = True
            newAnzahl = str(float(eachLine.split("]    ")[1].split("x    ")[0]) + float(Anzahl))
            newLine = "[" + Lieferant + "]    " + newAnzahl + "x    " + Artikel + "   " + Name + "       [Rest: " + RestAnzahl + "][" + Identification + "]\n"

            Inhalt = Inhalt.replace(eachLine, newLine)
            open("DATA/MINIMUM", "w").write(Inhalt)

    if not SchonInDerDatei:
        file = open("DATA/MINIMUM", "a")
        file.write("[" + Lieferant + "]    " + Anzahl + "x    " + Artikel + "   " + Name + "       [Rest: " + RestAnzahl + "][" + Identification + "]\n")

    CheckIfPrint()

def CheckIfPrint():    
    file = open("DATA/MINIMUM", "r")
    lines = file.readlines()
    if len(lines) > int(libs.BlueFunc.getData("Minimum_Anzahl_Der_Linien")):
        os.system("lpr DATA/MINIMUM")
        #os.system("gedit DATA/MINIMUM")
        if not os.path.exists("DATA/MINIMUM-ARCHIVE/"): os.system("mkdir DATA/MINIMUM-ARCHIVE")
        os.system("cp -n DATA/MINIMUM DATA/MINIMUM-ARCHIVE/" + str(Date()))
        os.remove("DATA/MINIMUM")
        open("DATA/MINIMUM", "a").write("    MINIMUM LISTE    |    ZU BESTELLEN\n\n")
