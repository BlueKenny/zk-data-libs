#!/usr/bin/env python3


import os

os.system("cd /tmp/ && wget -q -N http://repository.zaunz.org/deb/Packages.gz && gzip -f -d /tmp/Packages.gz")
data = open("/tmp/Packages", "r").read()
data_lines = data.split("\n\n")

packages_to_update = []

for package in ["zk-data-libs", "zk-data-client", "zk-data-pos", "zk-data-server"]:
    print("package: " + package)
    
    app_info = os.popen("apt info " + package).read()
    if "Version: " in app_info:# if installed
        installed_version = app_info.split("Version: ")[1].split("\n")[0]
        print("installed_version: " + installed_version)
        
        for line in data_lines:
            if "Package: " + package in line:
                #print("\nline: " + str(line))
                new_version = line.split("Version: ")[1].split("\n")[0]
                #print("new_version: " + new_version)
                
                if float(new_version) > float(installed_version):
                    packages_to_update.append(package)
    
print("packages_to_update: " + str(packages_to_update))
