#!/usr/bin/env python3

import datetime

def convert(identification):
    print("convert(" + str(identification) + ")")
    
    # REF +++010/8191/47402+++
    ref = str(identification)
    while len(ref) < 8:
        ref = "0" + ref
    year = "19"#str(datetime.datetime.now().strftime("%y"))
    ref = year + ref
    checksum = int(ref)%97
    if len(str(checksum)) == 1: 
        checksum = "0" + str(checksum)
    
    ref = "+++" + ref[0] + ref[1] + ref[2] + "/" + ref[3] + ref[4] + ref[5] + ref[6] + "/" + ref[7] + ref[8] + ref[9] + str(checksum) + "+++"
    
    print("ref: " + str(ref))
    return ref            
                
                
               
               #+++190/1975/58469+++ 
convert(1975584)
