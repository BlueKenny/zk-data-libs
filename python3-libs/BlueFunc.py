#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import time
import datetime
import codecs

import operator

import getpass

import socket

import sys
sys.path.append("/etc/zk-data-libs/")

import hashlib

from peewee import *
from playhouse.migrate import *
from playhouse.shortcuts import model_to_dict, dict_to_model

try: import xdg
except: 
    os.system("pip3 install --user xdg")
    import xdg

from pathlib import Path

import requests
import json

#from urllib.request import urlretrieve

#libs.BlueFunc.getData(variable)
#libs.BlueFunc.setData(name, variable)

def send_telegram_text(text):
    chatid = "-756286604";
    token = "1342927924:AAGziCsgMSkKlcRZdzwFP_DvpJ_3m6BhBrk";
    data = {
        'text': text,
        'chat_id': chatid
    }	
    url = "https://api.telegram.org/bot" + token + "/sendMessage"
	
    response = requests.get(url, data=data)
    
def get_version():
    return str(open("/etc/zk-data-libs/version", "r").readlines()[0]).rstrip()

def sort_list_of_dict(list_of_dict, key_one, key_two):
    print("sort list of dict by " + str(key_one) + " and " + str(key_two))
    #list_of_dict.sort(key=operator.itemgetter(key))
    #sorted_list_of_dict = sorted(list_of_dict, key=lambda i: (i[key_one], i[key_two]))
    sorted_list_of_dict = sorted(list_of_dict, key=operator.itemgetter(key_one, key_two))
    return sorted_list_of_dict
    
def split_list_of_dict_by_key(list_of_dict, key, var):
    list_without_var = []
    list_with_var = []

    for each_dict in list_of_dict:
        if key in each_dict:
            if var == each_dict[key]:
                list_with_var.append(each_dict)
            else:
                list_without_var.append(each_dict)
        else:
            list_without_var.append(each_dict)

    return [list_without_var, list_with_var]

def code_to_dates(code):
    print("code_to_dates(" + str(code) + ")")
    code = str(code)
        
    if code.isdigit():            
        if code == "0": # Heute
            date_start = Date()
            date_end = Date()            
            answer = [date_start, date_end]
            
        elif code == "1": # diese Woche
            # TODO
            date_start = Date()
            date_end = Date()
            answer = [date_start, date_end]
        
        elif code == "2": # diesen Monat
            date = Date().split("-")
            date_start = date[0] + "-" + date[1] + "-01"
            date_end = date[0] + "-" + date[1] + "-31"  
            answer = [date_start, date_end]
                      
        elif code == "3": # dieses Quartal
            month = int(Date().split("-")[1])
            year = Date().split("-")[0]
            
            if month < 4: # Q1
                date_start = year + "-01-01"
                date_end = year + "-03-31"
            elif month < 7: # Q2
                date_start = year + "-04-01"
                date_end = year + "-06-30"
            elif month < 10: # Q3
                date_start = year + "-07-01"
                date_end = year + "-09-30"
            else: # Q4
                date_start = year + "-10-01"
                date_end = year + "-12-31"  
            answer = [date_start, date_end]         
            
        elif code == "4": # dieses Jahr
            date = Date().split("-")
            date_start = date[0] + "-01-01"
            date_end = date[0] + "-12-31"
            answer = [date_start, date_end]
            
        elif code == "5": # Gestern
            date_start = TodayAddDays(-1)
            date_end = TodayAddDays(-1)
            answer = [date_start, date_end]
            
        elif code == "6": # Letzte Woche
            # TODO
            date_start = Date()
            date_end = Date()
            answer = [date_start, date_end]
            
        elif code == "7": # Letzter Monat
            date = Date().split("-")
            year = date[0]
            month = date[1]            
            if month == "01":
                year = str(int(year) - 1)
                month = "12"
            else:
                month = str(int(month) - 1)
                if len(month) == 1: month = "0" + month
            date_start = year + "-" + month + "-01"
            date_end = year + "-" + month + "-31"  
            answer = [date_start, date_end]
            
        elif code == "8": # Letztes Quartal
            month = int(Date().split("-")[1])
            year = Date().split("-")[0]
            if month < 4: # Q1 => Q4
                date_start = str(int(year)-1) + "-10-01"
                date_end = str(int(year)-1) + "-12-31"     
            elif month < 7: # Q2 => Q1
                date_start = year + "-01-01"
                date_end = year + "-03-31"
            elif month < 10: # Q3 => Q2
                date_start = year + "-04-01"
                date_end = year + "-06-30"
            else: # Q4 = Q3
                date_start = year + "-07-01"
                date_end = year + "-09-30"  
            answer = [date_start, date_end]
            
        elif code == "9": # Letztes Jahr
            date = Date().split("-")
            letztes_jahr = str(int(date[0])-1)
            date_start = letztes_jahr + "-01-01"
            date_end = letztes_jahr + "-12-31"
            answer = [date_start, date_end]
            
        else:
            date_start = Date()
            date_end = Date()
            answer = [date_start, date_end]
        
    
    else:
        answer = ["Heute", "diese Woche (WIP)", "diesen Monat", "dieses Quartal", "dieses Jahr", "Gestern", "Letzte Woche (WIP)", "Letzter Monat", "Letztes Quartal", "Letztes Jahr"]
 
    return answer

def translate(source_lang, target_lang, text):
    print("translate(" + str(source_lang) + ", " + str(target_lang) + ", " + str(text) + ")")

    data = {}
    
    data["source_lang"] = source_lang.upper()
    data["target_lang"] = target_lang.upper()
    data["text"] = text
    
    url = "https://api.zaunz.org/translate" + "?source_lang=" + str(data["source_lang"]) + "&target_lang=" + str(data["target_lang"]) + "&text=" + str(data["text"])
    #url = "https://api.zaunz.org/translate"
    #url = urlretrieve(url)
        
    r = requests.get(url, data=data)
    #print("r.text: " + str(r.text))
    answer = json.loads(r.text) 
    #answer = answer["translations"][0]["text"]
    #answer = r.text
    
    return answer

def upload(ip, datei):
    print("upload(" + str(datei) + ")")
    
    if os.path.exists(datei):        
        while True:
            print("start upload of " + str(datei))
        
            url = "http://" + str(ip).split(":")[0] + "/zk-data/file/upload.php"
            print("url for upload: " + str(url))
        
            response = requests.post( url, files={'datei': open(datei,'rb') } )
            print("response.status_code: " + str(response.status_code))
            
            if "200" in str(response.status_code):            
                link = str(response.text)
                print("link: " + str(link))
                if "/files/" in link:
                    link = link.split("/files/")[1]
                    if "<html>" in link:
                        link = link.split("<html>")[0]
                        
                    link = url.replace("upload.php", "") + "files/" + link
                    print("Test if upoad sucessfull at " + link)
                    test = requests.get(link)
                    if "200" in str(test.status_code):   
                        print("File uploaded !")
                        break
                    else:
                        print("ERROR File upload not sucessfull")
            else:
                print("Server don't return code 200")
                            
                            
            print("Restart upload in 2s")
            time.sleep(2)
        
        link = "http://" + str(ip).split(":")[0] + "/zk-data/file/files/" + link
        print("link: " + link)
        return link
    else:
        print("datei existiert nicht")
        return ""

def create_pdf(pdf_liste):
    print("create_pdf(" + str(pdf_liste) + ")")
    cmd = "img2pdf"
    timetmp = str(timestamp())
    output_file_big = "/tmp/zk-data_pdf/big_" + timetmp + ".pdf"
    output_file = "/tmp/zk-data_pdf/" + timetmp + ".pdf"
    
    if not os.path.exists("/tmp/zk-data_pdf/"):
        os.mkdir("/tmp/zk-data_pdf/")
        
    for pdf in pdf_liste:
        cmd = cmd + " '" + pdf + "'"
    cmd = cmd + " -o '" + output_file_big + "'"
    
    #use ps2pdf
    cmd = cmd + " && ps2pdf " + output_file_big + " " + output_file
    
    print(cmd)
    os.system(cmd)
        
    counter = 0
    while True:
        counter = counter + 1
        print("check if pdf is created")
        time.sleep(1)
        if os.path.exists(output_file):
            print("pdf found")
            break
        if counter == 5:
            print("Error file not found")
            print("restart founction !")
            output_file = create_pdf(pdf_liste)
    
    return output_file

def pdf_to_ppm(datei, identification):
    print("pdf_to_ppm(" + str(datei) + ", " + str(identification) +")")
    
    if "http://" in datei or "https://" in datei:
        name = datei.split("/")[-1]
        
        os.system("wget  -O /tmp/" + name + " " + datei)
        
        datei = "/tmp/" + name    
    
    datei = datei.replace("file://", "")
    dateiname = datei.split("/")[-1]
    dateiname_without_pdf = dateiname.replace(".pdf", "").replace(".PDF", "")
    
    tmp_dir = "/tmp/zk-data_pdf_to_ppm/" + str(timestamp())
    
    if not os.path.exists("/tmp/zk-data_pdf_to_ppm/"):
        os.mkdir("/tmp/zk-data_pdf_to_ppm/")
        
    if not os.path.exists(tmp_dir):
        os.mkdir(tmp_dir)
        
    os.system("cp '" + str(datei) + "' " + str(tmp_dir))
    
    cmd = "cd " + tmp_dir + " && pdftoppm '" + dateiname + "' '" + dateiname_without_pdf + "'"
    
    print("cmd: " + cmd)
    os.system(cmd)
    
    images = []
    
    for image in sorted(os.listdir(tmp_dir)):
        if ".ppm" in image:
            images.append(tmp_dir + "/" + image)
            
    if not identification == "":
        first_image_old_name = images[0]
        first_image_new_name = first_image_old_name + "_new_name"
        
        x = 500.0
        filesize = os.popen("file " + first_image_old_name).read()
        
        print("filesize: " + filesize)
        if "size = " in filesize:
            filesize = filesize.split("size = ")[1]
            filesize = filesize.split(", ")[0]
            filesize = filesize.split(" x ")
            
            x = float(filesize[0])
            
        else:
            identification = "?"
            
        os.system("ppmlabel -x " + str(x-550) + " -y 65 -color darkred -size 50 -text \"RE" + str(identification) + "\" " + first_image_old_name + " > " + first_image_new_name)
        
        images[0] = first_image_new_name
    
    return images
    
def ultra_simple_text(txt):
    print("ultra_simple_text(" + str(txt) + ")")
    
    txt = str(txt).upper()
    answer = ""
    
    for char in txt:
        if char.isdigit():
            answer = answer + str(char)
        else:
            if char.isalpha():
                answer = answer + str(char)
            else:
                answer = answer + "_"
                
    return answer

def simple_text(txt):
    print("simple_text(" + str(txt) + ")")    
    
    txt = str(txt).upper()
    txt = txt.replace("Ä", "A")
    txt = txt.replace("À", "A")
    txt = txt.replace("ç".upper(), "C")
    txt = txt.replace("È", "E")
    txt = txt.replace("É", "E")
    txt = txt.replace("Ê", "E")
    txt = txt.replace("Ü", "U")
    txt = txt.replace("Ù", "U")
    txt = txt.replace("Û", "U")
    txt = txt.replace("Ö", "O")
    txt = txt.replace("\\n", "")
    txt = txt.replace("\\r", "")
    txt = txt.replace("/n", "")
    txt = txt.replace("/r", "")
    txt = txt.rstrip()
    
    return txt
    
def set_start_function(cmd):
    print("set_start_function(" + str(cmd) + ")")
    if not os.path.exists("/tmp/zk-data"): 
        os.mkdir("/tmp/zk-data")        
    open("/tmp/zk-data/start", "w").write(cmd)

def online_monitor():
    print("online_monitor()")
    software = "zk-data-client"
    sn = get_unique_name()
    version = get_version()
    r = requests.get("http://online-monitor.zaunz.org?software=" + software + "&sn=" + sn + "&version=" + version, timeout=2)
    print(r.text)

def get_unique_name():
    print("get_unique_name()")
    data = os.popen("udevadm info --query=all --name=/dev/sda | grep ID_SERIAL=").read()
    username = str(getpass.getuser())
    data = str(data).rstrip()
    data = data.split("ID_SERIAL=")[1]
    return data + "_" + username

def split_by_length(data, len_to_split):
    print("split_by_length(" + str(data) + ", " + str(len_to_split) + ")")
    answer = []
    
    if type(data) == str:
        is_string = True
        new_part = ""
    else:
        is_string = False
        new_part = []
    #print("is_string: " + str(is_string))
    
    for i in data:
        if len(new_part) == len_to_split:
            answer.append(new_part)
            if is_string:
                new_part = str(i)
            else:
                new_part = [i]
        else:
            if is_string: 
                new_part = str(new_part) + str(i)
            else:
                new_part.append(i)
    if answer == []:
        answer.append(new_part)
    else:
        if not answer[-1] == new_part:
            answer.append(new_part)    
        
    #print("answer: " + str(answer))
    return answer
        

def getLastChar(text):
    text = str(text)[-1]
    return str(text)

def getHomeDir():
    home_dir = str(os.popen("echo $HOME").readlines()[0]).rstrip()
    #print("getHomeDir(): " + home_dir)
    return home_dir

def getClipboard():
    try: clipboard = os.popen("xclip -o").readlines()[0]
    except: clipboard = ""
    
    clipboard = str(clipboard).rstrip().replace("-", "").replace(" ", "")
    
    return clipboard

def setClipboard(text):
    print("setClipboard(" + str(text) + ")")
    os.system("echo \"" + str(text) + "\" | xclip -selection clipboard")

def BlueMkDir(directory):#	Macht ein verzeichnis wenn es noch nicht existiert
    print("BlueMkDir(" + str(directory) + ")")
    #directory = forPhone(directory)

    if not os.path.exists(directory):
        print("Create " + str(directory))
        os.makedirs(directory)
        #os.mkdir(directory)
    else:
        print("directory exists")
        
def getDir(file):
    #if os.path.exists("/home/phablet"):
    #    pfad = "/home/phablet/.local/share/zk-data.bluekenny/"
    #    if not os.path.exists(pfad + "DATA"): os.makedirs(pfad + "DATA")
    #    if not pfad in file:
    #        file = pfad + file
    #        file = file.replace("//", "/")
    if True: #else:
        #directory = str(Path(xdg.XDG_CONFIG_HOME)) + "/zk-data/"
        directory = str(Path.home()) + "/.config/zk-data/"
        if not os.path.exists(directory): os.mkdir(directory)
        if not directory in file: file = directory + file
        #print("file: " + str(file))

    return str(file)

def file_split(file_as_string, length):
    result = [file_as_string[y - length:y] for y in range(length, len(file_as_string) + length, length)]
    return result
   
def Date():
    now = datetime.datetime.now()
    zeit = str(now.strftime("%Y-%m-%d"))
    return zeit
    
def DateAddDays(date, number):
    date = str(date)
    number = int(number)
    date_object = datetime.datetime.strptime(date,"%Y-%m-%d")
    date_object = date_object + datetime.timedelta(days=number)
    date_object = str(date_object).split(" ")[0]
    return str(date_object)

def TodayAddDays(number):
    number = int(number)
    date = Date()    
    return DateAddDays(date, number)
    
def uhrzeit():
    now = datetime.datetime.now()
    zeit = str(now.strftime("%H:%M"))
    return zeit
    
def date_reversed(date):
    print("date_reversed(" + str(date) + ")")
    try:
        date = str(date).split("-")
        reversed_date = date[2] + "-" + date[1] + "-" + date[0]   
    except: 
        print("Error in date_reversed(" + str(date) + ") -> set date to today...")
        date = Date().split("-")
        reversed_date = date[2] + "-" + date[1] + "-" + date[0]
    return reversed_date
    
def weekday(year, month, day):
    return datetime.date(year, month, day).weekday()
def date_year():
    now = datetime.datetime.now()
    zeit = str(now.strftime("%Y"))
    return zeit
def date_month():
    now = datetime.datetime.now()
    zeit = str(now.strftime("%m"))
    return zeit
def date_day():
    now = datetime.datetime.now()
    zeit = str(now.strftime("%d"))
    return zeit
    
def date_year_and_month_with_three_digit():
    now = datetime.datetime.now()
    year = str(now.strftime("%y"))
    year = str(int(year[0]) + int(year[1]))
    month = str(now.strftime("%m"))
    return year + month
def date_year_and_month_with_four_digit():
    now = datetime.datetime.now()
    zeit = str(now.strftime("%y%m"))
    return zeit
    
def date_from_weeknumber(date):
    #timestamp = time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple())
    date_answer = datetime.datetime.strptime(date + '-1', '%G-W%V-%u')
    date_answer = str(date_answer).split(" ")[0]
    return date_answer
    
def date_to_timestamp(date):
    timestamp = time.mktime(datetime.datetime.strptime(date, "%Y-%m-%d").timetuple())
    return timestamp
    
def timestamp_to_date(timestamp):
    timestamp = float(timestamp)
    readable = datetime.datetime.fromtimestamp(timestamp)#.isoformat()
    date = str(readable.strftime("%Y-%m-%d"))
    return date
    
def timestamp_to_date_and_hours(timestamp):
    timestamp = float(timestamp)
    readable = datetime.datetime.fromtimestamp(timestamp)#.isoformat()
    date_and_hours = str(readable.strftime("%Y-%m-%d %H:%M"))
    return date_and_hours
    
def timestamp_to_weeknumber(timestamp):
    timestamp = float(timestamp)
    readable = datetime.datetime.fromtimestamp(timestamp)#.isoformat()
    date = str(readable.strftime("%W"))
    return date
    
def timestamp():
    return time.time()
    
def Timestamp():
    return timestamp()

def BlueLenDatei(File):	#	Gibt die anzahl linie zuruck in einem dokument
    File = forPhone(File)

    if os.path.exists(File): 
        Datei = open(File, "r")
        DateiDatenIndex = Datei.readlines()
        Datei.close()
        return len(DateiDatenIndex)

SplitIt = "&zKz&"
def BlueLoad(VarName, File):
    if os.path.isdir(File):
        if str(File)[-1] == "/": File = File + "data"        
        else: File = File + "/data"
    
    if os.path.exists(File): 
        try:
            Datei = codecs.open(File, "r", "utf-8")
            DateiDatenIndex = Datei.readlines()
            Datei.close()
        except:
            Datei = codecs.open(File, "r", "latin-1")
            DateiDatenIndex = Datei.readlines()
            Datei.close()
        Gefunden=False
        for AlleLinien in DateiDatenIndex:
            LinienVarName = AlleLinien.split(SplitIt)[0]
            if VarName == LinienVarName:	
                SavedData = AlleLinien.split(SplitIt)[1].rstrip()
                return str(SavedData)
    return "None"

def BlueSave(VarName, VarData, File):
    if os.path.isdir(File):
        if str(File)[-1] == "/": File = File + "data"        
        else: File = File + "/data"
        
    if os.path.exists(File): 
        Datei = open(File, "r", errors="ignore")
        DateiDatenIndex = Datei.readlines()
        Datei.close()
        Gefunden=False
        for AlleLinien in DateiDatenIndex:
            LinienVarName = AlleLinien.split(SplitIt)[0]
            if VarName == LinienVarName:	
                Gefunden=True
                LinienVarData = AlleLinien.split(SplitIt)[1]

                Datei = open(File, "r", errors="ignore")
                DateiDaten = Datei.read()
                Datei.close()

                Datei = open(File, "w")
                Datei.write(DateiDaten.replace(str(LinienVarName) + str(SplitIt) + str(LinienVarData), str(VarName) + str(SplitIt) + str(VarData) + "\n"))
                Datei.close()
        if not Gefunden:
            Datei = open(File, "a")
            Datei.write("\n" + str(VarName) + str(SplitIt) + str(VarData))
            Datei.close()
    else:
        Datei = open(File, "w")
        Datei.write(str(VarName) + str(SplitIt) + str(VarData))

def getData(variable):
    antwort = BlueLoad(variable, "/etc/zk-data-libs/data")
    if str(antwort) == "None":
        if variable == "IndexLimit": antwort = "50"
        if variable == "SERVERSTOCK": antwort = "192.168.1.35"#"127.0.0.1"
        if variable == "PCNAME": antwort = str(socket.gethostname())
        if variable == "Kunde_ID_Start": antwort = 0
        if variable == "Minimum_Anzahl_Der_Linien": antwort = 20
        if variable == "LastPrinter": antwort = 0
        if variable == "PrinterIP" : antwort = "192.168.1.19"#"ZBR7681522"
        if variable == "PrinterDeliveryNoteBarcodeIP" : antwort = "192.168.1.24"#"ZBR5533304"

        BlueSave(variable, json.dumps(antwort), "/etc/zk-data-libs/data")
    print("getData(" + str(variable) + "): " + str(antwort))
    
    try: antwort = json.loads(antwort)
    except:
        print("Error on loading data as json-> convert it to String")
        antwort = str(antwort)
        BlueSave(variable, json.dumps(antwort), "data")
    return antwort

def setData(name, variable):
    print("setData(" + str(name) + ", " + str(variable) + ")")
    BlueSave(name, json.dumps(variable), "/etc/zk-data-libs/data")
    return True
    
        
def set_data(name, content, filename="/etc/zk-data-libs/database/data.db"):
    name = str(name)
    content_as_json = json.dumps(content) # convert obj to json
    filename = str(filename)
    if not ".db" in filename: filename = filename + ".db"

    database_db = SqliteDatabase(filename)
    database_migrator = SqliteMigrator(filename)

    class Data(Model):
        identification = CharField(primary_key = True)
        content = CharField()

        class Meta:
            database = database_db
            
    try: database_db.connect()
    except: True

    try:
        database_db.create_tables([Data])
    except: True
    
    query = Data.select().where(Data.identification == name)
    if query.exists():
        ThisDATA = dict_to_model(Data, {"identification": name, "content": content_as_json})
        try:
            ThisDATA.save()
            print("DATA updated: " + str(ThisDATA.identification) + ": " + str(content_as_json))
        except: print("DATA updating ERROR")
    else:
        ThisDATA = Data.create(identification = name, content = content_as_json)
        try:
            ThisDATA.save()
            print("DATA created: " + str(ThisDATA.identification) + ": " + str(content_as_json))
        except: print("DATA creating ERROR")

    try: database_db.close()
    except: True
    
    return True    
    
def get_data(name, filename="/etc/zk-data-libs/database/data.db"):
    name = str(name)
    content = None
    filename = str(filename)
    if not ".db" in filename: filename = filename + ".db"

    database_db = SqliteDatabase(filename)
    database_migrator = SqliteMigrator(filename)

    class Data(Model):
        identification = CharField(primary_key = True)
        content = CharField()

        class Meta:
            database = database_db
            
    try: database_db.connect()
    except: True

    try:
        database_db.create_tables([Data])
    except: True
    
    query = Data.select().where(Data.identification == name)
    if query.exists():
        content = json.loads(query[0].content) # convert json to obj
    else:
        print("DATA not found in database")

    try: database_db.close()
    except: True
    
    return content

def get_all_data(filename="/etc/zk-data-libs/database/data.db"):
    content = None
    filename = str(filename)
    if not ".db" in filename: filename = filename + ".db"

    database_db = SqliteDatabase(filename)
    database_migrator = SqliteMigrator(filename)

    class Data(Model):
        identification = CharField(primary_key = True)
        content = CharField()

        class Meta:
            database = database_db
            
    try: database_db.connect()
    except: True

    try:
        database_db.create_tables([Data])
    except: True
    
    content = []
    query = Data.select()
    if query.exists():
        for obj in query:
            this_identification = json.loads(obj.identification)
            this_content = json.loads(obj.content) # convert json to obj
            content.append({"identification" : this_identification, "content": this_content})
    else:
        print("DATA not found in database")

    try: database_db.close()
    except: True
    
    return content
    
def get_printer():
    print("get_printer()")
    printerList0 = os.popen("lpstat -a").readlines()
    printerList = []
    for eachPrinterLine in printerList0:
        printerList.append(str(eachPrinterLine.split(" ")[0]))
    
    return printerList
