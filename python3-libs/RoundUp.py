#!/usr/bin/env python3

def RoundUp5(Zahl): # RoundUp without Decimal
    print("RoundUp5(" + str(Zahl) + ")")
    decimal = int(str(Zahl).split(".")[1][0])
    if decimal == 0:
        Zahl = float(int(Zahl))
    elif decimal < 6:
        Zahl = float(int(Zahl) + 0.5)
    else:
        Zahl = float(int(Zahl) + 1)
    return Zahl
    
def RoundUp(Zahl): # RoundUp without Decimal
    print("RoundUp(" + str(Zahl) + ")")
    decimal = int(str(Zahl).split(".")[1][0])
    if decimal == 0:
        Zahl = float(int(Zahl))
    else:
        Zahl = float(int(Zahl) + 1)
    return Zahl

def RoundUp05(Zahl): # 0.05
    Zahl = str(Zahl).replace(",", ".")
    Zahl = round(float(Zahl), 2)
    init_Zahl = str(Zahl)
    
    #if Zahl < 0.05:
    #    Zahl = 0.05
    
    Zahl = str(Zahl)
    if "." in Zahl:
        Int = Zahl.split(".")[0]
        Decimal = Zahl.split(".")[-1]
        FirstDecimal = int(Decimal[0])
        try: LastDecimal = int(Decimal[1])
        except: LastDecimal = 0

        if LastDecimal < 3: LastDecimal = 0
        else:
            if LastDecimal < 8: LastDecimal = 5 
            else:
                FirstDecimal = FirstDecimal + 1
                if FirstDecimal == 10:
                    FirstDecimal = 0
                    Int = int(Int) + 1
                LastDecimal = 0
    Zahl = str(Int) + "." + str(FirstDecimal) + str(LastDecimal)
    print("RoundUp05(" + init_Zahl + ") -> " + Zahl)

    Zahl = float(Zahl)        
    return Zahl

def RoundUp0000(Zahl): # 0.0000
	Zahl = str(Zahl).replace(",", ".")
	Zahl = float(Zahl) * 10000.0000
	Zahl = int(Zahl)
	Zahl = float(Zahl) / 10000.0000
	return Zahl
	
def RoundUp00(Zahl): # 0.00
	Zahl = str(Zahl).replace(",", ".")
	Zahl = float(Zahl) * 100.00
	Zahl = int(Zahl)	
	Zahl = float(Zahl / 100.00)	
	return Zahl
	
def RoundUp00_as_string(Zahl): # 0.00 always with 2 decimals
    Zahl = str(RoundUp00(Zahl))
    decimals = Zahl.split(".")[-1]
    if not len(decimals) == 2: Zahl = Zahl + "0"
    return Zahl
