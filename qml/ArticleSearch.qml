import QtQuick 2.7
import QtQuick.Window 2.15
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.15

Rectangle {
    id: rect_article_search
    property var text: ""
    onTextChanged: article_list_searchbox.text = text

    color: "lightblue"

    property var old_text: ""
    property var article_to_load: []
    property var articles: []
    property var article_categories: []
    property int article_to_load_index: 0
    property var selected_article: ""

    width: 800
    height: 800

    property var ip: ""
    property var secure_key: ""

    property int progress_max: 0
    property int progress_value: 0
    onProgress_valueChanged: {
        console.warn("progress_value: " + progress_value)
        progress_bar.value = 100/progress_max * progress_value /100

        if (progress_value == progress_max) {
            article_grid_list.visible = true
            progress_bar.visible = false
        } else {
            article_grid_list.visible = false
            progress_bar.visible = true
        }

    }

    Timer {
        id: timer_load_articles
        interval: 10
        onTriggered: {
            //console.warn("article_to_load: " + article_to_load)
            //console.warn("article_to_load_index: " + article_to_load_index)

            var id = article_to_load[article_to_load_index]

            python_article_search.call("libs.send.get_article", [ip, secure_key, id], function(answer_article) {
                if (answer_article["bild"] == "") {answer_article["bild"] = "/etc/zk-data-libs/images/no-image-found.jpeg"}
                answer_article["id"] = id

                //article_model.append(answer_article)
                articles.push(answer_article)
                progress_value += 1

                if (article_to_load_index+1 < article_to_load.length) {
                    article_to_load_index += 1
                    timer_load_articles.start()
                } else {
                    article_to_load_index = 0
                    timer_sort_articles.start()
                }
            })


        }
    }

    Timer {
        id: timer_sort_articles
        interval: 10
        onTriggered: {
            article_model.clear()

            python_article_search.call("libs.BlueFunc.split_list_of_dict_by_key", [articles, "anzahl", 0], function(answer_list) {
                var list_without_var = answer_list[0]
                var list_with_var = answer_list[1]

                python_article_search.call("libs.BlueFunc.sort_list_of_dict", [list_without_var, "groesse", "identification"], function(answer_list) {
                    for (var i = 0; i < answer_list.length; i++)  {
                        article_model.append(answer_list[i])
                    }

                    python_article_search.call("libs.BlueFunc.sort_list_of_dict", [list_with_var, "groesse", "identification"], function(answer_list) {
                        for (var i = 0; i < answer_list.length; i++)  {
                            article_model.append(answer_list[i])
                        }
                        progress_value = progress_max
                    })

                })
            })
        }
    }

    function search_articles(text) {
        if (text == old_text) {
        } else {
            article_model.clear()

            articles = []
            article_to_load = []
            article_to_load_index = 0

            progress_max = 1
            progress_value = 0

            var isChecked = false
            var cat_id = ""
            for (var i = 0; i < rect_article_search.article_categories.length; i++)  {
                isChecked = rect_article_search.article_categories[i]["isChecked"]
                if (isChecked == true) {
                    console.warn("isChecked: " + isChecked)
                    cat_id = rect_article_search.article_categories[i]["identification"]

                    python_article_search.call("libs.send.search_article", [ip, secure_key, {"suche": text, "categorie": cat_id, "limit": 100}], function(answer_list) {
                        for (var i = 0; i < answer_list.length; i++)  {
                            article_to_load.push(answer_list[i])
                        }
                        python_article_search.call("libs.send.search_article", [ip, secure_key, {"name": text, "categorie": cat_id, "limit": 100}], function(answer_list) {
                            for (var i = 0; i < answer_list.length; i++)  {
                                if (article_to_load.includes(answer_list[i]) === false) {
                                    article_to_load.push(answer_list[i])

                                }
                            }
                            old_text = text
                            if (article_to_load.length > 0) {
                                progress_max = article_to_load.length + 1
                                timer_load_articles.start()
                            } else {
                                progress_value = progress_max
                            }
                        })
                    })

                }


            }

            if (cat_id == "") {
                python_article_search.call("libs.send.search_article", [ip, secure_key, {"suche": text, "limit": 100}], function(answer_list) {
                    for (var i = 0; i < answer_list.length; i++)  {
                        article_to_load.push(answer_list[i])
                    }
                    python_article_search.call("libs.send.search_article", [ip, secure_key, {"name": text, "limit": 100}], function(answer_list) {
                        for (var i = 0; i < answer_list.length; i++)  {
                            if (article_to_load.includes(answer_list[i]) === false) {
                                article_to_load.push(answer_list[i])

                            }
                        }
                        old_text = text
                        if (article_to_load.length > 0) {
                            progress_max = article_to_load.length + 1
                            timer_load_articles.start()
                        } else {
                            progress_value = progress_max
                        }
                    })
                })
            }



        }
    }

    TextField {
        id: article_list_searchbox
        text: ""
        selectByMouse: true
        anchors.top: parent.top
        anchors.margins: 10
        anchors.left: parent.left

        onEditingFinished: {
            rect_article_search.text = text
            console.warn("article_list_searchbox.text: " + article_list_searchbox.text)
            search_articles(text)
        }
    }

    Button {
        id: button_get_categorie
        text: "Kategorien"
        anchors.top: parent.top
        anchors.margins: 10
        anchors.left: article_list_searchbox.right

        onClicked: {
            article_categorie_list.visible = true
        }
    }

    CheckList {
        id: article_categorie_list
        text: "Nach Folgenden Kategorien suchen"
        items: rect_article_search.article_categories
        onCheckedChanged: {
            for (var i = 0; i < rect_article_search.article_categories.length; i++) {
                if (changingItem == rect_article_search.article_categories[i]["name"]) {
                    rect_article_search.article_categories[i]["isChecked"] = changingState
                }

            }
        }
    }

    CircularProgressBar {
                id: progress_bar
                lineWidth: 10
                value: 0
                size: 150
                secondaryColor: "#e0e0e0"
                primaryColor: "#29b6f6"
                animationDuration: 10
                visible: false

                anchors.centerIn: article_grid_list
    }

    ListModel {
        id: article_model
    }

    GridView {
        id: article_grid_list
        width: rect_article_search.width
        height: rect_article_search.height - article_list_searchbox.height
        anchors.top: article_list_searchbox.bottom
        anchors.left: rect_article_search.left
        anchors.margins: 25
        clip: true

        cellHeight: 250
        cellWidth: 250

        model: article_model
        delegate: Column {
            Image {
                id: image_main
                source: bild
                anchors.horizontalCenter: parent.horizontalCenter
                width: 200
                height: 200
                asynchronous: true
                fillMode: Image.PreserveAspectFit

                MouseArea {
                    anchors.fill: parent
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    onClicked: (mouse)=> {
                        if (mouse.button == Qt.RightButton) {
                            python_article_search.call("os.system", ["python3 /etc/zk-data-client/qml/ChangeStock.py " + ip + " " + secure_key + " " +  id], function() {})
                        } else {
                            /*
                            if (anzahl == 0) {} else {
                                image_animation.running = true
                                image_animation_width.running = true
                            }
                            */
                            rect_article_search.selected_article = id

                        }
                    }
                }

                Label {
                    id: article_quantity
                    text: anzahl + "x"
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.margins: 5
                }

                Image {
                    id: image_out_of_stock
                    source: "/etc/zk-data-libs/images/out_of_stock.png"
                    width: 50
                    height: 50
                    visible: true
                }

                NumberAnimation on height {
                    id: image_animation
                    from: 200; to: 0
                    duration: 500
                    running: false
                    onStopped: {
                        image_main.height = 200

                        // add to delivery
                        console.warn("delivery: " + article_basket.basket_delivery_id)
                        var data = {'mode': 'set_delivery_line', 'secure_key': secure_key, "identification": article_basket.basket_delivery_id, "index": -1, "anzahl": 1, "barcode": id, "name": name_de, "preis": preisvk, "tva": parseFloat(tva)}
                        var answer_set_delivery_line = JSON.parse(send.send_and_receive(server_ip, JSON.stringify(data)))

                        article_basket.load_basket(article_basket.basket_delivery_id)
                        anzahl -= 1
                    }
                }
                NumberAnimation on width {
                    id: image_animation_width
                    from: 200; to: 0
                    duration: 500
                    running: false
                    onStopped: {
                        image_main.width = 200
                    }
                }
            }

            Text {
                text: id + ": " + name_de
                anchors.horizontalCenter: parent.horizontalCenter
                y: 150
                width: parent.width
                clip: true
                height: 80
                wrapMode: Text.WordWrap
            }

            Component.onCompleted: {
                if (anzahl == 0.0) {
                    image_out_of_stock.visible = true
                    image_main.opacity = 0.5
                    article_quantity.visible = false
                } else {
                    image_out_of_stock.visible = false
                    image_main.opacity = 1
                    article_quantity.visible = true
                }

            }
        }
    }


    Python {
        id: python_article_search
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('os', function () {});

            article_list_searchbox.forceActiveFocus()
        }

    }

}
