import QtQuick 2.7
import QtQuick.Controls 2.0

Item {
    id: root
    signal checkedChanged()

    //property bool visible: false
    property string text: ""
    property var items//: []
    property string changingItem: ""
    property bool changingState: false

    width: 500
    height: 500

    function antwortArbeiterListe(items) {
        contactModel.clear();
        for (var i=0; i<items.length; i++) {
            contactModel.append(items[i]);
        }
    }

    onVisibleChanged: {
        if (visible == true) {
            antwortArbeiterListe(root.items)
        }
    }

    z: parent.z + 5
    visible: false
    anchors.fill: parent
    Rectangle {
        id: rootRectangle
        anchors.fill: parent
        visible: root.visible
        z: 2
        color: "#80000000"
        Label {
            text: root.text
            x: rootRectangle.width / 2 - width / 2
            font.pixelSize: 15
            y: rectangle.y / 2 - height / 2
        }

        MouseArea {
            id: mouseAreaClose
            anchors.fill: rootRectangle
            z: rootRectangle.z - 2
            onClicked: {
                if ( rectangle.x > mouseX | mouseX > (rectangle.x + rectangle.width) | rectangle.y > mouseY | mouseY > (rectangle.y + rectangle.height )) {
                    root.visible = false
                }
            }
        }
        Rectangle {
            id: rectangle
            z: rootRectangle.z + 1
            border.width: 2
            radius: 20
            height: parent.height / 3 * 2
            width: parent.width / 3 * 2
            anchors.centerIn: parent

            ListView {
                id: listLieferscheinSuchen
                highlightMoveDuration: 0
                anchors.fill: parent

                clip: true

                ScrollBar.vertical: ScrollBar {
                    active: true;
                    //policy: ScrollBar.AlwaysOn
                    width: 25
                }

                ListModel {
                    id: contactModel
                }

                Component {
                    id: contactDelegate
                    Item {
                        id: itemListe
                        width: listLieferscheinSuchen.width
                        height: 50

                        CheckBox {
                            anchors.centerIn: parent
                            text: name
                            checked: isChecked
                            font.pixelSize: 10

                            onCheckedChanged: {
                                if (focus == true) {
                                    root.changingItem = name
                                    root.changingState = checked
                                    root.checkedChanged()
                                }
                            }
                        }

                    }
                }

                model: contactModel
                delegate: contactDelegate
            }
        }
    }

}
