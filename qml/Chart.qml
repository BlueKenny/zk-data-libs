import QtQuick 2.7
import QtQuick.Controls 2.0

// [[1, 100.00], [2, 150.00]]
Rectangle {
    id: root_chart_frame

    property var x_names: []
    property var points: []
    property int maxHeight: 1
    property var title: ""
    property var chart_color: "lightblue"

    border.width: 1

    height: 500; width: 500
    color: "transparent"

    Rectangle {
        id: axe_y
        width: 20
        height: root_chart_frame.height
        color: "transparent"
    }
    Rectangle {
        id: rectangle_title
        width: root_chart_frame.width
        height: axe_y.width * 2
        color: "transparent"

        Label {
            x: parent.width / 2 - width / 2
            y: parent.height / 2 - height / 2
            text: root_chart_frame.title
        }
    }

    Rectangle {
        id: axe_x
        width: root_chart_frame.width
        height: axe_y.width * 2
        color: "transparent"
        anchors.bottom: parent.bottom

    }

    function redraw() {
        console.log("chart redraw()")

        maxHeight = 1
        for (var i = 0; i < points.length; ++i) {
            if (points[i] > maxHeight) {
                maxHeight = points[i]
            }
        }

        for (var i = 0; i < points.length; ++i) {
            repeater.itemAt(i).height = (root_chart_frame.height - axe_x.height*3) / maxHeight * (points[i]+0.00001)
            repeater.itemAt(i).y = root_chart_frame.height - repeater.itemAt(i).height - axe_x.height

            repeater.itemAt(i).text = points[i]

            repeater.itemAt(i).x_name = x_names[i]

        }
    }

    Component.onCompleted: {
        redraw()
    }

    onHeightChanged: {
        redraw()
    }

    Row {
        x: axe_y.width
        Repeater {
            id: repeater
            model: points.length
            Rectangle {
                property var text: "TEXT"
                property var x_name: ""
                width: (root_chart_frame.width - axe_y.width*2) / points.length
                height: 0.01
                border.width: 1

                color: root_chart_frame.chart_color

                //anchors.bottom: root_chart_frame.bottom

                Label {
                    id: upper_text
                    text: parent.text
                    anchors.bottom: parent.top
                    anchors.bottomMargin: 10
                    x: parent.width / 2 - width / 2
                }
                Label {
                    id: lower_text
                    text: parent.x_name
                    y: parent.height
                    x: parent.width / 2 - width / 2
                }
            }
        }
    }

}
