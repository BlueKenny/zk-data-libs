import QtQuick 2.7
import QtQuick.Window 2.15
import io.thp.pyotherside 1.5
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import QtCharts 2.6

Rectangle {
    id: rect_article_change

    color: "lightblue"
    width: 800
    height: 800
    property var ip: ""
    property var secure_key: ""
    property var article_identification: ""
    property bool changed: false

    property int process_id: 0

    signal article_saved(var article)

    onArticle_identificationChanged: {
        if (article_identification == "") {
            label_article_identification_title_1.text = ""
            label_article_identification_title_2.text = ""
            label_article_identification_title_3.text = ""
            image_article_change.source = ""
            article_quantity.text = ""

        } else {
            python_article_change.call("libs.send.get_article", [ip, secure_key, article_identification], function(answer_article) {
                label_article_identification_title_1.text = answer_article["identification"] + ": " + answer_article["name_de"]
                label_article_identification_title_2.text = answer_article["identification"] + ": " + answer_article["name_de"]
                label_article_identification_title_3.text = answer_article["identification"] + ": " + answer_article["name_de"]

                if (answer_article["bild"] == "") {answer_article["bild"] = "/etc/zk-data-libs/images/no-image-found.jpeg"}
                image_article_change.source = answer_article["bild"]

                article_quantity.text = answer_article["anzahl"] + "x"

                //// allgemein
                article_name_de.text = answer_article["name_de"]
                article_name_fr.text = answer_article["name_fr"]
                article_supplier.text = answer_article["lieferant"]
                article_number_1.text = answer_article["artikel"]
                article_number_2.text = answer_article["artikel2"]
                article_number_3.text = answer_article["artikel3"]
                article_number_4.text = answer_article["artikel4"]
                article_categorie.text = answer_article["categorie"]
                article_ean.text = answer_article["barcode"]
                article_image.text = answer_article["bild"]
                article_size.text = answer_article["groesse"]
                //// inventar
                article_vat.text = answer_article["tva"]
                article_price_ek.text = answer_article["preisek"]
                article_price_vkh.text = answer_article["preisvkh"]
                article_price_vk.text = answer_article["preisvk"]
                article_minimum.text = answer_article["minimum"]
                article_place.text = answer_article["ort"]
                //// beschreibung
                article_info_de.text = answer_article["beschreibung_de"]
                article_info_fr.text = answer_article["beschreibung_fr"]


                python_article_change.call("libs.BlueFunc.date_year", [], function(year) {
                    article_chart.points = [0]
                    article_chart.redraw()
                    rect_article_change.process_id += 1
                    load_chart(year, 0, [0], rect_article_change.process_id)
                })


            })
        }
    }

    function article_save() {
        console.warn("article_save()")

        var article = {}

        article["identification"] = rect_article_change.article_identification
        article["name_de"] = article_name_de.text
        article["name_fr"] = article_name_fr.text
        article["lieferant"] = article_supplier.text
        article["artikel"] = article_number_1.text
        article["artikel2"] = article_number_2.text
        article["artikel3"] = article_number_3.text
        article["artikel4"] = article_number_4.text
        article["categorie"] = article_categorie.text
        article["barcode"] = article_ean.text
        article["bild"] = article_image.text
        article["groesse"] = article_size.text
        article["tva"] = article_vat.text
        article["preisek"] = article_price_ek.text
        article["preisvkh"] = article_price_vkh.text
        article["preisvk"] = article_price_vk.text
        article["minimum"] = article_minimum.text
        article["ort"] = article_place.text
        article["beschreibung_de"] = article_info_de.text
        article["beschreibung_fr"] = article_info_fr.text

        python_article_change.call("libs.send.set_article", [ip, secure_key, article], function(answer_article) {
            var saved_identification = rect_article_change.article_identification
            rect_article_change.article_identification = ""
            rect_article_change.article_identification = saved_identification

            rect_article_change.changed = false

            rect_article_change.article_saved(article)
        })
    }

    function load_chart(year, index, points, prozess_id) {
        if (rect_article_change.process_id == prozess_id) {
            if (index == 12) {
                article_chart.points = points
                article_chart.redraw()

                // Load Chart2 from last year
                article_chart2.points = [0]
                article_chart2.redraw()
                rect_article_change.process_id += 1
                load_chart2(year-1, 0, [0], rect_article_change.process_id)

            } else {
                var month = String(index+1)
                if (month.length === 1) {
                    var date = year + "-0" + month
                } else {
                    var date = year + "-" + month
                }
                python_article_change.call("libs.send.get_article_sell", [ip, secure_key, article_identification, date], function(answer_count) {
                    if (rect_article_change.process_id == prozess_id) {
                        points[index] = answer_count

                        article_chart.points = points
                        article_chart.redraw()

                        load_chart(year, index+1, points, prozess_id)
                    }
                })
            }
        }
    }

    function load_chart2(year, index, points, prozess_id) {
        if (rect_article_change.process_id == prozess_id) {
            if (index == 12) {
                article_chart2.points = points
                article_chart2.redraw()
            } else {
                var month = String(index+1)
                if (month.length === 1) {
                    var date = year + "-0" + month
                } else {
                    var date = year + "-" + month
                }
                python_article_change.call("libs.send.get_article_sell", [ip, secure_key, article_identification, date], function(answer_count) {
                    if (rect_article_change.process_id == prozess_id) {
                        points[index] = answer_count

                        article_chart2.points = points
                        article_chart2.redraw()

                        load_chart2(year, index+1, points, prozess_id)
                    }
                })
            }
        }
    }

    TabBar {
        id: bar_article_change
        width: parent.width
        TabButton {
            text: qsTr("Allgemein")
        }
        TabButton {
            text: qsTr("Preis - Inventar")
        }
        TabButton {
            text: qsTr("Beschreibung")
        }
        TabButton {
            text: qsTr("Statistik")
        }
    }

    StackLayout {
        width: parent.width
        anchors.top: bar_article_change.bottom

        currentIndex: bar_article_change.currentIndex
        Item {
            id: homeTab_article_change
            //anchors.fill: parent

            ColumnLayout {
                anchors.fill: parent
                Layout.fillHeight: false
                Layout.fillWidth: false
                Layout.margins: 10


                Label {
                    id: label_article_identification_title_1
                    anchors.margins: 10
                    Layout.alignment: Qt.AlignCenter
                }

                Image {
                    id: image_article_change
                    source: ""
                    anchors.margins: 10
                    asynchronous: true
                    fillMode: Image.PreserveAspectFit

                    Layout.maximumWidth: rect_article_change.width
                    Layout.maximumHeight: 250

                    Layout.alignment: Qt.AlignCenter

                    Label {
                        id: article_quantity
                        text: ""
                        font.pointSize: 20
                        anchors.right: image_article_change.right
                        anchors.top: image_article_change.top
                        anchors.margins: 5
                    }
                }

                RowLayout {
                    Label { text: "Name (Deutsch):"}
                    TextField {
                        id: article_name_de
                        text: ""
                        selectByMouse: true
                        Layout.preferredWidth: 350
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }


                RowLayout {
                    Label { text: "Name (Français):"}
                    TextField {
                        id: article_name_fr
                        text: ""
                        selectByMouse: true
                        Layout.preferredWidth: 350
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Lieferant:"}
                    TextField {
                        id: article_supplier
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Artikelnummer (1):"}
                    TextField {
                        id: article_number_1
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }
                RowLayout {
                    Label { text: "Artikelnummer (2):"}
                    TextField {
                        id: article_number_2
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }
                RowLayout {
                    Label { text: "Artikelnummer (3):"}
                    TextField {
                        id: article_number_3
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }
                RowLayout {
                    Label { text: "Artikelnummer (4):"}
                    TextField {
                        id: article_number_4
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "EAN Barcode:"}
                    TextField {
                        id: article_ean
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Bild URL/Link:"}
                    TextField {
                        id: article_image
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Grösse:"}
                    TextField {
                        id: article_size
                        text: ""
                        Layout.preferredWidth: 350
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                Button {
                    text: "Speichern"
                    Layout.alignment: Qt.AlignCenter
                    visible: rect_article_change.changed ? true : false
                    onClicked: {
                        article_save()
                    }
                }
            }
        }
        Item {
            id: inventoryTab_article_change

            ColumnLayout {
                anchors.fill: parent
                Layout.fillHeight: false
                Layout.margins: 10

                Label {
                    id: label_article_identification_title_2
                    x: rect_article_change.width / 2 - width / 2
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Mehrwehrtsteuersatz:"}
                    TextField {
                        id: article_vat
                        text: ""
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true

                            var price_vk = article_price_vk.text
                            var vat = 1+article_vat.text/100
                            var price_vkh = price_vk / vat
                            console.warn("price_vkh: " + price_vkh)
                            article_price_vkh.text = price_vkh
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }
                RowLayout {
                    Label { text: "Einkaufspreis:"}
                    TextField {
                        id: article_price_ek
                        text: ""
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }
                RowLayout {
                    Label { text: "Verkaufspreis (ohne Steuern):"}
                    TextField {
                        id: article_price_vkh
                        text: ""
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true

                            var price_vkh = article_price_vkh.text
                            var vat = 1+article_vat.text/100
                            var price_vk = price_vkh * vat
                            console.warn("price_vk: " + price_vk)
                            article_price_vk.text = price_vk
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }
                RowLayout {
                    Label { text: "Verkaufspreis (inkl. Steuern):"}
                    TextField {
                        id: article_price_vk
                        text: ""
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true

                            var price_vk = article_price_vk.text
                            var vat = 1+article_vat.text/100
                            var price_vkh = price_vk / vat
                            console.warn("price_vkh: " + price_vkh)
                            article_price_vkh.text = price_vkh
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Kategorie:"}
                    TextField {
                        id: article_categorie
                        text: ""
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Minimum Stock Anzahl:"}
                    TextField {
                        id: article_minimum
                        text: ""
                        selectByMouse: true
                        validator: IntValidator {bottom: 0; top: 999999}
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                RowLayout {
                    Label { text: "Lagerort:"}
                    TextField {
                        id: article_place
                        text: ""
                        selectByMouse: true
                        onEditingFinished: {
                            rect_article_change.changed = true
                        }
                    }
                    Layout.alignment: Qt.AlignCenter
                }

                Button {
                    text: "Speichern"
                    Layout.alignment: Qt.AlignCenter
                    visible: rect_article_change.changed ? true : false
                    onClicked: {
                        article_save()
                    }
                }

            }
        }
        Item {
            id: infoTab_article_change

            ColumnLayout {
                anchors.fill: parent
                Layout.fillHeight: false
                Layout.margins: 10

                Label {
                    id: label_article_identification_title_3
                    x: rect_article_change.width / 2 - width / 2
                    Layout.alignment: Qt.AlignCenter
                }

                Label { text: "Beschreibung DE:"}
                TextArea {
                    id: article_info_de
                    text: ""
                    selectByMouse: true
                    width: 500//rect_article_change.width
                    height: 250
                    onTextChanged: {
                        if (focus == true) {
                            rect_article_change.changed = true
                        }
                    }
                }
                Label { text: "Beschreibung FR:"}
                TextArea {
                    id: article_info_fr
                    text: ""
                    selectByMouse: true
                    width: 500//rect_article_change.width
                    height: 250
                    onTextChanged: {
                        if (focus == true) {
                            rect_article_change.changed = true
                        }
                    }
                }

                Button {
                    text: "Speichern"
                    Layout.alignment: Qt.AlignCenter
                    visible: rect_article_change.changed ? true : false
                    onClicked: {
                        article_save()
                    }
                }

            }
        }

        Item {
            id: statsTab_article_change

            ColumnLayout {
                anchors.fill: parent
                Layout.fillHeight: false
                Layout.margins: 10

                Label {
                    id: label_article_identification_title_4
                    x: rect_article_change.width / 2 - width / 2
                    Layout.alignment: Qt.AlignCenter
                }

                Chart {
                    id: article_chart

                    width: rect_article_change.width
                    height: 250

                    x_names: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]
                    points: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                }

                Label {
                    x: rect_article_change.width / 2 - width / 2
                    Layout.alignment: Qt.AlignCenter
                    text: "Verkaufsanzahl pro Monat in diesem Jahr"
                }                

                Chart {
                    id: article_chart2

                    width: rect_article_change.width
                    height: 250

                    x_names: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"]
                    points: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                }

                Label {
                    x: rect_article_change.width / 2 - width / 2
                    Layout.alignment: Qt.AlignCenter
                    text: "Verkaufsanzahl pro Monat im Letzten Jahr"
                }

            }

        }
    }




    Python {
        id: python_article_change
        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('/etc/zk-data-libs/'));
            importModule('libs.send', function () {});
            importModule('libs.BlueFunc', function () {});
            importModule('os', function () {});

        }

    }

}
